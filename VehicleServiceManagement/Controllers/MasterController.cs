﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AccounTechSoftware.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using VehicleServiceManagement.Models;
using VehicleServiceManagement.Models.Accounts;

namespace VehicleServiceManagement.Controllers
{
    public class MasterController : Controller
    {
        public static string[] ItemType = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public static string[] TaxType = new string[] { "Taxable", "Non-Taxable", "Free Zone", "G C C" };
        public static string[] companytype = new string[] { "Trading & Service", "Service", "Trading", "Manufacturing & Trading", "Public Relaton" };
        public static string[] DealerType = new string[] { "Retail", "Non Retail", "Workshop", "RE Dealer", "Dealer & Workshop", "Retail Dealer & Workshop", };
        public IActionResult Index()
        {
            return View();
        }
        public PartialViewResult _partialMasterLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }
        private readonly IHostingEnvironment hostingEnvironment;
        public IActionResult CompanyInfoEdit()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CompanyInformation").FirstOrDefault().UpdatePermission)
                {
                    CompanyInformation obj = new CompanyInformation();
                    Country country = new Country();
                    List<Country> objcntrylist = country.GetAllCountries();
                    ViewBag.Country = new SelectList(objcntrylist, "Id", "CountryName");

                    City cty = new City();
                    List<City> objcitylist = cty.GetAllCities();
                    ViewBag.City = new SelectList(objcitylist, "Id", "CityName");

                    Currency Currency = new Currency();
                    var c1 = Currency.GetAllCurrencies();
                    ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
                    SelectList list = new SelectList(ItemType);
                    ViewBag.month = list;

                    SelectList taxlist = new SelectList(TaxType);
                    ViewBag.taxtype = taxlist;

                    SelectList complist = new SelectList(companytype);
                    ViewBag.companytype = complist;

                    CompanyInformation companyInformation;
                    companyInformation = obj.details();

                    BankInformation objbnk = new BankInformation();
                    List<BankInformation> objbanklist = objbnk.GetAllInformation();
                    ViewBag.bank = new SelectList(objbanklist, "IdBranch", "BankName");

                    return View(companyInformation);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult CompanyInfoEdit(CompanyInformation cmpobj)
        {
            string uniqueFileName = null;
            if (cmpobj.photo != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + cmpobj.photo.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                cmpobj.photo.CopyTo(new FileStream(filePath, FileMode.Create));
                cmpobj.logo = "~/images/" + uniqueFileName;
            }
            if (!string.IsNullOrEmpty(cmpobj.IdBranch))
                cmpobj.BankId = Convert.ToInt32(cmpobj.IdBranch.Split(',')[0]);
            CompanyInformation obj = new CompanyInformation();
            //var result = true;
            int k = obj.update(cmpobj);

            #region returnobject

            Country country = new Country();
            List<Country> objcntrylist = country.GetAllCountries();
            ViewBag.Country = new SelectList(objcntrylist, "Id", "CountryName");

            City cty = new City();
            List<City> objcitylist = cty.GetAllCities();
            ViewBag.City = new SelectList(objcitylist, "Id", "CityName");

            Currency Currency = new Currency();
            var c1 = Currency.GetAllCurrencies();
            ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
            SelectList list = new SelectList(ItemType);
            ViewBag.month = list;

            SelectList taxlist = new SelectList(TaxType);
            ViewBag.taxtype = taxlist;

            SelectList complist = new SelectList(companytype);
            ViewBag.companytype = complist;

            CompanyInformation companyInformation;
            companyInformation = obj.details();

            BankInformation objbnk = new BankInformation();
            List<BankInformation> objbanklist = objbnk.GetAllInformation();
            ViewBag.bank = new SelectList(objbanklist, "IdBranch", "BankName");
            ViewBag.companyname = HttpContext.Session.GetString("CompanyName");

            #endregion
            ViewBag.successmessage = "Company Information Updated Successfully !!!";
            if (k <= 0)
            {
                ViewBag.successmessage = "Technical Error Please Try Again !!!!!!!!";
            }
            return View(companyInformation);
        }
        
        #region Dealer

        public IActionResult DealerInformationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault().DisplayPermission)
                {
                    DealerInformation dealer = new DealerInformation();
                    SelectList list1 = new SelectList(DealerType);
                    ViewBag.type = list1;
                    DealerInformationDisplay displayList = new DealerInformationDisplay();
                    displayList.dealerList = dealer.GetAllDealesInformation();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");

        }
        public IActionResult DealerInformationCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault().InsertPermission)
                {
                    SelectList list1 = new SelectList(DealerType);
                    ViewBag.type = list1;
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult DealerInformationCreate(DealerInformation DInfrom)
        {
            string uniqueFileName = null;
            if (DInfrom.TaxDocument != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images/DealerDocument");
                uniqueFileName = DInfrom.Name+"-TaxDocument-"+ Guid.NewGuid().ToString() + "_" + DInfrom.TaxDocument.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                DInfrom.TaxDocument.CopyTo(new FileStream(filePath, FileMode.Create));
                DInfrom.TaxDocumentPath = "~/images/DealerDocument" + uniqueFileName;
            }

            if (DInfrom.VatDocument != null)
            {
                string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images/DealerDocument");
                uniqueFileName = DInfrom.Name + "-VatDocument-" + Guid.NewGuid().ToString() + "_" + DInfrom.TaxDocument.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                DInfrom.VatDocument.CopyTo(new FileStream(filePath, FileMode.Create));
                DInfrom.VatDocumentPath = "~/images/DealerDocument" + uniqueFileName;
            }

            if(DInfrom.DealerType== "Workshop"  || DInfrom.DealerType == "Dealer & Workshop"|| DInfrom.DealerType == "Retail Dealer & Workshop")
            {
                Clsledger objld = new Clsledger();
                objld.Name = DInfrom.Name + " Vndr A/C";
                objld.Code = string.Empty;
                objld.SubGroupId = 17;
                objld.OpeningBalance = 0;
                objld.Currency =1;
                objld.AsOfDate = DateTime.Now;
                objld.Remark = "Workshop Vendor Create";
                DInfrom.VendorLedgerId =objld.create(objld);
                if(DInfrom.DealerType == "Workshop")
                {
                    DInfrom.CustomerLedgerId = 0;
                }

            }
            if(DInfrom.DealerType == "Retail" || DInfrom.DealerType == "Non Retail" || DInfrom.DealerType == "RE Dealer" || DInfrom.DealerType == "Dealer & Workshop" || DInfrom.DealerType == "Retail Dealer & Workshop")
            {
                Clsledger objld = new Clsledger();
                objld.Name = DInfrom.Name + " Cstmr A/C";
                objld.Code = string.Empty;
                objld.SubGroupId = 4;
                objld.OpeningBalance = 0;
                objld.Currency = 1;
                objld.AsOfDate = DateTime.Now;
                objld.Remark = "Dealer Customer Create";
                DInfrom.CustomerLedgerId = objld.create(objld);
                if (DInfrom.DealerType == "Retail" || DInfrom.DealerType == "Non Retail" || DInfrom.DealerType == "RE Dealer")
                {
                    DInfrom.VendorLedgerId = 0;
                }                
            }
            DealerInformation obj = new DealerInformation();
            obj.create(DInfrom);
            return RedirectToAction("DealerInformationIndex");
        }

        public IActionResult Dealerdetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault().DisplayPermission)
                {
                    DealerInformation obj = new DealerInformation();
                    SelectList list1 = new SelectList(DealerType);
                    ViewBag.type = list1;
                    DealerInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult DealerInformationEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault().UpdatePermission)
                {
                    DealerInformation obj = new DealerInformation();
                    SelectList list1 = new SelectList(DealerType);
                    ViewBag.type = list1;
                    DealerInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult DealerInformationEdit(DealerInformation dealer)
        {

            DealerInformation obj = new DealerInformation();

            obj.update(dealer);
            return RedirectToAction("DealerInformationIndex");
        }
        public JsonResult DeleteDealerInformation(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Dealer").FirstOrDefault().DeletePermission)
                {
                    DealerInformation obj1 = new DealerInformation();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        #endregion

        #region DealerBranch

        public IActionResult DealerBranchIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault().DisplayPermission)
                {
                    DealerBranch dealer = new DealerBranch();
                    DealerInformation info = new DealerInformation();
                    var c = info.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c, "Id", "Name");
                    DealerBranchDisplay displayList = new DealerBranchDisplay();
                    displayList.branchList = dealer.GetAllDealerBranch();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult DealerBranchCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault().InsertPermission)
                {
                    DealerInformation info = new DealerInformation();
                    var c = info.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c, "Id", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult DealerBranchCreate(DealerBranch branch)
        {

            DealerBranch obj = new DealerBranch();
            obj.create(branch);
            return RedirectToAction("DealerBranchIndex");
        }
        public IActionResult DealerBranchDeatils(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault().DisplayPermission)
                {
                    DealerBranch obj = new DealerBranch();
                    DealerInformation info = new DealerInformation();
                    var c = info.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c, "Id", "Name");
                    DealerBranch dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult EditDealerBranch(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault().UpdatePermission)
                {
                    DealerBranch obj = new DealerBranch();
                    DealerInformation info = new DealerInformation();
                    var c = info.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c, "Id", "Name");
                    DealerBranch dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditDealerBranch(DealerBranch dealer)
        {
            DealerBranch obj = new DealerBranch();

            obj.update(dealer);
            return RedirectToAction("DealerBranchIndex");
        }
        public JsonResult DeleteDealerBranch(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "DealerBranch").FirstOrDefault().DeletePermission)
                {
                    DealerBranch obj1 = new DealerBranch();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        #endregion
        
        #region make

        public IActionResult MakeDetailIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Make").FirstOrDefault().DisplayPermission)
                {
                    MakeDetail mk = new MakeDetail();
                    MakeDetailDisplay displayList = new MakeDetailDisplay();
                    displayList.makeList = mk.GetAllMakeDetail();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult MakeDetailCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Make").FirstOrDefault().InsertPermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    var c1 = obj.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductGroups obj1 = new ProductGroups();
                    var c2 = obj1.GetAllProductGroups();
                    ViewBag.pg = new SelectList(c2, "Id", "ProductGroup");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public IActionResult MakeDetailCreate(MakeDetail objRsp)
        {

            MakeDetail obj = new MakeDetail();
            obj.create(objRsp);
            return RedirectToAction("MakeDetailIndex");
        }
        public IActionResult MakeDetailss(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Make").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    var c1 = obj.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductGroups obj1 = new ProductGroups();
                    var c2 = obj1.GetAllProductGroups();
                    ViewBag.pg = new SelectList(c2, "Id", "ProductGroup");


                    MakeDetail mk = new MakeDetail();
                    MakeDetail objmk = mk.details(id);
                    return View(objmk);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult MakeDetailEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Make").FirstOrDefault().UpdatePermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    var c1 = obj.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductGroups obj1 = new ProductGroups();
                    var c2 = obj1.GetAllProductGroups();
                    ViewBag.pg = new SelectList(c2, "Id", "ProductGroup");


                    MakeDetail mk = new MakeDetail();
                    MakeDetail objmk = mk.details(id);
                    return View(objmk);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult MakeDetailEdit(MakeDetail objmake)
        {
            MakeDetail obj = new MakeDetail();

            obj.update(objmake);
            return RedirectToAction("MakeDetailIndex");
        }
        public JsonResult DeleteMakeDetail(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Make").FirstOrDefault().DeletePermission)
                {
                    MakeDetail obj1 = new MakeDetail();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }


        #endregion

        #region model

        public IActionResult ModelDetailIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault().DisplayPermission)
                {
                    ModelDetail obj = new ModelDetail();
                    MakeDetail obj1 = new MakeDetail();
                    var c1 = obj1.GetAllMakeDetail();
                    ViewBag.mk = new SelectList(c1, "Id", "Make");
                    ModelDetailDisplay displayList = new ModelDetailDisplay();
                    displayList.modelList = obj.GetAllModelDetail();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult ModelDetailCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault().InsertPermission)
                {
                    MakeDetail obj = new MakeDetail();
                    var c1 = obj.GetAllMakeDetail();
                    ViewBag.mk = new SelectList(c1, "Id", "Make");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ModelDetailCreate(ModelDetail mdetail)
        {

            ModelDetail obj = new ModelDetail();
            obj.create(mdetail);
            return RedirectToAction("ModelDetailIndex");
        }
        public IActionResult ModelDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault().DisplayPermission)
                {
                    ModelDetail obj = new ModelDetail();
                    ModelDetail mdetail;
                    mdetail = obj.details(id);

                    MakeDetail obj1 = new MakeDetail();
                    var c1 = obj1.GetAllMakeDetail();
                    ViewBag.mk = new SelectList(c1, "Id", "Make");

                    return View(mdetail);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ModelDetailEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault().UpdatePermission)
                {
                    ModelDetail obj = new ModelDetail();
                    ModelDetail mdetail;
                    mdetail = obj.details(id);

                    MakeDetail obj1 = new MakeDetail();
                    var c1 = obj1.GetAllMakeDetail();
                    ViewBag.mk = new SelectList(c1, "Id", "Make");

                    return View(mdetail);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ModelDetailEdit(ModelDetail mdetail)
        {

            ModelDetail obj = new ModelDetail();
            obj.update(mdetail);
            return RedirectToAction("ModelDetailIndex");
        }

        public JsonResult DeleteModelDetail(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Model").FirstOrDefault().DeletePermission)
                {
                    ModelDetail obj1 = new ModelDetail();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }


        #endregion

        #region category

        public IActionResult CategoryDetailIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    CategoryDetailDisplay displayList = new CategoryDetailDisplay();
                    displayList.categoryList = obj.GetAllCategoryDetail();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult CategoryDetailCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult CategoryDetailCreate(CategoryDetail category)
        {

            CategoryDetail obj = new CategoryDetail();
            obj.create(category);
            return RedirectToAction("CategoryDetailIndex");
        }
        public IActionResult CategoryDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    CategoryDetail category;
                    category = obj.details(id);
                    return View(category);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CategoryDetailEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault().UpdatePermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    CategoryDetail category;
                    category = obj.details(id);
                    return View(category);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult CategoryDetailEdit(CategoryDetail category)
        {

            CategoryDetail obj = new CategoryDetail();
            obj.update(category);
            return RedirectToAction("CategoryDetailIndex");
        }

        public JsonResult DeleteCategoryDetail(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Category").FirstOrDefault().DeletePermission)
                {
                    CategoryDetail obj1 = new CategoryDetail();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }


        #endregion

        #region Product Group
        public IActionResult ProductGroupsDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault().DisplayPermission)
                {
                    ProductGroups obj = new ProductGroups();
                    ProductGroups dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ProductGroupsIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault().DisplayPermission)
                {
                    ProductGroups service = new ProductGroups();
                    ProductGroupsDisplay displayList = new ProductGroupsDisplay();
                    displayList.productGroupList = service.GetAllProductGroups();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ProductGroupsCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ProductGroupsCreate(ProductGroups service)
        {

            ProductGroups obj = new ProductGroups();
            obj.create(service);
            return RedirectToAction("ProductGroupsIndex");
        }

        public IActionResult EditProductGroups(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault().UpdatePermission)
                {
                    ProductGroups obj = new ProductGroups();

                    ProductGroups dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditProductGroups(ProductGroups dealer)
        {
            ProductGroups obj = new ProductGroups();

            obj.update(dealer);
            return RedirectToAction("ProductGroupsIndex");
        }
        public JsonResult DeleteProductGroups(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductGroup").FirstOrDefault().DeletePermission)
                {
                    ProductGroups obj1 = new ProductGroups();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        #endregion

        #region Product SubGroup
        public IActionResult EditProductSubGroup(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault().UpdatePermission)
                {
                    ProductSubGroup obj = new ProductSubGroup();
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ProductSubGroup dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ProductSubGroupIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault().DisplayPermission)
                {
                    ProductSubGroup service = new ProductSubGroup();
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ProductSubGroupDisplay displayList = new ProductSubGroupDisplay();
                    displayList.productSubGroupList = service.GetAllProductSubGroup();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault();
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ProductSubGroupCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault().InsertPermission)
                {
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ProductSubGroupCreate(ProductSubGroup service)
        {

            ProductSubGroup obj = new ProductSubGroup();
            obj.create(service);
            return RedirectToAction("ProductSubGroupIndex");
        }

        public IActionResult ProductSubGroupDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault().DisplayPermission)
                {
                    ProductSubGroup obj = new ProductSubGroup();
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ProductSubGroup dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditProductSubGroup(ProductSubGroup dealer)
        {
            ProductSubGroup obj = new ProductSubGroup();

            obj.update(dealer);
            return RedirectToAction("ProductSubGroupIndex");
        }
        public JsonResult DeleteProductSubGroup(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ProductSubGroup").FirstOrDefault().DeletePermission)
                {
                    ProductSubGroup obj1 = new ProductSubGroup();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }


        #endregion


        //public IActionResult ModelYearIndex()
        //{
        //    ModelYear obj = new ModelYear();
        //    //return View(obj.GetAllModelYearDetail());
        //}

        //public IActionResult ModelYearCreate()
        //{

        //    return View();
        //}
        //[HttpPost]
        //public IActionResult ModelYearCreate(ModelYear myear)
        //{

        //    //ModelYear obj = new ModelYear();
        //    //obj.create(myear);
        //    //return RedirectToAction("ModelYearIndex");
        //}

        //public IActionResult ModelYearEdit(int id)
        //{
        //    //ModelYear obj = new ModelYear();
        //    //ModelYear myear;
        //    //myear = obj.details(id);

        //    //return View(myear);
        //}
        //[HttpPost]
        //public IActionResult ModelYearEdit(ModelYear myear)
        //{

        //    //ModelYear obj = new ModelYear();
        //    //obj.update(myear);
        //    //return RedirectToAction("ModelYearIndex");
        //}

        //public JsonResult DeleteModelYear(int Id)
        //{
        //    //ModelYear obj1 = new ModelYear();
        //    //var result = true;
        //    //obj1.delete(Id);
        //    //return Json(result);
        //}


    }
}