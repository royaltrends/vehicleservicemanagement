﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccounTechSoftware.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VehicleServiceManagement.Models;

namespace VehicleServiceManagement.Controllers
{
    public class VATController : Controller
    {
        public IActionResult Index()
        {
            //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
            return View();
        }
        public PartialViewResult _partialVATLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }
        public IActionResult VatCategoryIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "VATCategory").FirstOrDefault().DisplayPermission)
                {
                    VatCategory obj = new VatCategory();
                    VatCategory vat = new VatCategory();
                    List<VatCategory> dep = new List<VatCategory>();
                    dep = obj.GetAllVat().ToList();
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    vat.vatCategories = dep;
                    vat.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "VATCategory").FirstOrDefault();
                    return View(vat);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult CreateVatCategory(VatCategory vat)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "VATCategory").FirstOrDefault().InsertPermission)
                {
                    VatCategory obj = new VatCategory();
                    obj.create(vat);
                    return RedirectToAction("VatCategoryIndex");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult EditVatCategory(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "VATCategory").FirstOrDefault().UpdatePermission)
                {
                    VatCategory obj = new VatCategory();
                    VatCategory vat;
                    vat = obj.GetbyId(Id);
                    List<VatCategory> vatlist = new List<VatCategory>();
                    vatlist = vat.GetAllVat().ToList();
                    return PartialView("_VatcategoryEdit", vat);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditVatCategory(VatCategory vat)
        {
            VatCategory obj = new VatCategory();
            obj.update(vat);
            return RedirectToAction("VatCategoryIndex");
        }

        public JsonResult Delete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "VATCategory").FirstOrDefault().DeletePermission)
                {
                    VatCategory obj = new VatCategory();
                    var result = true;
                    obj.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
    }

}
