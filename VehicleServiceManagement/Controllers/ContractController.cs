﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using VehicleServiceManagement.Models;

namespace VehicleServiceManagement.Controllers
{
    public class ContractController : Controller
    {

        private IMemoryCache _cache;
        private static string Guid;
        public static string[] status = new string[] { "Pending","FinanceApproved", "Rejected","Autherized","Cancelled" };
        private readonly IHostingEnvironment hostingEnvironment;

        public ContractController(IMemoryCache memoryCache, IHostingEnvironment hostingEnvironment)
        {
            _cache = memoryCache;
            this.hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }
        public PartialViewResult _partialContractLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }

        public JsonResult GetDealerBranch(int dealerid)
        {
            DealerBranch objbr = new DealerBranch();
            List<DealerBranch> branchlist = objbr.GetAllBranchByDealer(dealerid);
            SelectList obgbranchlist = new SelectList(branchlist, "Id", "BranchName", 0);
            return Json(obgbranchlist);
        }

        public JsonResult ChangeProductGroup(int productGroup)
        {
            ProductSubGroup objsub = new ProductSubGroup();
            List<ProductSubGroup> objsublist = objsub.GetSubGroupByProductGroup(productGroup);
            SelectList obgbranchlist = new SelectList(objsublist, "Id", "SubGroup", 0);

            MakeDetail objmake = new MakeDetail();
            List<MakeDetail> objmakelist = objmake.GetAllMakeByProduct(productGroup);
            SelectList objmklist = new SelectList(objmakelist, "IdCategory", "Make");

            List<SelectList> response = new List<SelectList>();
            response.Add(obgbranchlist);
            response.Add(objmklist);

            return Json(response);
        }

        public JsonResult ChangeDuration(int durationid, int dealerId)
        {
            MileageCutoffInformation objcut = new MileageCutoffInformation();
            List<MileageCutoffInformation> objcutlist = objcut.GetAllMileageCutoffBylist(durationid, dealerId);
            SelectList obcutlist = new SelectList(objcutlist, "Id", "MilageCutoff", 0);
            return Json(obcutlist);
        }

        public JsonResult changeMake(int makeid,int productgroupId,int productsubgroup,int CategoryId)
        {
            ModelDetail objmod = new ModelDetail();
            List<ModelDetail> objmodlist = objmod.GetModelByMake(makeid);
            SelectList modellist = new SelectList(objmodlist, "Id", "ModelName", 0);

            ManufactureCutOffInformation objman = new ManufactureCutOffInformation();
            List<ManufactureCutOffInformation> objmanlist = objman.GetbymakeandPrdctgroup(makeid, productgroupId);
            SelectList objdurationlist = new SelectList(objmanlist, "Id", "Duration", 0);
            SelectList objmilagelist = new SelectList(objmanlist, "Id", "ManufacturerCutoff", 0);

            Durations objdur = new Durations();
            List<Durations> objdurlist = objdur.Getbylist(CategoryId, productsubgroup);
            SelectList durationlist = new SelectList(objdurlist, "Id", "Duration", 0);

            List<SelectList> response = new List<SelectList>();
            response.Add(modellist);
            response.Add(objdurationlist);
            response.Add(objmilagelist);
            response.Add(durationlist);
            return Json(response);
        }


        public JsonResult ChangeOtherService(int WorkshopId, int OtherServiceId)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            List<OtherServicePremiumDetails> objval = obj.GetOtherService();
            SelectList response = new SelectList(objval, "WorkShopId", "WorkShop", 0);
            return Json(response);
        }


        public JsonResult ChangeWorkshop(int WorkshopId, int OtherServiceId)
       {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            List<OtherServicePremiumDetails> objval = obj.GetWorkshop();
            SelectList response = new SelectList(objval, "WorkShopBranchId", "WorkShopBranch", 0);
            return Json(response);
        }


        public JsonResult ChangeBranch(int BranchId, int WorkshopId, int OtherServiceId)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            List<OtherServicePremiumDetails> objval = obj.GetBranch();
            SelectList response = new SelectList(objval, "CategoryId", "CategoryName", 0);
            return Json(response);
        }

        public JsonResult ChangeCategory(int CategoryId, int BranchId, int WorkshopId, int OtherServiceId)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            List<OtherServicePremiumDetails> objval = obj.GetCategory();
            SelectList response = new SelectList(objval, "SubGroupId", "SubGroup", 0);
            return Json(response);
        }
             
        public JsonResult ChangeProductSubgroup(int ProductSubgroupId, int CategoryId,int BranchId, int WorkshopId, int OtherServiceId)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;    
            List<OtherServicePremiumDetails> objval = obj.GetProductSubGroup();
            SelectList response = new SelectList(objval, "CCRangeId", "Range", 0);
            return Json(response);
        }



        public JsonResult ChangeCCRange( int CcRangeId,int ProductSubgroupId, int CategoryId, 
                                            int BranchId, int WorkshopId, int OtherServiceId)
        {

            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;
            obj.CCRangeId = CcRangeId;

            List<OtherServicePremiumDetails> objval = obj.GetCCRange();
            SelectList response = new SelectList(objval, "MaDurationId", "MaDurationId", 0);

            return Json(response);
        }


        public JsonResult ChangeManDuration( int ManDuration, int CcRangeId,
                                              int ProductSubgroupId, int CategoryId, int BranchId, int WorkshopId, int OtherServiceId)
        {

            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;
            obj.CCRangeId = CcRangeId;
            obj.MaDurationId = ManDuration;

            List<OtherServicePremiumDetails> objval = obj.GetManDuration();
            SelectList response = new SelectList(objval, "MaMilegeCuttoffId", "MaMilegeCuttoffId", 0);

            return Json(response);
        }


        public JsonResult ChangeManuCuttOff(int ManCuttoff, int ManDuration, int CcRangeId,
                                             int ProductSubgroupId, int CategoryId, int BranchId, int WorkshopId, int OtherServiceId)
        {

             OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;
            obj.CCRangeId = CcRangeId;
            obj.MaDurationId = ManDuration;
            obj.MaMilegeCuttoffId = ManCuttoff;

            List<OtherServicePremiumDetails> objval = obj.GetManuCuttOff();
            SelectList response = new SelectList(objval, "ExtendedDurationId", "ExtendedDurationId", 0);

            return Json(response);
        }


        public JsonResult ChangeDurationOther(int DurationId, int ManCuttoff, int ManDuration, int CcRangeId,
                                               int ProductSubgroupId, int CategoryId, int BranchId, int WorkshopId, int OtherServiceId)
        {

            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;
            obj.CCRangeId = CcRangeId;
            obj.MaDurationId = ManDuration;
            obj.MaMilegeCuttoffId = ManCuttoff;
            obj.ExtendedDurationId = DurationId;

            List<OtherServicePremiumDetails> objval = obj.GetDuration();
            SelectList response = new SelectList(objval, "ExtendedMilageCuttoffId", "ExtendedMilageCuttoffId", 0);
           
            return Json(response);
        }



        public JsonResult ChangeMilageCuttoff(int mcutoff,int DurationId,int ManCuttoff,int ManDuration,int CcRangeId,
                                               int ProductSubgroupId,int CategoryId,int BranchId,int WorkshopId,int OtherServiceId)
        {

            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.OtherServiceId = OtherServiceId;
            obj.WorkShopId = WorkshopId;
            obj.WorkShopBranchId = BranchId;
            obj.CategoryId = CategoryId;
            obj.SubGroupId = ProductSubgroupId;
            obj.CCRangeId = CcRangeId;
            obj.MaDurationId = ManDuration;
            obj.MaMilegeCuttoffId = ManCuttoff;
            obj.ExtendedDurationId = DurationId;
            obj.ExtendedMilageCuttoffId = mcutoff;

            OtherServicePremiumDetails objval = obj.GetMilageCuttoff();  
            List<float> response = new List<float>();
            response.Add(objval.MinPremiumAmount);
            response.Add(objval.PremiumAmount);           
            return Json(response);
        }



        public IActionResult ContractInformationCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault().InsertPermission)
                {
                    ContractManagement cnt = new ContractManagement();
                    long contarctnumber = cnt.GetContarctNumber();
                    ViewBag.cntnumber = contarctnumber;
                    DealerInformation objdealer = new DealerInformation();
                    List<DealerInformation> dealervalue = objdealer.GetAllDealesInformation();
                    ViewBag.dealer = new SelectList(dealervalue, "Id", "Name");

                    ProductGroups objgroup = new ProductGroups();
                    List<ProductGroups> grouplist = objgroup.GetAllProductGroups();
                    ViewBag.productgroup = new SelectList(grouplist, "Id", "ProductGroup");

                    ProductSubGroup objsubgroup = new ProductSubGroup();
                    List<ProductSubGroup> objsubgrouplist = objsubgroup.GetAllProductSubGroup();
                    ViewBag.subgroup = new SelectList(objsubgrouplist, "Id", "SubGroup");

                    List<ModelYear> yearlist = new List<ModelYear>();
                    for (int i = 20; i >= 0; i--)
                    {
                        ModelYear obj = new ModelYear();
                        obj.Modelyear = Convert.ToString(DateTime.Now.Year - i);
                        yearlist.Add(obj);
                    }
                    ViewBag.mdlyear = new SelectList(yearlist, "Modelyear", "Modelyear");

                    CCRange obj11 = new CCRange();
                    var c11 = obj11.GetAllCCRange();
                    ViewBag.ccrange = new SelectList(c11, "Id", "Range");

                    ClaimLimits obj15 = new ClaimLimits();
                    var c15 = obj15.GetAllClaimLimits();
                    ViewBag.limit = new SelectList(c15, "Id", "ClaimLimit");


                    OtherServiceDetails obj14 = new OtherServiceDetails();
                    var c14 = obj14.GetAllOtherService();
                    ViewBag.other = new SelectList(c14, "Id", "OtherService");

                    SelectList list1 = new SelectList(status);
                    ViewBag.ss = list1;

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        [HttpPost]
        public IActionResult ContractInformationCreate(ContractManagement objcntrct)
        {
            contractinformationinsert(objcntrct);
            ViewBag.ContractManagement = objcntrct;
            return RedirectToAction("ContractInformationIndex");
        }

        [HttpPost]
        public IActionResult ContractSavePrint(ContractManagement objcntrct)
        {            
            ClsContractPrint obj = new ClsContractPrint();
            ClsContractPrint objprint = obj.GetPrint(contractinformationinsert(objcntrct));
            ViewBag.Value = objprint;
            return PartialView("_ContractPrint");
        }


        public IActionResult ContractPrint(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault().PrintPermission)
                {
                    ClsContractPrint obj = new ClsContractPrint();

                    ClsContractPrint objprint = obj.GetPrint(id);
                    ViewBag.Value = objprint;
                    return View("_ContractPrint");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public int contractinformationinsert(ContractManagement objcntrct)
        {
            ContractManagement obj = new ContractManagement();
            int masterid = obj.CreateMaster(objcntrct);

            List<OtherService> PrdetailList = new List<OtherService>();
            if (_cache.Get<List<OtherService>>("otherservice" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<OtherService>>("otherservice" + Guid);
            }
            if (PrdetailList.Count > 0)
            {
                obj.CreateDetails(PrdetailList, objcntrct.contractInformation.ContractNumber);
            }
            return masterid;
        }

        //public IActionResult EditContractInformation(int id)
        //{
        //    ContractInformation obj1 = new ContractInformation();

        //    ContractManagement cnt = new ContractManagement();
        //    long contarctnumber = cnt.GetContarctNumber();
        //    ViewBag.cntnumber = contarctnumber;
        //    DealerInformation objdealer = new DealerInformation();
        //    List<DealerInformation> dealervalue = objdealer.GetAllDealesInformation();
        //    ViewBag.dealer = new SelectList(dealervalue, "Id", "Name");

        //    ProductGroups objgroup = new ProductGroups();
        //    List<ProductGroups> grouplist = objgroup.GetAllProductGroups();
        //    ViewBag.productgroup = new SelectList(grouplist, "Id", "ProductGroup");

        //    ProductSubGroup objsubgroup = new ProductSubGroup();
        //    List<ProductSubGroup> objsubgrouplist = objsubgroup.GetAllProductSubGroup();
        //    ViewBag.subgroup = new SelectList(objsubgrouplist, "Id", "SubGroup");

        //    List<ModelYear> yearlist = new List<ModelYear>();
        //    for (int i = 20; i >= 0; i--)
        //    {
        //        ModelYear obj = new ModelYear();
        //        obj.Modelyear = Convert.ToString(DateTime.Now.Year - i);
        //        yearlist.Add(obj);
        //    }
        //    ViewBag.mdlyear = new SelectList(yearlist, "Modelyear", "Modelyear");

        //    CCRange obj11 = new CCRange();
        //    var c11 = obj11.GetAllCCRange();
        //    ViewBag.ccrange = new SelectList(c11, "Id", "Range");

        //    ClaimLimits obj15 = new ClaimLimits();
        //    var c15 = obj15.GetAllClaimLimits();
        //    ViewBag.limit = new SelectList(c15, "Id", "ClaimLimit");


        //    OtherServiceDetails obj14 = new OtherServiceDetails();
        //    var c14 = obj14.GetAllOtherService();
        //    ViewBag.other = new SelectList(c14, "Id", "OtherService");

        //    SelectList list1 = new SelectList(status);
        //    ViewBag.ss = list1;



        //    ContractInformation contract;
        //    contract = obj1.details(id);
        //    return View(contract);
        //}

        public IActionResult ContractInformationIndex()
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault().DisplayPermission)
                {
                    ContractManagement contract = new ContractManagement();
                    ModelDetail info = new ModelDetail();
                    var c = info.GetAllModelDetail();
                    ViewBag.modelname = new SelectList(c, "Id", "ModelName");
                    ContractManagementDisplay displayList = new ContractManagementDisplay();
                    displayList.contractList = contract.GetAllContractInformation();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult DeleteContractInformation(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault().DeletePermission)
                {
                    ContractManagement obj1 = new ContractManagement();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        public JsonResult RemoveOtherService(int otherserviceId,string UID)
        {
            List<OtherService> otherservicedtlsList = new List<OtherService>();
            if (_cache.Get<List<OtherService>>("otherservice" + Guid) != null)
            {
                otherservicedtlsList = _cache.Get<List<OtherService>>("otherservice" + Guid);
            }

            OtherService val = otherservicedtlsList.Where(a => a.OtherServiceId == otherserviceId).FirstOrDefault();
            if (val != null)
            {
                otherservicedtlsList.Remove(val);
            }

            _cache.Set<List<OtherService>>("otherservice" + UID, otherservicedtlsList);
            String value = null;

            value = @" <thead><tr><th>Delete</th><th>Other Service</th><th>Workshop</th><th>Manu Duration</th>
                       <th>Manu CutOff</th><th>Duration</th><th>Mileage CutOff</th><th>Quantity</th><th>Min Premium</th><th>Premium</th></tr></thead>";


            foreach (var item in otherservicedtlsList)
            {
                value += @"<tr id=""" + item.OtherServiceId + @""">";
                value += @" <td><a class=""fa fa-remove"" href=""javascript: void(0); ""></a></td>
                            <td>" + item.OtherServiceName + @"</td>
                            <td>" + item.Workshop + @"</td> 
                            <td>" + item.ManuDuration + @"</td>
                            <td>" + item.ManuCuttoff + @"</td>
                            
                            <td>" + item.Duration + @"</td>
                            <td>" + item.MilageCuttoff + @"</td>
                            <td>" + item.Quantity + @"</td>
                            <td>" + item.MinPremium + @"</td>
                            <td>" + item.Premium + @"</td>                           
                            </tr>";

            }
            StringBuilder str = new StringBuilder();
            str.Append(value);
            return Json(new { data = str.ToString() });
        }

        public JsonResult setOtherService(OtherService otherservicedtls)
        {

            OtherService ob = new OtherService();
            StringBuilder str = new StringBuilder();
            Guid = otherservicedtls.UID;
            List<OtherService> otherservicedtlsList = new List<OtherService>();
            if (_cache.Get<List<OtherService>>("otherservice" + Guid) != null)
            {
                otherservicedtlsList = _cache.Get<List<OtherService>>("otherservice" + Guid);
            }


            if (otherservicedtlsList.Count == 0)
            {
                otherservicedtlsList = new List<OtherService>();
                otherservicedtlsList.Add(otherservicedtls);
            }
            else
            {
                OtherService val = otherservicedtlsList.Where(a => a.OtherServiceId == otherservicedtls.OtherServiceId).FirstOrDefault();
                if (val != null)
                {
                    otherservicedtlsList.Remove(val);
                }
                otherservicedtlsList.Add(otherservicedtls);
            }

            _cache.Set<List<OtherService>>("otherservice" + otherservicedtls.UID, otherservicedtlsList);
            String value = null;

            value = @" <thead><tr><th>Delete</th><th>Other Service</th><th>Workshop</th><th>Manu Duration</th>
                       <th>Manu CutOff</th><th>Duration</th><th>Mileage CutOff</th><th>Quantity</th><th>Min Premium</th><th>Premium</th></tr></thead>";


            foreach (var item in otherservicedtlsList)
            {
                value += @"<tr id=""" + item.OtherServiceId + @""">";
                value += @" <td><a class=""fa fa-remove"" href=""javascript: void(0); ""></a></td>
                            <td>" + item.OtherServiceName + @"</td>
                            <td>" + item.Workshop + @"</td> 
                            <td>" + item.ManuDuration + @"</td>
                            <td>" + item.ManuCuttoff + @"</td>
                            
                            <td>" + item.Duration + @"</td>
                            <td>" + item.MilageCuttoff + @"</td>
                            <td>" + item.Quantity + @"</td>
                            <td>" + item.MinPremium + @"</td>
                            <td>" + item.Premium + @"</td>                           
                            </tr>";

            }
            str.Append(value);
            return Json(new { data = str.ToString() });

        }

       


    }
}