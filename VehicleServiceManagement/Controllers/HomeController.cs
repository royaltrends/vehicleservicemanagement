﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AccounTechSoftware.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VehicleServiceManagement.Models;

namespace VehicleServiceManagement.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("Login");
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]

        public ActionResult CreateLogin(Login log)
        {

            LoginInformation obj = new LoginInformation();
            LoginInformation objval = obj.GetAllLogin(log);

            if (objval.UserName == null)
            {
                ViewBag.ErrorMessage = "Incorrect Username or Password";
                return View("Login");
            }
            else
            {              
                HttpContext.Session.SetInt32("EmployeeId", objval.EmployeeId);
                HttpContext.Session.SetString("CompanyName", objval.CompanyName);
                HttpContext.Session.SetString("username", objval.UserName);
                HttpContext.Session.SetInt32("UserId", objval.UserId);
                if (UserPermission.permissionList == null)
                {
                    UserPermission.permissionList = new List<UserPermission>();
                }
                UserPermission.permissionList.RemoveAll(a => a.UserId == objval.UserId || a.UserId == 0);
                UserPermission _user = new UserPermission();
                List<UserPermission> pList = new List<UserPermission>();
                pList = _user.GetUserPermissions(objval.UserId);
                UserPermission.permissionList.AddRange(pList);
               
                return RedirectToAction("Dashboard", "Home");
            }
        }
        public IActionResult Dashboard()
        {
            ContractInformation objcntrct = new ContractInformation();
            Premium objprm = new Premium();
            OtherServicePremiumDetails objother = new OtherServicePremiumDetails();

            ViewBag.Contract = objcntrct.getcount();
            ViewBag.Premium = objprm.getcount(); ;
            ViewBag.Other = objother.getcount();
            return View();
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        } 
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
