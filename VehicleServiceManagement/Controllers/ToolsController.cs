﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using VehicleServiceManagement.Models;

namespace VehicleServiceManagement.Controllers
{
    public class ToolsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public PartialViewResult _partialToolLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }
        public IActionResult OtherServiceDetailss(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Contract").FirstOrDefault().DisplayPermission)
                {
                    OtherServiceDetails obj = new OtherServiceDetails();
                    OtherServiceDetails dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult OtherServiceDetailsIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherService").FirstOrDefault().DisplayPermission)
                {
                    OtherServiceDetails service = new OtherServiceDetails();
                    OtherServiceDetailsDisplay displayList = new OtherServiceDetailsDisplay();
                    displayList.otherServiceList = service.GetAllOtherService();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherService").FirstOrDefault();
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult OtherServiceDetailsCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherService").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult OtherServiceDetailsCreate(OtherServiceDetails service)
        {

            OtherServiceDetails obj = new OtherServiceDetails();
            obj.create(service);
            return RedirectToAction("OtherServiceDetailsIndex");
        }

        public IActionResult EditOtherServiceDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherService").FirstOrDefault().UpdatePermission)
                {
                    OtherServiceDetails obj = new OtherServiceDetails();
                    OtherServiceDetails dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditOtherServiceDetails(OtherServiceDetails dealer)
        {
            OtherServiceDetails obj = new OtherServiceDetails();

            obj.update(dealer);
            return RedirectToAction("OtherServiceDetailsIndex");
        }
        public JsonResult DeleteOtherServiceDetails(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherService").FirstOrDefault().DeletePermission)
                {
                    OtherServiceDetails obj1 = new OtherServiceDetails();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult ManufactureCutOffInformationDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault().DisplayPermission)
                {
                    ManufactureCutOffInformation obj = new ManufactureCutOffInformation();
                    MakeDetail info = new MakeDetail();
                    var c = info.GetAllMakeDetail();
                    ViewBag.make = new SelectList(c, "Id", "Make");
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ManufactureCutOffInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ManufactureCutOffInformationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault().DisplayPermission)
                {
                    ManufactureCutOffInformation service = new ManufactureCutOffInformation();
                    MakeDetail info = new MakeDetail();
                    var c = info.GetAllMakeDetail();
                    ViewBag.make = new SelectList(c, "Id", "Make");
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ManufactureCutoffInformationDisplay displayList = new ManufactureCutoffInformationDisplay();
                    displayList.cutoffList = service.GetAllManufacturCutoff();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult ManufactureCutOffInformationCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault().InsertPermission)
                {
                    MakeDetail info = new MakeDetail();
                    var c = info.GetAllMakeDetail();
                    ViewBag.make = new SelectList(c, "Id", "Make");
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ManufactureCutOffInformationCreate(ManufactureCutOffInformation service)
        {

            ManufactureCutOffInformation obj = new ManufactureCutOffInformation();
            obj.create(service);
            return RedirectToAction("ManufactureCutOffInformationIndex");
        }

        public IActionResult EditManufactureCutOffInformation(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault().UpdatePermission)
                {
                    ManufactureCutOffInformation obj = new ManufactureCutOffInformation();
                    MakeDetail info = new MakeDetail();
                    var c = info.GetAllMakeDetail();
                    ViewBag.make = new SelectList(c, "Id", "Make");
                    ProductGroups info1 = new ProductGroups();
                    var c1 = info1.GetAllProductGroups();
                    ViewBag.Product = new SelectList(c1, "Id", "ProductGroup");
                    ManufactureCutOffInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditManufactureCutOffInformation(ManufactureCutOffInformation dealer)
        {
            ManufactureCutOffInformation obj = new ManufactureCutOffInformation();

            obj.update(dealer);
            return RedirectToAction("ManufactureCutOffInformationIndex");
        }
        public JsonResult DeleteManufactureCutOffInformation(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ManufacturerCutoff").FirstOrDefault().DeletePermission)
                {
                    ManufactureCutOffInformation obj1 = new ManufactureCutOffInformation();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        public IActionResult MileageCutoffInformationdetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault().DisplayPermission)
                {
                    MileageCutoffInformation obj = new MileageCutoffInformation();

                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    DealerInformation infos = new DealerInformation();
                    var c2 = infos.GetAllDealesInformation();
                    ViewBag.inf = new SelectList(c2, "Id", "Name");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    MileageCutoffInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult MileageCutoffInformationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault().DisplayPermission)
                {
                    MileageCutoffInformation dealer = new MileageCutoffInformation();
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    DealerInformation infos = new DealerInformation();
                    var c2 = infos.GetAllDealesInformation();
                    ViewBag.inf = new SelectList(c2, "Id", "Name");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    MileageCutoffInformationDisplay displayList = new MileageCutoffInformationDisplay();
                    displayList.mileageCutoffList = dealer.GetAllMileageCutoffInformation();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult MileageCutoffInformationCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault().InsertPermission)
                {
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    DealerInformation infos = new DealerInformation();
                    var c2 = infos.GetAllDealesInformation();
                    ViewBag.inf = new SelectList(c2, "Id", "Name");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult MileageCutoffInformationCreate(MileageCutoffInformation branch)
        {

            MileageCutoffInformation obj = new MileageCutoffInformation();
            obj.create(branch);
            return RedirectToAction("MileageCutoffInformationIndex");
        }

        public IActionResult EditMileageCutoffInformation(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault().UpdatePermission)
                {
                    MileageCutoffInformation obj = new MileageCutoffInformation();
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    DealerInformation infos = new DealerInformation();
                    var c2 = infos.GetAllDealesInformation();
                    ViewBag.inf = new SelectList(c2, "Id", "Name");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    MileageCutoffInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditMileageCutoffInformation(MileageCutoffInformation dealer)
        {
            MileageCutoffInformation obj = new MileageCutoffInformation();

            obj.update(dealer);
            return RedirectToAction("MileageCutoffInformationIndex");
        }
        public JsonResult DeleteMileageCutoffInformation(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "MileageCutoff").FirstOrDefault().DeletePermission)
                {
                    MileageCutoffInformation obj1 = new MileageCutoffInformation();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        public IActionResult OtherServicePremiumDetailss(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault().DisplayPermission)
                {
                    OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                    OtherServiceDetails ser = new OtherServiceDetails();
                    var c7 = ser.GetAllOtherService();
                    ViewBag.service = new SelectList(c7, "Id", "OtherService");
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    OtherServicePremiumDetails dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult OtherServicePremiumDetailsIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault().DisplayPermission)
                {
                    OtherServicePremiumDetails dealer = new OtherServicePremiumDetails();
                    OtherServiceDetails ser = new OtherServiceDetails();
                    var c7 = ser.GetAllOtherService();
                    ViewBag.service = new SelectList(c7, "Id", "OtherService");

                    DealerInformation inf = new DealerInformation();
                    var c8 = inf.GetAllDealerdetails();
                    ViewBag.info = new SelectList(c8, "Id", "Name");
                    //DealerBranch branch = new DealerBranch();
                    //var c9 = branch.GetAllDealerbanches();
                    //ViewBag.br = new SelectList(c9, "Id", "BranchName");


                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");

                    OtherServicePremiumDetailsDisplay displayList = new OtherServicePremiumDetailsDisplay();
                    displayList.otherServicePremiumList = dealer.GetAllOtherServicePremiumDetails();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault();
                    return View();

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult OtherServicePremiumDetailsCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault().InsertPermission)
                {
                    OtherServiceDetails ser = new OtherServiceDetails();
                    var c7 = ser.GetAllOtherService();
                    ViewBag.service = new SelectList(c7, "Id", "OtherService");

                    DealerInformation inf = new DealerInformation();
                    var c8 = inf.GetAllDealerdetails();
                    ViewBag.info = new SelectList(c8, "Id", "Name");
                    //DealerBranch branch = new DealerBranch();
                    //var c9 = branch.GetAllDealerbanches();
                    //ViewBag.br = new SelectList(c9, "Id", "BranchName");


                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult OtherServicePremiumDetailsCreate(OtherServicePremiumDetails branch)
        {

            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            obj.create(branch);
            return RedirectToAction("OtherServicePremiumDetailsIndex");
        }

        public IActionResult EditOtherServicePremiumDetailsIndex(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault().UpdatePermission)
                {
                    OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                    OtherServiceDetails ser = new OtherServiceDetails();
                    var c7 = ser.GetAllOtherService();
                    ViewBag.service = new SelectList(c7, "Id", "OtherService");

                    DealerInformation inf = new DealerInformation();
                    var c8 = inf.GetAllDealerdetails();
                    ViewBag.info = new SelectList(c8, "Id", "Name");

                    //DealerBranch branch = new DealerBranch();
                    //var c9 = branch.GetAllDealerbanches();
                    //ViewBag.br = new SelectList(c9, "Id", "BranchName");


                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    OtherServicePremiumDetails dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditOtherServicePremiumDetailsIndex(OtherServicePremiumDetails dealer)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();

            obj.update(dealer);
            return RedirectToAction("OtherServicePremiumDetailsIndex");
        }
        public JsonResult DeleteOtherServicePremiumDetails(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "OtherServicePremium").FirstOrDefault().DeletePermission)
                {
                    OtherServicePremiumDetails obj1 = new OtherServicePremiumDetails();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult EntryEligibilityDeatils(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().DisplayPermission)
                {
                    EntryEligibility obj = new EntryEligibility();
                    ProductSubGroup info1 = new ProductSubGroup();
                    var c1 = info1.GetAllProductSubGroup();
                    ViewBag.Product = new SelectList(c1, "Id", "SubGroup");
                    EntryEligibility dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult EntryEligibilityIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().DisplayPermission)
                {
                    EntryEligibility service = new EntryEligibility();
                    ProductSubGroup info1 = new ProductSubGroup();
                    var c1 = info1.GetAllProductSubGroup();
                    ViewBag.Product = new SelectList(c1, "Id", "SubGroup");
                    EntryEligibilityDisplay displayList = new EntryEligibilityDisplay();
                    displayList.entryEligibilityList = service.GetAllEntryEligibility();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult EntryEligibilityCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().InsertPermission)
                {
                    ProductSubGroup info1 = new ProductSubGroup();
                    var c1 = info1.GetAllProductSubGroup();
                    ViewBag.Product = new SelectList(c1, "Id", "SubGroup");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EntryEligibilityCreate(EntryEligibility service)
        {

            EntryEligibility obj = new EntryEligibility();
            obj.create(service);
            return RedirectToAction("EntryEligibilityIndex");
        }

        public IActionResult EditEntryEligibility(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().UpdatePermission)
                {
                    EntryEligibility obj = new EntryEligibility();
                    ProductSubGroup info1 = new ProductSubGroup();
                    var c1 = info1.GetAllProductSubGroup();
                    ViewBag.Product = new SelectList(c1, "Id", "SubGroup");
                    EntryEligibility dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditEntryEligibility(EntryEligibility dealer)
        {
            EntryEligibility obj = new EntryEligibility();

            obj.update(dealer);
            return RedirectToAction("EntryEligibilityIndex");
        }
        public JsonResult DeleteEntryEligibility(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().DeletePermission)
                {
                    EntryEligibility obj1 = new EntryEligibility();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult EntryEligibilityDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EntryEligibility").FirstOrDefault().DisplayPermission)
                {
                    EntryEligibility obj = new EntryEligibility();
                    EntryEligibility vendor;
                    vendor = obj.details(id);
                    return View(vendor);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CCRangeDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault().DisplayPermission)
                {
                    CCRange obj = new CCRange();
                    CCRange ccrange;
                    ccrange = obj.details(id);

                    CategoryDetail obj2 = new CategoryDetail();
                    var c1 = obj2.GetAllCategoryDetail();
                    ViewBag.cc = new SelectList(c1, "Id", "CategoryName");

                    DealerInformation obj1 = new DealerInformation();
                    var c2 = obj1.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    return View(ccrange);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CCRangeCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault().InsertPermission)
                {
                    CategoryDetail obj = new CategoryDetail();
                    var c1 = obj.GetAllCategoryDetail();
                    ViewBag.cc = new SelectList(c1, "Id", "CategoryName");

                    DealerInformation obj1 = new DealerInformation();
                    var c2 = obj1.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult CCRangeCreate(CCRange ccrange)
        {

            CCRange obj = new CCRange();
            obj.create(ccrange);
            return RedirectToAction("CCRangeIndex");
        }

        public IActionResult CCRangeEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault().UpdatePermission)
                {
                    CCRange obj = new CCRange();
                    CCRange ccrange;
                    ccrange = obj.details(id);

                    CategoryDetail obj2 = new CategoryDetail();
                    var c1 = obj2.GetAllCategoryDetail();
                    ViewBag.cc = new SelectList(c1, "Id", "CategoryName");

                    DealerInformation obj1 = new DealerInformation();
                    var c2 = obj1.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    return View(ccrange);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult CCRangeEdit(CCRange ccrange)
        {

            CCRange obj = new CCRange();
            obj.update(ccrange);
            return RedirectToAction("CCRangeIndex");
        }

        public JsonResult DeleteCCRange(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault().DeletePermission)
                {
                    CCRange obj1 = new CCRange();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult CCRangeIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj2 = new CategoryDetail();
                    var c1 = obj2.GetAllCategoryDetail();
                    ViewBag.cc = new SelectList(c1, "Id", "CategoryName");

                    DealerInformation obj1 = new DealerInformation();
                    var c2 = obj1.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");
                    CCRange obj = new CCRange();

                    CCRangeDisplay displayList = new CCRangeDisplay();
                    displayList.ccrangeList = obj.GetAllCCRange();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "CCRange").FirstOrDefault();
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult ClaimLimitsdetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault().DisplayPermission)
                {
                    ProductSubGroup obj1 = new ProductSubGroup();
                    var c1 = obj1.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c1, "Id", "SubGroup");

                    DealerInformation obj2 = new DealerInformation();
                    var c2 = obj2.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    ClaimLimits obj = new ClaimLimits();
                    ClaimLimits climit;
                    climit = obj.details(id);

                    return View(climit);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult ClaimLimitsCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault().InsertPermission)
                {
                    ProductSubGroup obj1 = new ProductSubGroup();
                    var c1 = obj1.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c1, "Id", "SubGroup");

                    DealerInformation obj2 = new DealerInformation();
                    var c2 = obj2.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ClaimLimitsCreate(ClaimLimits climit)
        {

            ClaimLimits obj = new ClaimLimits();
            obj.create(climit);
            return RedirectToAction("ClaimLimitsIndex");
        }

        public IActionResult ClaimLimitsEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault().UpdatePermission)
                {
                    ProductSubGroup obj1 = new ProductSubGroup();
                    var c1 = obj1.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c1, "Id", "SubGroup");

                    DealerInformation obj2 = new DealerInformation();
                    var c2 = obj2.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");


                    ClaimLimits obj = new ClaimLimits();
                    ClaimLimits climit;
                    climit = obj.details(id);

                    return View(climit);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult ClaimLimitsEdit(ClaimLimits climit)
        {

            ClaimLimits obj = new ClaimLimits();
            obj.update(climit);
            return RedirectToAction("ClaimLimitsIndex");
        }

        public JsonResult DeleteClaimLimits(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault().DeletePermission)
                {
                    ClaimLimits obj1 = new ClaimLimits();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult ClaimLimitsIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault().DisplayPermission)
                {
                    ProductSubGroup obj1 = new ProductSubGroup();
                    var c1 = obj1.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c1, "Id", "SubGroup");

                    DealerInformation obj2 = new DealerInformation();
                    var c2 = obj2.GetAllDealesInformation();
                    ViewBag.di = new SelectList(c2, "Id", "Name");

                    ClaimLimits obj = new ClaimLimits();

                    ClaimLimitsDisplay displayList = new ClaimLimitsDisplay();
                    displayList.claimLimitsList = obj.GetAllClaimLimits();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ClaimLimit").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult DurationInformationCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault().InsertPermission)
                {
                    CategoryDetail obj1 = new CategoryDetail();
                    var c1 = obj1.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductSubGroup obj2 = new ProductSubGroup();
                    var c2 = obj2.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c2, "Id", "SubGroup");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult DurationInformationCreate(Durations duration)
        {
            Durations obj = new Durations();
            obj.create(duration.objval);
            return RedirectToAction("DurationInformationIndex");
        }

        public IActionResult DurationInformationEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault().UpdatePermission)
                {
                    CategoryDetail obj1 = new CategoryDetail();
                    var c1 = obj1.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductSubGroup obj2 = new ProductSubGroup();
                    var c2 = obj2.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c2, "Id", "SubGroup");

                    Durations obj = new Durations();
                    Durations duration;
                    duration = obj.details(id);

                    return View(duration);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult DurationInformationEdit(Durations duration)
        {

            Durations obj = new Durations();
            obj.update(duration.objval);
            return RedirectToAction("DurationInformationIndex");

        }

        public JsonResult DeleteDurationInformation(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault().DeletePermission)
                {
                    Durations obj1 = new Durations();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult DurationInformationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj1 = new CategoryDetail();
                    var c1 = obj1.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductSubGroup obj2 = new ProductSubGroup();
                    var c2 = obj2.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c2, "Id", "SubGroup");

                    Durations obj = new Durations();
                    DurationsDisplay displayList = new DurationsDisplay();
                    displayList.durationsList = obj.GetAllDurations();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult DurationInformationDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExtendDuration").FirstOrDefault().DisplayPermission)
                {
                    CategoryDetail obj1 = new CategoryDetail();
                    var c1 = obj1.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c1, "Id", "CategoryName");

                    ProductSubGroup obj2 = new ProductSubGroup();
                    var c2 = obj2.GetAllProductSubGroup();
                    ViewBag.sg = new SelectList(c2, "Id", "SubGroup");

                    Durations obj = new Durations();
                    Durations duration;
                    duration = obj.details(id);
                    return View(duration);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult PremiumIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Premium").FirstOrDefault().DisplayPermission)
                {
                    Premium dealer = new Premium();
                    DealerInformation info1 = new DealerInformation();
                    var c8 = info1.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c8, "Id", "Name");
                    ClaimLimits ser = new ClaimLimits();
                    var c7 = ser.GetAllClaimLimits();
                    ViewBag.service = new SelectList(c7, "Id", "ClaimLimit");

                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");

                    return View(dealer.GetAllPremium());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult PremiumCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Premium").FirstOrDefault().InsertPermission)
                {
                    DealerInformation info1 = new DealerInformation();
                    var c8 = info1.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c8, "Id", "Name");
                    ClaimLimits ser = new ClaimLimits();
                    var c7 = ser.GetAllClaimLimits();
                    ViewBag.service = new SelectList(c7, "Id", "ClaimLimit");
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult PremiumCreate(Premium branch)
        {

            Premium obj = new Premium();
            obj.create(branch);
            return RedirectToAction("PremiumIndex");
        }

        public IActionResult PremiumEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Premium").FirstOrDefault().UpdatePermission)
                {
                    Premium obj = new Premium();
                    DealerInformation info1 = new DealerInformation();
                    var c8 = info1.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c8, "Id", "Name");
                    ClaimLimits ser = new ClaimLimits();
                    var c9 = ser.GetAllClaimLimits();
                    ViewBag.service = new SelectList(c9, "Id", "ClaimLimit");
                    //OtherServiceDetails sevr = new OtherServiceDetails();
                    //var c7 = serv.GetAllOtherService();
                    //ViewBag.service = new SelectList(c7, "Id", "OtherService");
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    Premium dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult PremiumEdit(Premium dealer)
        {
            Premium obj = new Premium();

            obj.update(dealer);
            return RedirectToAction("PremiumIndex");
        }
       
        public JsonResult PremiumDelete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Premium").FirstOrDefault().DeletePermission)
                {
                    Premium obj1 = new Premium();
                    var result = true;
                    obj1.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult PremiumDetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Premium").FirstOrDefault().DisplayPermission)
                {
                    DealerInformation info1 = new DealerInformation();
                    var c8 = info1.GetAllDealesInformation();
                    ViewBag.bk = new SelectList(c8, "Id", "Name");
                    ClaimLimits ser = new ClaimLimits();
                    var c7 = ser.GetAllClaimLimits();
                    ViewBag.service = new SelectList(c7, "Id", "ClaimLimit");
                    //OtherServiceDetails ser = new OtherServiceDetails();
                    //var c7 = ser.GetAllOtherService();
                    //ViewBag.service = new SelectList(c7, "Id", "OtherService");
                    CategoryDetail info = new CategoryDetail();
                    var c = info.GetAllCategoryDetail();
                    ViewBag.cat = new SelectList(c, "Id", "CategoryName");
                    ProductSubGroup prod = new ProductSubGroup();
                    var c1 = prod.GetAllProductSubGroup();
                    ViewBag.sub = new SelectList(c1, "Id", "SubGroup");
                    Durations ds = new Durations();
                    var c3 = ds.GetAllDurations();
                    ViewBag.du = new SelectList(c3, "Id", "Duration");
                    CCRange cc = new CCRange();
                    var c4 = cc.GetAllCCRange();
                    ViewBag.range = new SelectList(c4, "Id", "Range");
                    ManufactureCutOffInformation mx = new ManufactureCutOffInformation();
                    var c5 = mx.GetAllManufacturCutoff();
                    ViewBag.manu = new SelectList(c5, "Id", "ManufacturerCutoff");
                    MileageCutoffInformation mc = new MileageCutoffInformation();
                    var c6 = mc.GetAllMileageCutoffInformation();
                    ViewBag.mil = new SelectList(c6, "Id", "MilageCutoff");
                    Premium obj = new Premium();
                    Premium vendor;

                    vendor = obj.details(id);

                    return View(vendor);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

    }
}