﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using VehicleServiceManagement.Models;

namespace VehicleServiceManagement.Controllers
{
    public class ControlPanelController : Controller
    {
        public static string[] gender = new string[] { "Male", "Female", "Others" };
        public IActionResult Index()
        {
            return View();
        }
        public PartialViewResult _partialControlPanelLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }
        public IActionResult UserIndex()
        {         
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault().DisplayPermission)
                {
                    UserSettings obj = new UserSettings();
                    UserSettingsDisplay displayList = new UserSettingsDisplay();
                    displayList.userList = obj.GetAllUsers();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult UserSettingsdetails(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault().DisplayPermission)
                {
                    UserSettings obj = new UserSettings();
                    UserSettings user;
                    user = obj.details(id);
                    return View(user);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult UserSettingsCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult UserSettingsCreate(UserSettings user)
        {

            UserSettings obj = new UserSettings();
            obj.create(user);
            return RedirectToAction("UserIndex");
        }

        public IActionResult UserSettingsEdit(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault().UpdatePermission)
                {
                    UserSettings obj = new UserSettings();
                    UserSettings user;
                    user = obj.details(id);

                    return View(user);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult UserSettingsEdit(UserSettings user)
        {

            UserSettings obj = new UserSettings();
            obj.update(user);
            return RedirectToAction("UserIndex");
        }

        public JsonResult DeleteUserSettings(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "UserMaster").FirstOrDefault().DeletePermission)
                {
                    UserSettings obj = new UserSettings();
                    var result = true;
                    obj.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        public IActionResult DepartmentIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault().DisplayPermission)
                {
                    Department dealer = new Department();
                    DepartmentDisplay displayList = new DepartmentDisplay();
                    displayList.departmentList = dealer.GetAllDepartment();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CreateDepartment()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult CreateDepartment(Department department)
        {
            Department obj = new Department();
            obj.create(department);
            return RedirectToAction("DepartmentIndex");
        }
        public IActionResult EditDepartment(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault().UpdatePermission)
                {
                    Department obj = new Department();
                    Department dep;
                    dep = obj.details(id);
                    return View(dep);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditDepartment(Department department)
        {

            Department obj = new Department();

            obj.update(department);
            return RedirectToAction("DepartmentIndex");
        }

        public JsonResult Delete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault().DeletePermission)
                {
                    Department obj = new Department();
                    var result = true;
                    obj.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult DepartmentDeatils(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Department").FirstOrDefault().DisplayPermission)
                {
                    Department obj = new Department();
                    Department dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult DesignationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault().DisplayPermission)
                {
                    Designation dealer = new Designation();
                    DesignationDisplay displayList = new DesignationDisplay();
                    displayList.designationList = dealer.GetAllDesignation();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CreateDesignation()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault().InsertPermission)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult CreateDesignation(Designation department)
        {
            Designation obj = new Designation();
            obj.create(department);
            return RedirectToAction("DesignationIndex");
        }
        public IActionResult EditDesignation(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault().UpdatePermission)
                {
                    Designation obj = new Designation();
                    Designation dep;
                    dep = obj.details(id);
                    return View(dep);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditDesignation(Designation department)
        {

            Designation obj = new Designation();

            obj.update(department);
            return RedirectToAction("DesignationIndex");
        }

        public JsonResult DesignationDelete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault().DeletePermission)
                {
                    Designation obj = new Designation();
                    var result = true;
                    obj.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult DesignationDeatils(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "Designation").FirstOrDefault().DisplayPermission)
                {
                    Designation obj = new Designation();
                    Designation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
                     
        public IActionResult EmployeeBasicInformationIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault().DisplayPermission)
                {
                    EmployeeBasicInformation dealer = new EmployeeBasicInformation();
                    SelectList list1 = new SelectList(gender);
                    ViewBag.type = list1;
                    Designation ob2 = new Designation();
                    var designation = ob2.GetAllDesignation().ToList();
                    ViewBag.Designation = new SelectList(designation, "Id", "Name");
                    Department ob3 = new Department();
                    var department = ob3.GetAllDepartment().ToList();
                    ViewBag.Department = new SelectList(department, "Id", "Name");
                    EmployeeBasicInformationDisplay displayList = new EmployeeBasicInformationDisplay();
                    displayList.employeeList = dealer.GetEmployeeMaster();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult CreateEmployeeBasicInformation()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault().InsertPermission)
                {
                    SelectList list1 = new SelectList(gender);
                    ViewBag.type = list1;
                    Designation ob2 = new Designation();
                    var designation = ob2.GetAllDesignation().ToList();
                    ViewBag.Designation = new SelectList(designation, "Id", "Name");
                    Department ob3 = new Department();
                    var department = ob3.GetAllDepartment().ToList();
                    ViewBag.Department = new SelectList(department, "Id", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult CreateEmployeeBasicInformation(EmployeeBasicInformation Employee)
        {
            EmployeeBasicInformation obj = new EmployeeBasicInformation();
            obj.create(Employee);
            return RedirectToAction("EmployeeBasicInformationIndex");
        }
        public IActionResult EditEmployeeBasicInformation(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault().UpdatePermission)
                {
                    EmployeeBasicInformation obj = new EmployeeBasicInformation();
                    SelectList list1 = new SelectList(gender);
                    ViewBag.type = list1;
                    Designation ob2 = new Designation();
                    var designation = ob2.GetAllDesignation().ToList();
                    ViewBag.Designation = new SelectList(designation, "Id", "Name");
                    Department ob3 = new Department();
                    var department = ob3.GetAllDepartment().ToList();
                    ViewBag.Department = new SelectList(department, "Id", "Name");
                    EmployeeBasicInformation dep;
                    dep = obj.details(id);
                    return View(dep);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditEmployeeBasicInformation(EmployeeBasicInformation department)
        {

            EmployeeBasicInformation obj = new EmployeeBasicInformation();

            obj.update(department);
            return RedirectToAction("EmployeeBasicInformationIndex");
        }

        public JsonResult EmployeeBasicInformationDelete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault().UpdatePermission)
                {
                    EmployeeBasicInformation obj = new EmployeeBasicInformation();
                    var result = true;
                    obj.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }
        public IActionResult EmployeeBasicInformationDeatils(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeMaster").FirstOrDefault().DisplayPermission)
                {
                    EmployeeBasicInformation obj = new EmployeeBasicInformation();
                    SelectList list1 = new SelectList(gender);
                    ViewBag.type = list1;
                    Designation ob2 = new Designation();
                    var designation = ob2.GetAllDesignation().ToList();
                    ViewBag.Designation = new SelectList(designation, "Id", "Name");
                    Department ob3 = new Department();
                    var department = ob3.GetAllDepartment().ToList();
                    ViewBag.Department = new SelectList(department, "Id", "Name");
                    EmployeeBasicInformation dealer;
                    dealer = obj.details(id);
                    return View(dealer);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult WorkTaskIndex(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "TaskManagement").FirstOrDefault().DisplayPermission)
                {
                    WorkTask obj = new WorkTask();
                    List<WorkTask> objlist = obj.GetAllWorkTask();
                    WorkTaskDisplay displayList = new WorkTaskDisplay();
                    displayList.taskList = objlist;
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "TaskManagement").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult ChangeDepartment(int DepartmentId)
        {
            EmployeeBasicInformation obj = new EmployeeBasicInformation();
            obj.Department = DepartmentId;
            List<EmployeeBasicInformation> objval = obj.GetEmployeeMasterByDepartment();
            SelectList response = new SelectList(objval, "Id", "Name", 0);
            return Json(response);
        }

        public IActionResult CreateWorkTask(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "TaskManagement").FirstOrDefault().InsertPermission)
                {
                    string[] status = new string[] { "Pending", "FinanceApproved", "Rejected", "Autherized", "Cancelled" };
                    Array StatusArray = System.Enum.GetValues(typeof(EnumWorkStatus));
                    ViewBag.Status = new SelectList(StatusArray);

                    Array PriorityArray = System.Enum.GetValues(typeof(EnumWorkPriority));
                    ViewBag.Priority = new SelectList(PriorityArray);

                    Department dep = new Department();
                    List<Department> deplist = dep.GetAllDepartment();
                    ViewBag.Depart = new SelectList(deplist, "Id", "Name");

                    EmployeeBasicInformation emp = new EmployeeBasicInformation();
                    List<EmployeeBasicInformation> emplist = emp.GetEmployeeMaster();
                    ViewBag.emp = new SelectList(emplist, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult CreateWorkTask(WorkTask worktsk)
        {
            WorkTask obj = new WorkTask();
            obj.create(worktsk);
            return RedirectToAction("WorkTaskIndex");
        }

        public IActionResult EditWorkTask(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "TaskManagement").FirstOrDefault().UpdatePermission)
                {
                    WorkTask obj = new WorkTask();
                    obj.JobNo = id;
                    obj = obj.details();
                    return View(obj);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditWorkTask(WorkTask worktsk)
        {
            WorkTask obj = new WorkTask();
            obj.update(worktsk);
            return RedirectToAction("WorkTaskIndex");
        }

        public JsonResult WorkTaskDelete(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "TaskManagement").FirstOrDefault().DeletePermission)
                {
                    WorkTask obj = new WorkTask();
                    obj.JobNo = Id;
                    obj.delete();
                    var result = true;
                    return Json(result);
                }
            }
            return Json(false);
        }

        public IActionResult EmployeeWorkTaskIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeTask").FirstOrDefault().DisplayPermission)
                {
                    if (HttpContext.Session.GetInt32("EmployeeId") != null)
                    {
                        WorkTask obj = new WorkTask();
                        obj.EmployeeId = (int)HttpContext.Session.GetInt32("EmployeeId");
                        List<WorkTask> objlist = obj.GetAllWorkTaskByEmployee();
                        WorkTaskDisplay displayList = new WorkTaskDisplay();
                        displayList.taskList = objlist;
                        displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeTask").FirstOrDefault();
                        return View(displayList);
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult EditEmployeeTask(int id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "EmployeeTask").FirstOrDefault().UpdatePermission)
                {
                    WorkTask obj = new WorkTask();
                    obj.JobNo = id;
                    obj = obj.details();
                    Array StatusArray = System.Enum.GetValues(typeof(EnumWorkStatus));
                    ViewBag.Status = new SelectList(StatusArray, obj.Status);
                    return View(obj);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public IActionResult EditEmployeeTask(WorkTask worktsk)
        {
            WorkTask obj = new WorkTask();
            obj.updatebyEmployee(worktsk);
            return RedirectToAction("EmployeeWorkTaskIndex");
        }


        public IActionResult UserPermissions(Int64 UserId = 0)
        {
            List<UserPermission> permissionList = new List<UserPermission>();
            UserPermission _userP = new UserPermission();
            permissionList = _userP.GetUserPermissions(UserId);

            SetPermission setP = new SetPermission();
            setP.UserId = UserId;
            setP.permissionList = permissionList;

            return View(setP);
        }

        [HttpPost]
        public IActionResult UserPermissions(SetPermission _setP)
        {
            _setP.UpdatePermissions(_setP);

            return RedirectToAction("UserPermissions", new { UserId = _setP.UserId });
        }

    }
}