﻿using AccounTechSoftware.Models;
using VehicleServiceManagement.Models;
using VehicleServiceManagement.Models.Accounts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static VehicleServiceManagement.Models.Accounts.ClsAccountTransaction;
using BusinessSoftware.Models;
using Microsoft.AspNetCore.Http;

namespace VehicleServiceManagement.Controllers
{
    public class FinancialController : Controller
    {

        public static string[] StatusType = new string[] { "Pending", "Completed", "Rejected" };
        private IMemoryCache _cache;
        private static string Guid;
        JournalVoucher obj = new JournalVoucher();
        PaymentVoucher obj1 = new PaymentVoucher();
        ClsSubGroup obj2 = new ClsSubGroup();
        public FinancialController(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }
        public static string[] CreditDebit = new string[] { "Credit", "Debit"};
        public IActionResult Index()
        {           
            return View();
        }
        public PartialViewResult _partialFinancialLayout()
        {
            List<UserPermission> userP = new List<UserPermission>();
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
            }
            UserPermission pn = new UserPermission();
            userP = pn.GetUserPermissions(UserId);

            return PartialView(userP);
        }

        public IActionResult ChartofAccIndex()
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ChartOfAccounts").FirstOrDefault().DisplayPermission)
                {
                    ClsChartofAccount obj = new ClsChartofAccount();
                    return View(obj.GetChartOfAccount());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");

        }
       
        public IActionResult subgroupDetails(string Id)
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault().DisplayPermission)
                {
                    Clsledger obj = new Clsledger();
                    return PartialView("_SubGroupDetails", obj.GetAccountBySubGroup(Convert.ToInt32(Id)));
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult EditSubGroup(int Id)
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault().UpdatePermission)
                {
                    ClsSubGroup sgroup;
                    sgroup = obj2.details(Id);
                    List<ClsSubGroup> clsList = new List<ClsSubGroup>();
                    clsList = sgroup.GetAllSubGroup().ToList();
                    return PartialView("EditSubGroup", sgroup);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult EditSubGroup(ClsSubGroup sgroup)
        {
            obj2.update(sgroup);
            ViewBag.successmessage = "SubGroup Information Updated Successfully !!!";
            ClsSubGroup objsubgroup = new ClsSubGroup();
            List<ClsSubGroup> c = new List<ClsSubGroup>();
            c = obj2.GetAllSubGroup().ToList();
            objsubgroup.clsList = c;
            //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
            return View("SubGroupIndex", objsubgroup);
        }

        public JsonResult DeleteSubGroup(int Id)
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault().DeletePermission)
                {
                    var result = true;
                    obj2.delete(Id);
                    return Json(result);
                }
            }
            return Json(false);
        }

        public IActionResult SubGroupIndex()
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault().DisplayPermission)
                {
                    //ClsSubGroup obj1= new ClsSubGroup();
                    ClsSubGroup sub = new ClsSubGroup();
                    List<ClsSubGroup> c = new List<ClsSubGroup>();
                    c = obj2.GetAllSubGroup().ToList();
                    sub.clsList = c;
                    sub.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault();
                    ClsGroup group = new ClsGroup();
                    var d = group.GetAllGroup();
                    ViewBag.group = new SelectList(d, "Id", "Name");
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    return View(sub);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult SubGroupCreate(ClsSubGroup sub)
        {
            int UserId = 0;
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "AccountsGroup").FirstOrDefault().InsertPermission)
                {
                    //ClsSubGroup obj2 = new ClsSubGroup();
                    obj2.create(sub);
                    return RedirectToAction("SubGroupIndex");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");

        }
         
        public IActionResult JournalVoucherIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault().DisplayPermission)
                {
                    JournalVoucherMaster master = new JournalVoucherMaster();
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    JournalVoucherMasterDisplay displayList = new JournalVoucherMasterDisplay();
                    displayList.journalVoucherList = master.GetJournalVoucherMaster();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult JournalVoucherCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault().InsertPermission)
                {
                    Clsledger acc = new Clsledger();
                    var c1 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c1, "Id", "Name");

                    Location loc = new Location();
                    var c2 = loc.GetLocationName();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");

                    EmployeeBasicInformation emp = new EmployeeBasicInformation();
                    var c3 = emp.GetEmployeeMaster();
                    ViewBag.Employee = new SelectList(c3, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult SetJournalVoucher(JournalVoucherDetails prdtdetails)
        {

            StringBuilder str = new StringBuilder();
            Guid = prdtdetails.UID;
            List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
            if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + prdtdetails.UID) != null)
            {
                PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + prdtdetails.UID);
            }
            if (PrdetailList.Count == 0)
            {
                PrdetailList = new List<JournalVoucherDetails>();
                PrdetailList.Add(prdtdetails);
            }
            else
            {
                JournalVoucherDetails val = PrdetailList.Where(a => a.LedgerId == prdtdetails.LedgerId).FirstOrDefault();
                if (val != null)
                {
                    val.DrAmount = val.DrAmount + prdtdetails.DrAmount;
                    val.CrAmount = val.CrAmount + prdtdetails.CrAmount;
                }
                else
                {
                    PrdetailList.Add(prdtdetails);
                }
            }

            _cache.Set<List<JournalVoucherDetails>>("addproduct" + prdtdetails.UID, PrdetailList);
            String value = null;
            foreach (var item in PrdetailList)
            {
                value += @"<tr id=""" + item.LedgerId + @""">";
                value += @" <td>" + item.LedgerName + @"</td>
                            <td>" + item.Narration + @"</td>
                            <td>" + item.DrAmount + @"</td>      
                            <td>" + item.CrAmount + @"</td>
                            <td>" + item.Balance + @"</td>
                            <td><input type = ""button"" onclick=""removetable('" + item.LedgerId + @"')"" value=""Remove"" /></td></tr>";

            }

            str.Append(value);
            return Json(new { data = str.ToString() });
        }

        public JsonResult DelJournalVoucher(int id, string uid)
        {
            Guid = uid;
            List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
            if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + uid) != null)
            {
                PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + uid);
            }
            PrdetailList.Remove(PrdetailList.Where(a => a.LedgerId == id).FirstOrDefault());
            bool r = true;
            return Json(r);
        }

        [HttpPost]
        public IActionResult JournalVoucherCreate(JournalVoucher journal)
        {
            long j = 1;
            JournalVoucher obj = new JournalVoucher();
            j = obj.JournalVoucherMasterCreate(journal);
            List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
            if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid);
            }
            obj.JournalVoucherDetailsCreate(PrdetailList, j);
            return RedirectToAction("JournalVoucherIndex");
        }

        public IActionResult JournalVoucherEdit(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault().UpdatePermission)
                {
                    Location loc = new Location();
                    var c2 = loc.GetAllLocation();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");
                    Currency currency = new Currency();
                    var c1 = currency.GetAllCurrencies();
                    ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
                    Clsledger acc = new Clsledger();
                    var c3 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c3, "Id", "Name");

                    JournalVoucher p = new JournalVoucher();
                    JournalVoucherMaster p1 = new JournalVoucherMaster();
                    p1 = obj.details(Id);
                    p.JournalVoucherMaster = p1;
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    return View(p);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult EditJournalVoucherproduct(string uid, int no)
        {
            JournalVoucher ob = new JournalVoucher();
            StringBuilder str = new StringBuilder();
            Guid = uid;
            List<JournalVoucherDetails> products = new List<JournalVoucherDetails>();
            products = ob.GetAllDetails(no);
            List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
            if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid);
            }

            foreach (var item in products)
            {
                PrdetailList.Add(item);
            }
                       
            _cache.Set<List<JournalVoucherDetails>>("addproduct" + uid, PrdetailList);
            String value = null;
            foreach (var item in PrdetailList)
            {
                value += @"<tr id=""" + item.LedgerId + @""">";
                value += @" <td>" + item.LedgerName + @"</td>
                            <td>" + item.Narration + @"</td> 
                            <td>" + item.DrAmount + @"</td>
                            <td>" + item.CrAmount + @"</td>
                            <td>" + item.Balance + @"</td>
                            <td><input type = ""button"" onclick=""removetable('" + item.LedgerId + @"')"" value=""Remove"" /></td></tr>";

            }
            str.Append(value);
            return Json(new { data = str.ToString() });
        }

        [HttpPost]
        public IActionResult JournalVoucherEdit(JournalVoucher purchase)
        {
            long j = 1;
            JournalVoucher obj = new JournalVoucher();
            j = obj.UpdateJournalVoucherMaster(purchase);
            List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
            if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid);
            }
            j = obj.UpdateDetails(PrdetailList,j);
            return RedirectToAction("JournalVoucherIndex");
        }
        public IActionResult JournalVoucherDetails(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault().DisplayPermission)
                {
                    Location loc = new Location();
                    var c2 = loc.GetAllLocation();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");
                    Currency currency = new Currency();
                    var c1 = currency.GetAllCurrencies();
                    ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
                    Clsledger acc = new Clsledger();
                    var c3 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c3, "Id", "Name");

                    JournalVoucher p = new JournalVoucher();
                    JournalVoucherMaster p1 = new JournalVoucherMaster();
                    p1 = obj.details(Id);
                    p.JournalVoucherMaster = p1;
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    return View(p);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        public IActionResult JournalVoucherDelete(int id, JournalVoucher purchase)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "JournalVoucher").FirstOrDefault().DeletePermission)
                {
                    long j = 1;
                    JournalVoucher obj = new JournalVoucher();
                    j = obj.JournalVoucherMasterDelete(id);
                    List<JournalVoucherDetails> PrdetailList = new List<JournalVoucherDetails>();
                    if (_cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid) != null)
                    {
                        PrdetailList = _cache.Get<List<JournalVoucherDetails>>("addproduct" + Guid);
                    }
                    j = obj.JournalVoucherDetailsDelete(j);
                    return RedirectToAction("JournalVoucherIndex");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }


        public IActionResult JournalVoucherReportIndex()
        {
            //Vendor vendor = new Vendor();
            //var c6 = vendor.GetAllVendor();
            //ViewBag.vendor = new SelectList(c6, "Id", "VendorName");
            //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
            return View();

        }

        public IActionResult AllJournalVoucherReport()
        {
            List<JournalVoucherReport> PrdetailList = new List<JournalVoucherReport>();
            JournalVoucherReport report = new JournalVoucherReport();
            PrdetailList = report.GetJournalVoucherReport();
            report.journalList = PrdetailList;
            return PartialView("_AllJournalVoucherReport", report);
        }

        public IActionResult JournalVoucherReportDate(string todate, string fromdate)
        {
            List<JournalVoucherReport> PrdetailList = new List<JournalVoucherReport>();
            JournalVoucherReport Report = new JournalVoucherReport();
            PrdetailList = Report.JournalVoucherReportFetch(todate, fromdate);
            Report.journalList = PrdetailList;
            return PartialView("_AllJournalVoucherReport", Report);
        }

        // Payment Voucher


        public IActionResult PaymentVoucherIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault().DisplayPermission)
                {
                    PaymentVoucherMaster master = new PaymentVoucherMaster();
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    PaymentVoucherMasterDisplay displayList = new PaymentVoucherMasterDisplay();
                    displayList.paymentVoucherList = master.GetPaymentVoucherMaster();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult PaymentVoucherCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault().InsertPermission)
                {
                    Clsledger acc = new Clsledger();
                    var c1 = acc.GetAccountBySubGroupCode(3300);
                    ViewBag.ledger = new SelectList(c1, "Id", "Name");

                    Location loc = new Location();
                    var c2 = loc.GetLocationName();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");

                    EmployeeBasicInformation emp = new EmployeeBasicInformation();
                    var c3 = emp.GetEmployeeMaster();
                    ViewBag.Employee = new SelectList(c3, "Id", "Name");

                    var c4 = acc.GetAccountBySubGroupCode(10600);
                    ViewBag.Discount = new SelectList(c4, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult GetPaymentDetails(long ledgerId)
        {

            StringBuilder str = new StringBuilder();
            PaymentVoucherDetails obj = new PaymentVoucherDetails();
            List<PaymentVoucherDetails> PrdetailList = obj.GetDetailsByVendor(ledgerId);         

            String value = null;
            string[] data = new string[2];
            float totalamount = 0;
            foreach (var item in PrdetailList)
            {
                value += @"<tr id=""" + item.VoucherNo + @""">";
                value += @" <td>" + item.VoucherNo + @"</td>
                            <td>" + item.Date.ToString("dd-MM-yyyy") + @"</td>
                            <td>" + item.VendorInvoice + @"</td>
                            <td>" + item.InvoiceAmount + @"</td>
                            <td>" + item.PrevPaid + @"</td>
                            <td>" + item.Balance + @"</td>
                            <td>
                                <input type=""number"" id=" + item.VoucherNo + @"_Discount name=Discount  onfocusout=""DiscountCahange('" + item.VoucherNo + @"','" + item.Balance + @"')"" value=""0.00""  />
                                <input type=""hidden"" name=VoucherNo value=" + item.VoucherNo+@" />
                                <input type=""hidden"" name=Balance value=" + item.Balance+ @" />
                            </td>
                            <td>
                                <input type=""number"" id=" + item.VoucherNo + @"_PaidAmount name=PaidAmount onfocusout=""Amountchange('" + item.VoucherNo + @"')"" value=""0.00""  />
                                <input type=""hidden"" id=" + item.VoucherNo + @"_HPaidAmount />
                            </td>
                            <td><input type = ""checkbox"" id=" + item.VoucherNo + @"_Chk  onclick=""setpayamount('" + item.VoucherNo + @"','" + item.Balance + @"')"" class=""form - check""  /></td></tr>";
                totalamount = totalamount + item.Balance;
            }
            str.Append(value);
            data[0] = str.ToString();
            data[1] = totalamount.ToString();
            return Json( data);
        }
                
        [HttpPost]
        public IActionResult PaymentVoucherCreate(PaymentVoucher payment)
        {
            
            string vouchernumber = Request.Form["VoucherNo"];
            string paidAmount = Request.Form["PaidAmount"];
            string discountstr = Request.Form["Discount"];
            string balancestr = Request.Form["Balance"];

            string[] voucherlist = vouchernumber.Split(',');
            string[] Paidamountlist = paidAmount.Split(',');
            string[] DiscountList = discountstr.Split(',');
            string[] balancelist = balancestr.Split(',');
            float totalpaidamount = 0;
            float totaldiscount = 0;
            int k = 0;

            List<PaymentVoucherDetails> PaymentDetailsList = new List<PaymentVoucherDetails>();

            foreach (var item in voucherlist)
            {
                long voucherid = 0;
                float amount = 0;
                float discount = 0;
                float balance = 0;
                if (!string.IsNullOrEmpty(item))
                    voucherid = Convert.ToInt64(item);
                if (Paidamountlist.Count() > k && !string.IsNullOrEmpty(Paidamountlist[k]))
                {
                    amount = float.Parse(Paidamountlist[k]);
                }
                if(DiscountList.Count() > k && !string.IsNullOrEmpty(DiscountList[k]))
                {
                    discount = float.Parse(DiscountList[k]);
                }
                if (balancelist.Count() > k && !string.IsNullOrEmpty(balancelist[k]))
                {
                    balance = float.Parse(balancelist[k]);
                }
                if (amount>0)
                {
                    PaymentVoucherDetails objval = new PaymentVoucherDetails();
                    if(balance-discount>=amount)
                    {
                        objval.PaidAmount = balance - discount;
                        objval.FullyPaid = true;
                    }
                    else
                    {
                        objval.FullyPaid = false;
                        objval.PaidAmount = amount;
                    }
                    objval.VoucherNo = voucherid;
                    objval.Discount = discount;
                    totalpaidamount = totalpaidamount + objval.PaidAmount;
                    totaldiscount = totaldiscount + objval.Discount;
                    PaymentDetailsList.Add(objval);
                }
                k++;
            }

            payment.PaymentVoucherMaster.PaidAmount =Convert.ToDecimal(totalpaidamount);
           
            PaymentVoucher obj = new PaymentVoucher();
            long paymentmasterId = obj.PaymentVoucherMasterCreate(payment.PaymentVoucherMaster);            
            
            foreach(var item in PaymentDetailsList)
            {
                item.Id = obj.PaymentVoucherDetailsCreate(item, paymentmasterId);
                ClsAccountTransaction objAc = new ClsAccountTransaction();
                if (payment.PaymentVoucherMaster.PaymentMethod == 1)
                {
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), payment.PaymentVoucherMaster.FromAccount, Convert.ToDecimal(item.PaidAmount), false, "Payment Entry");
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Cash), Convert.ToDecimal(item.PaidAmount), true, "Payment Entry");
                   
                }
                else
                {
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), payment.PaymentVoucherMaster.FromAccount, Convert.ToDecimal(item.PaidAmount), false, "Payment Entry");
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Bank), Convert.ToDecimal(item.PaidAmount),true, "Payment Entry");
                    
                }
                if(item.Discount>0)
                {
                    if (payment.PaymentVoucherMaster.PaymentMethod == 1)
                    {
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Cash), Convert.ToDecimal(item.Discount), false, "Payment Discount Entry");
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), payment.PaymentVoucherMaster.DiscountLedger, Convert.ToDecimal(item.Discount),true, "Payment Discount Entry");                        
                    }
                    else
                    {
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Bank), Convert.ToDecimal(item.Discount), false, "Payment Discount Entry");
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), payment.PaymentVoucherMaster.DiscountLedger, Convert.ToDecimal(item.Discount),true, "Payment Discount Entry");                        
                    }

                }
            }
            return RedirectToAction("PaymentVoucherIndex");
        }

        public IActionResult PaymentVoucherEdit(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault().UpdatePermission)
                {
                    Location loc = new Location();
                    var c = loc.GetAllLocation();
                    ViewBag.location = new SelectList(c, "Id", "LocationName");
                    Currency currency = new Currency();
                    var c1 = currency.GetAllCurrencies();
                    ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
                    PaymentType PaymentType = new PaymentType();
                    var c2 = PaymentType.GetAllPaymentType();
                    ViewBag.payment = new SelectList(c2, "Id", "PaymentName");
                    Clsledger acc = new Clsledger();
                    var c3 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c3, "Id", "Name");
                    //Vendor v = new Vendor();
                    //var c4 = v.GetAllVendor();
                    //ViewBag.vendor = new SelectList(c4, "Id", "VendorName");
                    ClsSubGroup sb = new ClsSubGroup();
                    var c5 = sb.GetAllSubGroup();
                    ViewBag.account = new SelectList(c5, "Id", "Name");

                    PaymentVoucher p = new PaymentVoucher();
                    PaymentVoucherMaster p1 = new PaymentVoucherMaster();
                    p1 = obj1.details(Id);
                    p.PaymentVoucherMaster = p1;
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    return View(p);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }
        
        [HttpPost]
        public IActionResult PaymentVoucherEdit(PaymentVoucher purchase)
        {
            int j = 1;
            PaymentVoucher obj = new PaymentVoucher();
            j = obj.UpdatePaymentVoucherMaster(purchase);
            List<PaymentVoucherDetails> PrdetailList = new List<PaymentVoucherDetails>();
            if (_cache.Get<List<PaymentVoucherDetails>>("addproduct" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<PaymentVoucherDetails>>("addproduct" + Guid);
            }
            j = obj.UpdateDetails(PrdetailList);
            return RedirectToAction("PaymentVoucherIndex");
        }

        public IActionResult PaymentVoucherDetails(int Id)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault().DisplayPermission)
                {
                    Location loc = new Location();
                    var c = loc.GetAllLocation();
                    ViewBag.location = new SelectList(c, "Id", "LocationName");
                    Currency currency = new Currency();
                    var c1 = currency.GetAllCurrencies();
                    ViewBag.currency = new SelectList(c1, "Id", "CurrencyName");
                    PaymentType PaymentType = new PaymentType();
                    var c2 = PaymentType.GetAllPaymentType();
                    ViewBag.payment = new SelectList(c2, "Id", "PaymentName");
                    Clsledger acc = new Clsledger();
                    var c3 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c3, "Id", "Name");
                    //Vendor v = new Vendor();
                    //var c4 = v.GetAllVendor();
                    //ViewBag.vendor = new SelectList(c4, "Id", "VendorName");
                    ClsSubGroup sb = new ClsSubGroup();
                    var c5 = sb.GetAllSubGroup();
                    ViewBag.account = new SelectList(c5, "Id", "Name");
                    PaymentVoucher p = new PaymentVoucher();
                    PaymentVoucherMaster p1 = new PaymentVoucherMaster();
                    p1 = obj1.details(Id);
                    p.PaymentVoucherMaster = p1;
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    return View(p);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult PaymentVoucherDelete(int id, PaymentVoucher purchase)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PaymentVoucher").FirstOrDefault().DeletePermission)
                {
                    int j = 1;
                    PaymentVoucher obj = new PaymentVoucher();
                    j = obj.PaymentVoucherMasterDelete(id);
                    List<PaymentVoucherDetails> PrdetailList = new List<PaymentVoucherDetails>();
                    if (_cache.Get<List<PaymentVoucherDetails>>("addproduct" + Guid) != null)
                    {
                        PrdetailList = _cache.Get<List<PaymentVoucherDetails>>("addproduct" + Guid);
                    }
                    j = obj.PaymentVoucherDetailsDelete(PrdetailList);
                    return RedirectToAction("PaymentVoucherIndex");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public JsonResult GetCurrencyDetails(int Id)
        {
            Currency currency = new Currency();
            var c1 = currency.details(Id);
            return Json(c1);
        }

        // Accounts List
        public IActionResult AccountsListIndex()
        {
            Clsledger obj = new Clsledger();
            obj.objledgerlist = obj.GetAllLedger();
            Currency Currency = new Currency();
            var c5 = Currency.GetAllCurrencies();
            ViewBag.Currency = new SelectList(c5, "Id", "CurrencyName");
            ClsSubGroup sub = new ClsSubGroup();
            var c6 = sub.GetAllSubGroup();
            ViewBag.SubGroup = new SelectList(c6, "Id", "Name");
            SelectList list = new SelectList(CreditDebit);
            ViewBag.dtype = list;
            //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
            return View(obj);
        }

        [HttpPost]
        public IActionResult AccountsListCreate(Clsledger ledger)
        {
            Clsledger obj =new Clsledger();
            obj.create(ledger);
            return RedirectToAction("AccountsListIndex");
        }
        
        public IActionResult AccountsListEdit(int Id)
        {
            Clsledger obj = new Clsledger();
            Clsledger ledger;
            ledger = obj.details(Id);
            Currency Currency = new Currency();
            var c5 = Currency.GetAllCurrencies();
            ViewBag.Currency = new SelectList(c5, "Id", "CurrencyName");
            ClsSubGroup sub = new ClsSubGroup();
            var c6 = sub.GetAllSubGroup();
            ViewBag.SubGroup = new SelectList(c6, "Id", "Name");
            SelectList list = new SelectList(CreditDebit);
            ViewBag.dtype = list;
            return PartialView("_AccountsListEdit", ledger);
        }

        [HttpPost]
        public IActionResult AccountsListEdit(Clsledger obj1)
        {
            Clsledger obj = new Clsledger();
            obj.update(obj1);
            return RedirectToAction("AccountsListIndex");
        }

        public JsonResult AccountsListDelete(int Id)
        {
            Clsledger obj = new Clsledger();
            var result = true;
            obj.delete(Id);
            return Json(result);
        }
        
        // Receipt Voucher        

        public JsonResult GetCustomer(int Id)
        {
            DealerInformation cust = new DealerInformation();
            var c1 = cust.details(Id);
            return Json(c1);
        }
               
        public IActionResult ReceiptVoucherIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ReceiptVoucher").FirstOrDefault().DisplayPermission)
                {
                    ReceiptVoucherMaster receipt = new ReceiptVoucherMaster();
                    List<ReceiptVoucherMaster> list = new List<ReceiptVoucherMaster>();
                    list = receipt.GetReceiptMaster().ToList();
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    ReceiptVoucherMasterDisplay displayList = new ReceiptVoucherMasterDisplay();
                    displayList.receiptVoucherList = list;
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ReceiptVoucher").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult ReceiptVoucherCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ReceiptVoucher").FirstOrDefault().InsertPermission)
                {
                    Clsledger acc = new Clsledger();
                    var c1 = acc.GetAccountBySubGroupCode(1400);
                    ViewBag.ledger = new SelectList(c1, "Id", "Name");

                    Location loc = new Location();
                    var c2 = loc.GetAllLocation();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");

                    EmployeeBasicInformation emp = new EmployeeBasicInformation();
                    var c3 = emp.GetEmployeeMaster();
                    ViewBag.Employee = new SelectList(c3, "Id", "Name");

                    var c4 = acc.GetAccountBySubGroupCode(8300);
                    ViewBag.Discount = new SelectList(c4, "Id", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult ReceiptVoucherCreate(ReceiptVoucher receipt)
        {
            string vouchernumber = Request.Form["VoucherNo"];
            string RecievingAmount = Request.Form["RecievingAmount"];
            string discountstr = Request.Form["Discount"];
            string balancestr = Request.Form["Balance"];

            string[] voucherlist = vouchernumber.Split(',');
            string[] Recievamountlist = RecievingAmount.Split(',');
            string[] DiscountList = discountstr.Split(',');
            string[] balancelist = balancestr.Split(',');
            float totalrecievedamount = 0;
            float totaldiscount = 0;
            int k = 0;

            List<ReceiptVoucherDetails> RecieptDetailsList = new List<ReceiptVoucherDetails>();

            foreach (var item in voucherlist)
            {
                long voucherid = 0;
                float amount = 0;
                float discount = 0;
                float balance = 0;
                if (!string.IsNullOrEmpty(item))
                    voucherid = Convert.ToInt64(item);
                if (Recievamountlist.Count() > k && !string.IsNullOrEmpty(Recievamountlist[k]))
                {
                    amount = float.Parse(Recievamountlist[k]);
                }
                if (DiscountList.Count() > k && !string.IsNullOrEmpty(DiscountList[k]))
                {
                    discount = float.Parse(DiscountList[k]);
                }
                if (balancelist.Count() > k && !string.IsNullOrEmpty(balancelist[k]))
                {
                    balance = float.Parse(balancelist[k]);
                }
                if (amount > 0)
                {
                    ReceiptVoucherDetails objval = new ReceiptVoucherDetails();
                    if (balance - discount >= amount)
                    {
                        objval.RecieptAmount = balance - discount;
                        objval.FullyRecieved = true;
                    }
                    else
                    {
                        objval.FullyRecieved = false;
                        objval.RecieptAmount = amount;
                    }
                    objval.ReceiptVoucherNo = voucherid;
                    objval.Discount = discount;
                    totalrecievedamount = totalrecievedamount + objval.RecieptAmount;
                    totaldiscount = totaldiscount + objval.Discount;
                    RecieptDetailsList.Add(objval);
                }
                k++;
            }
            receipt.ReceiptMaster.RecievingAmount = totalrecievedamount;

            ReceiptVoucher obj = new ReceiptVoucher();
            long recieptmasterId = obj.CreateMaster(receipt.ReceiptMaster);

            foreach (var item in RecieptDetailsList)
            {
                item.Id = obj.CreateDetails(item, recieptmasterId);
                ClsAccountTransaction objAc = new ClsAccountTransaction();
                if (receipt.ReceiptMaster.PaymentMethod == 1)
                {
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Cash), Convert.ToDecimal(item.RecieptAmount), false, "Reciept Entry");
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), receipt.ReceiptMaster.ReceivingAccount, Convert.ToDecimal(item.RecieptAmount), true, "Reciept Entry");

                }
                else
                {
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Bank), Convert.ToDecimal(item.RecieptAmount), false, "Reciept Entry");
                    objAc.create(EnumTransactionType.payment, item.Id.ToString(), receipt.ReceiptMaster.ReceivingAccount, Convert.ToDecimal(item.RecieptAmount), true, "Reciept Entry");

                }
                if (item.Discount > 0)
                {
                    if (receipt.ReceiptMaster.PaymentMethod == 1)
                    {
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), receipt.ReceiptMaster.DiscountLedger, Convert.ToDecimal(item.Discount), false, "Reciept Discount Entry");
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Cash), Convert.ToDecimal(item.Discount), true, "Reciept Discount Entry");

                    }
                    else
                    {
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), receipt.ReceiptMaster.DiscountLedger, Convert.ToDecimal(item.Discount), false, "Reciept Discount Entry");
                        objAc.create(EnumTransactionType.payment, item.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Bank), Convert.ToDecimal(item.Discount), true, "Reciept Discount Entry");
                    }

                }
            }
            return RedirectToAction("ReceiptVoucherIndex");
        }
        
        public JsonResult GetRecieptDetails(long ledgerId)
        {

            StringBuilder str = new StringBuilder();
            ReceiptVoucherDetails obj = new ReceiptVoucherDetails();
            List<ReceiptVoucherDetails> PrdetailList = obj.GetDetailsByCustomer(ledgerId);

            String value = null;
            string[] data = new string[2];
            float totalamount = 0;
            foreach (var item in PrdetailList)
            {
                value += @"<tr id=""" + item.ReceiptVoucherNo + @""">";
                value += @" <td>" + item.ReceiptVoucherNo + @"</td>
                            <td>" + item.Date.ToString("dd-MM-yyyy") + @"</td>
                            <td>" + item.InvoiceAmount + @"</td>
                            <td>" + item.PrevPaid + @"</td>
                            <td>" + item.Balance + @"</td>
                            <td>
                                <input type=""number"" id=" + item.ReceiptVoucherNo + @"_Discount name=Discount  onfocusout=""DiscountCahange('" + item.ReceiptVoucherNo + @"','" + item.Balance + @"')"" value=""0.00""  />
                                <input type=""hidden"" name=VoucherNo value=" + item.ReceiptVoucherNo + @" />
                                <input type=""hidden"" name=Balance value=" + item.Balance + @" />
                            </td>
                            <td>
                                <input type=""number"" id=" + item.ReceiptVoucherNo + @"_RecievingAmount name=RecievingAmount onfocusout=""Amountchange('" + item.ReceiptVoucherNo + @"')"" value=""0.00""  />
                                <input type=""hidden"" id=" + item.ReceiptVoucherNo + @"_HRecievingAmount />
                            </td>
                            <td><input type = ""checkbox"" id=" + item.ReceiptVoucherNo + @"_Chk  onclick=""setpayamount('" + item.ReceiptVoucherNo + @"','" + item.Balance + @"')"" class=""form - check""  /></td></tr>";
                totalamount = totalamount + item.Balance;
            }
            str.Append(value);
            data[0] = str.ToString();
            data[1] = totalamount.ToString();
            return Json(data);
        }

        // Expense Voucher
        public IActionResult ExpenseVoucherIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExpenseVoucher").FirstOrDefault().DisplayPermission)
                {
                    ExpenseVoucherMaster receipt = new ExpenseVoucherMaster();
                    List<ExpenseVoucherMaster> list = new List<ExpenseVoucherMaster>();
                    list = receipt.GetExpenseMaster().ToList();
                    //ViewBag.companyname = HttpContext.Session.GetString("CompanyName");
                    ExpenseVoucherMasterDisplay displayList = new ExpenseVoucherMasterDisplay();
                    displayList.expenseVoucherList = list;
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExpenseVoucher").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult ExpenseVoucherCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExpenseVoucher").FirstOrDefault().InsertPermission)
                {
                    Clsledger acc = new Clsledger();
                    var c1 = acc.GetAllLedger();
                    ViewBag.ledger = new SelectList(c1, "Id", "Name");

                    Location loc = new Location();
                    var c2 = loc.GetLocationName();
                    ViewBag.location = new SelectList(c2, "Id", "LocationName");

                    EmployeeBasicInformation emp = new EmployeeBasicInformation();
                    var c3 = emp.GetEmployeeMaster();
                    ViewBag.Employee = new SelectList(c3, "Id", "Name");

                    var c4 = acc.GetAccountBySubGroupCode(10600);
                    ViewBag.Discount = new SelectList(c4, "Id", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public IActionResult ExpenseVoucherCreate(ExpenseVoucher expense)
        {

            int j = 1;
            ExpenseVoucher obj = new ExpenseVoucher();
            j = obj.CreateMaster(expense);
            List<ExpenseVoucherDetails> PrdetailList = new List<ExpenseVoucherDetails>();
            if (_cache.Get<List<ExpenseVoucherDetails>>("Expense" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<ExpenseVoucherDetails>>("Expense" + Guid);
            }
            j = obj.CreateDetails(PrdetailList);
            DealerInformation objcustomer = new DealerInformation();
            ClsAccountTransaction objAc = new ClsAccountTransaction();


            foreach (var item in PrdetailList)
            {
                long ledgerId = item.Account;
                objAc.create(EnumTransactionType.Expense, expense.ExpenseMaster.Id.ToString(), Convert.ToInt64(EnumGeneralLedger.Cash), Convert.ToDecimal(expense.ExpenseMaster.PaidAmount),false, "Expense Entry");
                objAc.create(EnumTransactionType.Expense, expense.ExpenseMaster.Id.ToString(), ledgerId, Convert.ToDecimal(expense.ExpenseMaster.TotalAmount),true, "Expense Entry");
                
            }
            return RedirectToAction("ReceiptVoucherCreate");
        }

        public JsonResult ExpenseVoucherSetProduct(ExpenseVoucherDetails prdtdetails)
        {
            ExpenseVoucherDetails ob = new ExpenseVoucherDetails();
            StringBuilder str = new StringBuilder();
            Guid = prdtdetails.UID;
            List<ExpenseVoucherDetails> PrdetailList = new List<ExpenseVoucherDetails>();
            if (_cache.Get<List<ExpenseVoucherDetails>>("Expense" + Guid) != null)
            {
                PrdetailList = _cache.Get<List<ExpenseVoucherDetails>>("Expense" + Guid);
            }


            if (PrdetailList.Count == 0)
            {
                PrdetailList = new List<ExpenseVoucherDetails>();

            }
            PrdetailList.Add(prdtdetails);
           
            _cache.Set<List<ExpenseVoucherDetails>>("Expense" + prdtdetails.UID, PrdetailList);
            String value = null;

            foreach (var item in PrdetailList)
            {
                value += @"<tr id=""" + item.Account + @""">";
                value += @" <td>" + item.AccountName + @"</td>
                            <td>" + item.Narration + @"</td>
                            <td>" + item.LocationName + @"</td> 
                            <td>" + item.Payee + @"</td>
                            <td>" + item.RefDate + @"</td>
                            <td>" + item.Amount + @"</td>
                            <td>" + item.VATAmount + @"</td>
                             <td>" + item.DrAmount + @"</td>
                            <td>" + item.Balance + @"</td>
                            <td><input type = ""button"" onclick=""removetable('" + item.Account + @"')"" value=""Remove"" /></td></tr>";

            }
            str.Append(value);
            return Json(new { data = str.ToString() });
        }

        public JsonResult ExpenseVoucherDelProduct(int id, string uid)
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "ExpenseVoucher").FirstOrDefault().DeletePermission)
                {
                    Guid = uid;
                    List<ExpenseVoucherDetails> PrdetailList = new List<ExpenseVoucherDetails>();
                    if (_cache.Get<List<ExpenseVoucherDetails>>("Expense" + uid) != null)
                    {
                        PrdetailList = _cache.Get<List<ExpenseVoucherDetails>>("Expense" + uid);
                    }

                    PrdetailList.Remove(PrdetailList.Single(t => t.Account == id));
                    //_cache.Set<List<SalesInquiryDetails>>("addproduct" + uid, PrdetailList);

                    bool r = true;
                    return Json(r);
                }
            }
            return Json(false);
        }

        //Balance Sheet

        public IActionResult BalanceSheetIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "FinancialReport").FirstOrDefault().DisplayPermission)
                {
                    BalanceSheet obj = new BalanceSheet();
                    return View(obj.GetBalanceSheet());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        // Profit and Loss

        public IActionResult ProfitLossIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "FinancialReport").FirstOrDefault().DisplayPermission)
                {
                    ProfitandLoss at = new ProfitandLoss();                   
                    return View(at.GetProfitandLoss());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult TrialBalanceReport()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "FinancialReport").FirstOrDefault().DisplayPermission)
                {
                    FinalAccounting obj = new FinalAccounting();
                    return View(obj.GetTrialBalance());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public IActionResult CheckPaymentCreate()
        {
            return View();
        }
               
        public IActionResult PurchaseInvoiceIndex()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PurchaseEntry").FirstOrDefault().DisplayPermission)
                {
                    PurchaseMaster master = new PurchaseMaster();
                    PurchaseMasterDisplay displayList = new PurchaseMasterDisplay();
                    displayList.purchaseMasterList = master.GetPurchaseInvoiceMaster();
                    displayList.userP = UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PurchaseEntry").FirstOrDefault();
                    return View(displayList);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public static string[] VATCategory = new string[] { "Taxable", "Non-Taxable" };

        public IActionResult PurchaseCreate()
        {
            if (HttpContext.Session.GetInt32("UserId") != null)
            {
                int UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId"));
                if (UserPermission.permissionList.Where(a => a.UserId == UserId && a.FormName == "PurchaseEntry").FirstOrDefault().InsertPermission)
                {
                    SelectList list = new SelectList(VATCategory);
                    ViewBag.myList = list;

                    EmployeeBasicInformation objemp = new EmployeeBasicInformation();
                    List<EmployeeBasicInformation> objemplist = objemp.GetEmployeeMaster();
                    ViewBag.employee = new SelectList(objemplist, "Id", "Name");

                    DealerInformation objdealer = new DealerInformation();
                    List<DealerInformation> objdealerlist = objdealer.GetDealerVendor();
                    ViewBag.dealer = new SelectList(objdealerlist, "VendorLedgerId", "Name");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login", "Home");
        }

        public string Guid1 = null;

        [HttpPost]
        public IActionResult PurchaseCreate(PurchaseInvoice purchase)
        {
            PurchaseInvoice obj = new PurchaseInvoice();
            long purchaseId = obj.PurchaseCreate(purchase);
            List<PurchaseDetails> purdtls = _cache.Get<List<PurchaseDetails>>("addproduct" + purchase.UID);
            obj.PurchaseInvoiceDetailsCreate(purdtls, purchaseId);

            ClsAccountTransaction objAc = new ClsAccountTransaction();
            objAc.CreateByLedgerCode(EnumTransactionType.purchase, purchaseId.ToString(), "9101", Convert.ToDecimal(purchase.PurchaseInvoiceMaster.PurchaseTotal),false, "Purchase Entry");
            objAc.CreateByLedgerCode(EnumTransactionType.purchase, purchaseId.ToString(), "3501", Convert.ToDecimal(purchase.PurchaseInvoiceMaster.VATAmount),false, "Purchase Vat Entry");
            objAc.create(EnumTransactionType.purchase, purchaseId.ToString(), purchase.PurchaseInvoiceMaster.DealerLedgerId,Convert.ToDecimal( purchase.PurchaseInvoiceMaster.SubTotal),true, "Purchase Entry");

            return RedirectToAction("PurchaseInvoiceIndex");
        }

        public string[] setpurdetails(List<PurchaseDetails> PrdetailList)
        {
            StringBuilder str = new StringBuilder();
            string[] data = new string[4];
            String value = null;
            int k = 0;
           
            float totalprice = 0;
            float vattotal = 0;           
            foreach (var item in PrdetailList)
            {
                k++;
                value += @"<tr id=""" + item.Particular + @""">";
                value += @" <td>" + k + @"</td>
                           
                            <td>" + item.Particular + @"</td>
                            <td>" + item.Quantity + @"</td>                         
                            <td>" + item.Price + @"</td>                            
                            <td>" + item.TotalPrice + @"</td>                            
                            <td>" + item.Vatcategory + @"</td>
                            <td>" + item.Vat + @"</td>
                            <td>" + item.Total + @"</td>                           
                            <td><input type = ""button"" class=""btn btn-danger form-control form-control-spl"" onclick=""removetable('" + item.Particular + @"')"" value=""Remove"" /></td></tr>";

                totalprice = totalprice + item.TotalPrice;
                vattotal = vattotal + item.Vat;
            }
          
            float nettotal = totalprice + vattotal;
            str.Append(value);
            data[0] = str.ToString();
            data[1] = totalprice.ToString();
            data[2] = vattotal.ToString();
            data[3] = nettotal.ToString();
            return data;
        }

        public JsonResult SetProduct(PurchaseDetails prdtdetails)
        {
            PurchaseDetails ob = new PurchaseDetails(); 
            List<PurchaseDetails> PrdetailList = new List<PurchaseDetails>();
            if (_cache.Get<List<PurchaseDetails>>("addproduct" + prdtdetails.UID) != null)
            {
                PrdetailList = _cache.Get<List<PurchaseDetails>>("addproduct" + prdtdetails.UID);
            }
            if (PrdetailList.Count == 0)
            {
                PrdetailList = new List<PurchaseDetails>();
                PrdetailList.Add(prdtdetails);
            }
            else
            {
                PurchaseDetails val = PrdetailList.Where(a => a.Particular == prdtdetails.Particular).FirstOrDefault();
                if (val != null)
                {
                    val.Total = val.Total + prdtdetails.Total;
                    val.Quantity = val.Quantity + prdtdetails.Quantity;
                }
                else
                {
                    PrdetailList.Add(prdtdetails);
                }
            }
            _cache.Set<List<PurchaseDetails>>("addproduct" + prdtdetails.UID, PrdetailList);   
            return Json(setpurdetails(PrdetailList));
        }

        public JsonResult DelProduct(PurchaseDetails prdtdetails)
        {
            List<PurchaseDetails> PrdetailList = new List<PurchaseDetails>();
            if (_cache.Get<List<PurchaseDetails>>("addproduct" + prdtdetails.UID) != null)
            {
                PrdetailList = _cache.Get<List<PurchaseDetails>>("addproduct" + prdtdetails.UID);
                PurchaseDetails val = PrdetailList.Where(a => a.Particular == prdtdetails.Particular).FirstOrDefault();
                PrdetailList.Remove(val);
            }
            _cache.Set<List<PurchaseDetails>>("addproduct" + prdtdetails.UID, PrdetailList);
            return Json(setpurdetails(PrdetailList));
        }
               
        public IActionResult ContractCreate()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ContractCreate(ContractService vendor)
        {
            ContractService obj = new ContractService();
            int ledgerid = obj.GetLedgerbyContractnumber(vendor.ContractNumber);
            obj.create(vendor);
            ClsAccountTransaction objAc = new ClsAccountTransaction();
            objAc.create(EnumTransactionType.purchase, vendor.ContractNumber, ledgerid, Convert.ToDecimal(vendor.serviceamount),false, "Sales Entry");
            objAc.CreateByLedgerCode(EnumTransactionType.purchase, vendor.ContractNumber, "7101", Convert.ToDecimal(vendor.serviceamount),true, "Sales Entry");

            return RedirectToAction("ContractIndex");
        }

        public IActionResult ContractIndex()
        {
            ContractService vendor = new ContractService();
            return View(vendor.GetAllContractService());
        }

        public JsonResult GetContractInformationDetails(int id)
        {
            ContractInformation obj = new ContractInformation();
            ContractInformation con;
            con = obj.details(id);
            return Json(con);

        }
        public IActionResult EditContractService(int ServiceInvoice)
        {
            ContractService obj = new ContractService();

            ContractService dealer;
            dealer = obj.details(ServiceInvoice);
            return View(dealer);
        }

        [HttpPost]
        public IActionResult EditContractService(ContractService dealer)
        {
            ContractService obj = new ContractService();
            obj.update(dealer);
            return RedirectToAction("ContactIndex");
        }


    }
}
