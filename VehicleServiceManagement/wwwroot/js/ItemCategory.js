﻿

$.get("/Inventory/GetStudentList", null, DataBind);


function DataBind(StudentList) {
       
    var SetData = $("#SetStudentList");
    for (var i = 0; i < StudentList.length; i++) {

        var Data = "<tr class='row_" + StudentList[i].id + "'>" +
            "<td>" + StudentList[i].id + "</td>" +
            "<td>" + StudentList[i].categoryName + "</td>" +
            "<td>" + StudentList[i].subCategory + "</td>" +
            "<td>" + StudentList[i].code + "</td>" +
            "<td>" + StudentList[i].description + "</td>" +
            "<td>" + StudentList[i].remarks + "</td>" +
            "<td>" + "<a href='#' class='btn btn-warning' onclick='EditCategoryRecord(" + StudentList[i].id + ")' ><span class='glyphicon glyphicon-edit'></span></a>" + "</td>" +
            "<td>" + "<a href='#' class='btn btn-danger' onclick='DeleteCategoryRecord(" + StudentList[i].id + ")'><span class='glyphicon glyphicon-trash'></span></a>" + "</td>" +
            "</tr>";

        SetData.append(Data);


    }
}

function AddNewCategory(CategoryId) {
    alert("a");
    $("#form")[0].reset();
    $("#ModalTitle").html("Add New Category");
    $("#MyModal").modal();
}



function DeleteCategoryRecord(CategoryId) {
    $("#Id").val(CategoryId);

    $("#DeleteConfirmation").modal("show");
}
function ConfirmDelete() {
    var id = $("#Id").val();

    $.ajax({
        type: "POST",
        url: "/Inventory/Delete?Id=" + id,
        success: function (data) {
            $("#DeleteConfirmation").modal("hide");
            $(".row_" + id).remove();

        }
    })



}

function EditCategoryRecord(CategoryId) {
    alert("" + CategoryId);
    var url = "/Inventory/Edit?Id=" + CategoryId;
    $("#ModalTitle1").html("Update Category Record");
    $("#MyModal1").modal();


    $.ajax({
        type: "GET",
        url: url,

        success: function (data) {


            $("#Id1").val(data.id);
            $("#CategoryName1").val(data.categoryName);
            $("#SubCategory1").val(data.subCategory);
            $("#Code1").val(data.code);
            $("#Description1").val(data.description);
            $("#Remarks1").val(data.remarks);

        }
    })
}



$("#SaveCategoryRecord").click(function () {

    var data = $("#SubmitForm").serialize();

    $.ajax({
        type: "Post",
        url: "/Inventory/Create",
        data: data,
        success: function () {
            alert("Success");
            window.location.href = "/Inventory/Index";
            $("#MyModal").modal("hide");
        }
    })
})


$("#UpdateCategoryRecord").click(function () {
    var data = $("#SubmitForm1").serialize();
    $.ajax({
        type: "Post",
        url: "/Inventory/Update",
        data: data,
        success: function () {
            alert("Sucess");
            window.location.href = "/Inventory/Index";
            $("#MyModal1").modal("hide");
        }
    })
})