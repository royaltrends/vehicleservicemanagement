﻿var i = $('tblIndex tr').length;
$(document).on('keyup', '.lst', function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    
    if (code == 13) {
      
        html = '<tr>';
        html += '<td>' + i + '</td>';
        html += '<td><input type="text" class="inputs" name="Code_' + i + '" id="Code_' + i + '" /></td>';
        html += '<td><input type="text" class="inputs" name="Item_' + i + '" id="Item_' + i + '" /></td>';
        html += '<td><input type="text" class="inputs lst" name="Quantity_' + i + '" id="Quantity_' + i + '" /></td>';
        html += '</tr>';
        $('tblIndex').append(html);
        $(this).focus().select();
        i++;
    }
});

$(document).on('keydown', '.inputs', function (e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {
        var index = $('.inputs').index(this) + 1;
        $('.inputs').eq(index).focus();
    }
});