﻿
function selectdealerbranch() {
    var dealerId = $('#contractInformation_DealerId').val();
  
    $.ajax({
        type: "POST",
        url: "/Contract/GetDealerBranch?dealerid=" + dealerId,
      
        success: function (response) {
            var branch = "<option value='0'>Select Branch</option>";
            for (var x = 0; x < response.length; x++) {
                branch += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#contractInformation_BranchId").html(branch).show();
           
        }
    })
}

function SelectProductGroup() {
    var prgroupid = $('#contractInformation_ProductGroupId').val();
  
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeProductGroup?productGroup=" + prgroupid,

        success: function (response) {
            var Group = "<option value='0'>Select Product SubGroup</option>";
            for (var x = 0; x < response[0].length; x++) {
                Group += "<option value=" + response[0][x].value + ">" + response[0][x].text + "</option>";
            }
            $("#contractInformation_ProductSubGroupId").html(Group).show();

            var make = "<option value='0'>Select Make</option>";
            for (var x = 0; x < response[1].length; x++) {
                make += "<option value=" + response[1][x].value + ">" + response[1][x].text + "</option>";
            }
            $("#contractInformation_MakeId").html(make).show();

        }
    })
}

function SelectMake() {
    var makeval = $('#contractInformation_MakeId').val();
    var prgroup = $('#contractInformation_ProductGroupId').val();
    var subgroup = $('#contractInformation_ProductSubGroupId').val();
    if (prgroup == "") {
        prsubgroup = 0;
    }
    if (subgroup == "") {
        subgroup = 0;
    }
    var makeid = makeval.split(',')[0];
    var categoryId = makeval.split(',')[1];
    var category = makeval.split(',')[2];

    $.ajax({
        type: "POST",
        url: "/Contract/changeMake",
        data: {
            makeid: makeid,
            productgroupId: prgroup,
            productsubgroup:subgroup,
            CategoryId: categoryId
                },
        success: function (response) {

            var model = "<option value='0'>Select Model</option>";            
            for (var x = 0; x < response[0].length; x++) {
                model += "<option value=" + response[0][x].value + ">" + response[0][x].text + "</option>";
            }
            $("#contractInformation_ModelId").html(model).show();

            var Duration = "<option value='0'>Select Duration</option>";
            for (var x = 0; x < response[1].length; x++) {
                Duration += "<option value=" + response[1][x].value + ">" + response[1][x].text + "</option>";
            }
            $("#contractInformation_ManufacturerAssistanceDuration").html(Duration).show();

            var Milage = "<option value='0'>Select Milage Cuttoff</option>";
            for (var x = 0; x < response[2].length; x++) {
                Milage += "<option value=" + response[2][x].value + ">" + response[2][x].text + "</option>";
            }
            $("#contractInformation_ManufacturerAssistanceMileageCutoff").html(Milage).show();   

            var extduration = "<option value='0'>Select Ext Duration</option>";
            for (var x = 0; x < response[3].length; x++) {
                extduration += "<option value=" + response[3][x].value + ">" + response[3][x].text + "</option>";
            }
            $("#contractInformation_ExtendedAssistanceDuration").html(extduration).show();

        }
    })
    document.getElementById('contractInformation_CategoryName').value = category; 
}


function SelectDuration() {
    var durId = $('#contractInformation_ExtendedAssistanceDuration').val();
    var dealerId = $('#contractInformation_DealerId').val();
    if (dealerId == "") {
        dealerId = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeDuration",
        data: {
            durationid: durId,
            dealerId: dealerId           
        },
        success: function (response) {
            var cutoff = "<option value='0'>Select  Ext-CutOff</option>";
            for (var x = 0; x < response.length; x++) {
                cutoff += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#contractInformation_ExtendedAssistanceMileageCutoff").html(cutoff).show();


        }
    })
}

function ChangeOtherService() {
    var serviceid = $('#otherService_OtherServiceId').val();   
    if (serviceid == "") {
        serviceid = 0;
    }

    $.ajax({
        type: "POST",
        url: "/Contract/ChangeOtherService",
        data: {
            OtherServiceId: serviceid            
        },
        success: function (response) {
            var workshop = "<option value='0'>--Select  Workshop--</option>";
            for (var x = 0; x < response.length; x++) {
                workshop += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_WorkshopId").html(workshop).show();


        }
    })
}

function ChangeWorkshop() {
    var workshopid = $('#otherService_WorkshopId').val();
    alert(workshopid);
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeWorkshop",
        data: {
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var branch = "<option value='0'>--Select  Branch--</option>";
            for (var x = 0; x < response.length; x++) {
                branch += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_WorkshopBranchId").html(branch).show();


        }
    })
}

function ChangeBranch() {
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeBranch",
        data: {
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var category = "<option value='0'>--Select  Category--</option>";
            for (var x = 0; x < response.length; x++) {
                category += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_CategoryId").html(category).show();


        }
    })
}

function ChangeCategory() {
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }

    $.ajax({
        type: "POST",
        url: "/Contract/ChangeCategory",
        data: {
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var prsub = "<option value='0'>--Select  Product Subgroup--</option>";
            for (var x = 0; x < response.length; x++) {
                prsub += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_ProductSubGroupId").html(prsub).show();
        }
    })
}

function ChangeProductSubGroup() {
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeProductSubgroup",
        data: {
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var ccrange = "<option value='0'>--Select  CC Range--</option>";
            for (var x = 0; x < response.length; x++) {
                ccrange += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_CCRangeId").html(ccrange).show();
        }
    })
}

function ChangeCCRange() {
    var ccrange = $('#otherService_CCRangeId').val();
    if (ccrange == "") {
        ccrange = 0;
    }
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeCCRange",
        data: {
            CcRangeId: ccrange,
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var mandur = "<option value='0'>--Select  Man-Duration--</option>";
            for (var x = 0; x < response.length; x++) {
                mandur += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_ManuDuration").html(mandur).show();
        }
    })
}

function ChangeManDuration() {
    var mandur = $('#otherService_ManuDuration').val();
    if (mandur == "") {
        mandur = 0;
    }
    var ccrange = $('#otherService_CCRangeId').val();
    if (ccrange == "") {
        ccrange = 0;
    }
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeManDuration",
        data: {
            ManDuration: mandur,
            CcRangeId: ccrange,
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var mancuttoff = "<option value='0'>--Select  ManuCuttoff--</option>";
            for (var x = 0; x < response.length; x++) {
                mancuttoff += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_ManuCuttoff").html(mancuttoff).show();
        }
    })
}

function ChangeManuCuttoff() {
    var cuttoff = $('#otherService_ManuCuttoff').val();
    if (cuttoff == "") {
        cuttoff = 0;
    }
    var mandur = $('#otherService_ManuDuration').val();
    if (mandur == "") {
        mandur = 0;
    }
    var ccrange = $('#otherService_CCRangeId').val();
    if (ccrange == "") {
        ccrange = 0;
    }
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }

    $.ajax({
        type: "POST",
        url: "/Contract/ChangeManuCuttOff",
        data: {
            ManCuttoff: cuttoff,
            ManDuration: mandur,
            CcRangeId: ccrange,
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid
        },
        success: function (response) {
            var duration = "<option value='0'>--Select  Duration--</option>";
            for (var x = 0; x < response.length; x++) {
                duration += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_Duration").html(duration).show();
        }
    })
}

function ChangeDuration() {
    var dur = $('#otherService_Duration').val();
    if (dur == "") {
        dur = 0;
    }
    var cuttoff = $('#otherService_ManuCuttoff').val();
    if (cuttoff == "") {
        cuttoff = 0;
    }
    var mandur = $('#otherService_ManuDuration').val();
    if (mandur == "") {
        mandur = 0;
    }
    var ccrange = $('#otherService_CCRangeId').val();
    if (ccrange == "") {
        ccrange = 0;
    }
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }

    $.ajax({
        type: "POST",
        url: "/Contract/ChangeDurationOther",
        data: {
            DurationId: dur,
            ManCuttoff: cuttoff,
            ManDuration: mandur,
            CcRangeId: ccrange,
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid 
        },
        success: function (response) {
            var cutoff = "<option value='0'>--Select  Milage Cutoff --</option>";
            for (var x = 0; x < response.length; x++) {
                cutoff += "<option value=" + response[x].value + ">" + response[x].text + "</option>";
            }
            $("#otherService_MilageCuttoff").html(cutoff).show();
        }
    })
}

function ChangeMilageCuttoff() {
    var mcutoff = $('#otherService_MilageCuttoff').val();
    if (mcutoff == "") {
        mcutoff = 0;
    }
    var dur = $('#otherService_Duration').val();
    if (dur == "") {
        dur = 0;
    }
    var cuttoff = $('#otherService_ManuCuttoff').val();
    if (cuttoff == "") {
        cuttoff = 0;
    }
    var mandur = $('#otherService_ManuDuration').val();
    if (mandur == "") {
        mandur = 0;
    }
    var ccrange = $('#otherService_CCRangeId').val();
    if (ccrange == "") {
        ccrange = 0;
    }
    var prdsub = $('#otherService_ProductSubGroupId').val();
    if (prdsub == "") {
        prdsub = 0;
    }
    var categoryid = $('#otherService_CategoryId').val();
    if (categoryid == "") {
        categoryid = 0;
    }
    var branchid = $('#otherService_WorkshopBranchId').val();
    if (branchid == "") {
        branchid = 0;
    }
    var workshopid = $('#otherService_WorkshopId').val();
    if (workshopid == "") {
        workshopid = 0;
    }
    var serviceid = $('#otherService_OtherServiceId').val();
    if (serviceid == "") {
        serviceid = 0;
    }
    var minpremium = 0;
    var premium = 0;
    $.ajax({
        type: "POST",
        url: "/Contract/ChangeMilageCuttoff",
        data: {
            mcutoff: mcutoff,
            DurationId: dur,
            ManCuttoff: cuttoff,
            ManDuration: mandur,
            CcRangeId: ccrange,
            ProductSubgroupId: prdsub,
            CategoryId: categoryid,
            BranchId: branchid,
            WorkshopId: workshopid,
            OtherServiceId: serviceid 
        },
        success: function (response) {
            minpremium = response[0];
            premium = response[0];           
        }
    })
    document.getElementById('otherService_MinPremium').value = minpremium;
    document.getElementById('otherService_Premium').value = premium; 
}




