﻿var ALERT_TYPE_RED = 'red'
var ALERT_TYPE_GREEN = 'green'
var ALERT_TYPE_BLUE = 'blue'
var ALERT_TYPE_PURPLE = 'purple'
var ALERT_TYPE_ORANGE = 'orange'
var ALERT_TYPE_DARK = 'dark'





function ShowAlertPopup(content, title, type, icon, backgroundDismiss) {
    $.alert({
        icon: icon,
        title: title,
        content: content,
        type: type,
        backgroundDismiss: backgroundDismiss,
        buttons: {
            OK: {
            }
        }
    });
}