﻿$(document).ready(function () {

    //Populate Contact
    LoadDesignation();
});
function LoadDesignation() {
    $('#update_panel').html('Loading Data...');

    $.ajax({
        url: '/Designation/GetDesignation',
        type: 'GET',
        dataType: 'json',
        success: function (d) {
            if (d.length > 0) {
                var $data = $('<table></table>').addClass('table table-responsive table-striped');
                var header = "<thead><tr><th>Name</th><th>Code</th><th></th></tr></thead>";
                $data.append(header);

                $.each(d, function (i, row) {
                    var $row = $('<tr/>');
                    $row.append($('<td/>').html(row.Name));
                    $row.append($('<td/>').html(row.Code));
                  
                    $row.append($('<td/>').html("<a href='/Department/Save/" + row.ID + "' class='popup'>Edit</a>&nbsp;|&nbsp;<a href='/Department/Delete/" + row.ID + "' class='popup'>Delete</a>"));
                    $data.append($row);
                });

                $('#update_panel').html($data);
            }
            else {
                var $noData = $('<div/>').html('No Data Found!');
                $('#update_panel').html($noData);
            }
        },
        error: function () {
            alert('Error! Please try again.');
        }
    });

}
