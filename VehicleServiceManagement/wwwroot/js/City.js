﻿$.get("/Company/GetCityList", null, DataBind);

function DataBind(CityList) {


    var SetData = $("#SetCityList");
    for (var i = 0; i < CityList.length; i++) {

        var Data = "<tr class='row_" + CityList[i].id + "'>" +
            "<td>" + CityList[i].id + "</td>" +
            "<td>" + CityList[i].cityname + "</td>" +
            "<td>" + CityList[i].countryid + "</td>" +
            "<td>" + CityList[i].code + "</td>" +
            "<td>" + CityList[i].description + "</td>" +
            "<td>" + CityList[i].remarks + "</td>" +
            "<td>" + "<a href='#' class='btn btn-warning' onclick='EditStudentRecord(" + CityList[i].id + ")' ><span class='glyphicon glyphicon-edit'></span></a>" + "</td>" +
            "<td>" + "<a href='#' class='btn btn-danger' onclick='DeleteStudentRecord(" + CityList[i].id + ")'><span class='glyphicon glyphicon-trash'></span></a>" + "</td>" +
            "</tr>";

        SetData.append(Data);

    }
}

function AddNewCity(Id) {
    
    $("#form")[0].reset();
    $("#ModalTitle").html("Add New City");
    $("#MyModal").modal();
}



