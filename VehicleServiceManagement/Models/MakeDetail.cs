﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class MakeDetail
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Make")]
        public string Make { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroupId")]
        public int ProductGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroup")]
        public string ProductGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Please Enter  BranchName")]
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please Enter  Remarks")]
        public string Remarks { get; set; }

        public string IdCategory { get; set; }

        public int create(MakeDetail obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddMakeDetail");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Make", obj.Make);
            cmd.Parameters.AddWithValue("@ProductGroupId", obj.ProductGroupId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);

            i = DbConnection.Create(cmd);

            return i;

        }

        public List<MakeDetail> GetAllMakeDetail()
        {

            List<MakeDetail> make = new List<MakeDetail>();

            SqlCommand com = new SqlCommand("ReadMakeDetail");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                make.Add(

                       new MakeDetail
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           Make = Convert.ToString(dr["Make"]),
                           ProductGroupId = Convert.ToInt32(dr["ProductGroupId"]),
                           ProductGroup = Convert.ToString(dr["ProductGroup"]),
                           CategoryId = Convert.ToInt32(dr["CategoryId"]),
                           CategoryName = Convert.ToString(dr["CategoryName"]),
                           Remarks = Convert.ToString(dr["Remarks"]),
                           IdCategory= Convert.ToInt32(dr["Id"])+","+ Convert.ToString(dr["CategoryName"])
                       }

                       );
            }

            return make;

        }

        public List<MakeDetail> GetAllMakeByProduct(int prgroupid)
        {

            List<MakeDetail> make = new List<MakeDetail>();

            SqlCommand com = new SqlCommand("GetMakeByProduct");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ProductGroupId", prgroupid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                make.Add(

                       new MakeDetail
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           Make = Convert.ToString(dr["Make"]),                          
                           IdCategory = Convert.ToInt32(dr["Id"]) + "," + Convert.ToInt32(dr["CategoryId"]) + "," + Convert.ToString(dr["CategoryName"])
                       }

                       );
            }

            return make;

        }


        public MakeDetail details(int id)
        {
            MakeDetail con = new MakeDetail();
            SqlCommand com = new SqlCommand("MakeDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.Make = Convert.ToString(dr["Make"]);
                con.ProductGroupId = Convert.ToInt32(dr["ProductGroupId"]);
                con.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                con.Remarks = Convert.ToString(dr["Remarks"]);

            }


            return con;

        }
        public int update(MakeDetail obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("MakeDetailUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@Make", obj.Make);
            cmd.Parameters.AddWithValue("@ProductGroupId", obj.ProductGroupId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("MakeDetailDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }

    }

    public class MakeDetailDisplay
    {
        public List<MakeDetail> makeList { get; set; }
        public UserPermission userP { get; set; }
    }
}
