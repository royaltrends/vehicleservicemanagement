﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Location
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Location Name")]
        public string LocationName { get; set; }     
   
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Select Location Type")]
        public string Type { get; set; }
        [Required(ErrorMessage = "Please Select Country")]
        public int Country { get; set; }
        
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int City { get; set; }
        
        public string CityName { get; set; }
  
        public string Remarks { get; set; }
 

        public List<Location> locationlist { get; set; }
        /**Used***/
        public int create(Location obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddLocation");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("LocationName", obj.LocationName);
            cmd.Parameters.AddWithValue("Address", obj.Address);
            cmd.Parameters.AddWithValue("Type", obj.Type);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("City", obj.City);
            cmd.Parameters.AddWithValue("Remarks", obj.Remarks);

            i = DbConnection.Create(cmd);
            return i;
        }
        /**Used***/
        public List<Location> GetAllLocation()
        {

            List<Location> Location = new List<Location>();

            SqlCommand com = new SqlCommand("ReadLocation");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Location.Add(

                    new Location
                    {
                         Id = Convert.ToInt32(dr["Id"]),
                        LocationName = Convert.ToString(dr["LocationName"]),                     
                        Address = Convert.ToString(dr["Address"]),
                        Type = Convert.ToString(dr["Type"]),
                        Country = Convert.ToInt32(dr["Country"]),
                        CountryName = Convert.ToString(dr["CountryName"]),
                        City = Convert.ToInt32(dr["City"]),
                        CityName = Convert.ToString(dr["CityName"]),
                        Remarks = Convert.ToString(dr["Remarks"])
                    }
                    );
            }
            return Location;
        }

        public List<Location> GetLocationName()
        {

            List<Location> Location = new List<Location>();

            SqlCommand com = new SqlCommand("GetLocationName");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                Location.Add(new Location
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        LocationName = Convert.ToString(dr["LocationName"]),                      
                    });
            }
            return Location;
        }

        /**Used***/
        public Location details(int id)
        {
            Location obj = new Location();
            SqlCommand com = new SqlCommand("ViewLocationDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.LocationName = dr["LocationName"].ToString();         
            obj.Address = dr["Address"].ToString();
            obj.Type = dr["Type"].ToString();
            obj.Country = Convert.ToInt32(dr["Country"]);
            obj.City = Convert.ToInt32(dr["City"]);
            obj.Remarks = dr["Remarks"].ToString();
            return (obj);
        }
        /**Used***/
        public int update(Location obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateLocation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("LocationName", obj.LocationName);        
            cmd.Parameters.AddWithValue("Address", obj.Address);
            cmd.Parameters.AddWithValue("Type", obj.Type);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("City", obj.City);
            cmd.Parameters.AddWithValue("Remarks", obj.Remarks);


            i = DbConnection.Update(cmd);
            return i;
        }
        /**Used***/
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteLocation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }
}
