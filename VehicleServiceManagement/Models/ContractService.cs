﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ContractService
    {
        public ContractInformation contractInformation { get; set; }
        public int ServiceInvoice { get; set; }
        public int ledgerId { get; set; }
        public string ContractNumber { get; set; }
        public string vehiclenumber { get; set; }
        public string chasisnumber { get; set; }
        public string dealername { get; set; }
        public string branchname { get; set; }
        public string customername { get; set; }
        public string customeraddress { get; set; }
        public float serviceamount { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public int create(ContractService obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddcontractService");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ContractNumber", obj.ContractNumber);
            cmd.Parameters.AddWithValue("@serviceamount", obj.serviceamount);
            cmd.Parameters.AddWithValue("@Date", obj.Date);
            i = DbConnection.Create(cmd);
            return i;
        }

        public List<ContractService> GetAllContractService()
        {

            List<ContractService> Country = new List<ContractService>();

            SqlCommand com = new SqlCommand("ReadContractService");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Country.Add(
                    new ContractService
                    {
                        ServiceInvoice = Convert.ToInt32(dr["ServiceInvoice"]),
                        ContractNumber = Convert.ToString(dr["ContractNumber"]),
                        serviceamount = Convert.ToSingle(dr["serviceamount"]),
                        Date = Convert.ToDateTime(dr["Date"])
                    }
                    );
            }
            return Country;
        }

        public ContractService details(int ServiceInvoice)
        {

            ContractService con = new ContractService();
            SqlCommand com = new SqlCommand("GetContractService");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ServiceInvoice", ServiceInvoice);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            con.ServiceInvoice = Convert.ToInt32(dr["ServiceInvoice"]);
            con.ContractNumber = Convert.ToString(dr["ContractNumber"]);
            con.serviceamount = Convert.ToSingle(dr["serviceamount"]);
            con.Date = Convert.ToDateTime(dr["Date"]);
            return con;
        }

        public int update(ContractService obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateContractService");
            cmd = new SqlCommand("AddcontractService");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ServiceInvoice", obj.ServiceInvoice);
            cmd.Parameters.AddWithValue("@ContractNumber", obj.ContractNumber);

            cmd.Parameters.AddWithValue("@serviceamount", obj.serviceamount);
            cmd.Parameters.AddWithValue("@Date", obj.Date);
            i = DbConnection.Update(cmd);
            return i;
        }

        public int GetLedgerbyContractnumber(string contractnumber)
        {
            int ledgerid = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("GetLedgerbyContractnumber");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ContractNumber", contractnumber);
            DataTable dt = DbConnection.Read(cmd);
            if (dt.Rows.Count > 0)
            {
                ledgerid = Convert.ToInt32(dt.Rows[0][0]);
            }
            return ledgerid;
        }
    }
}
