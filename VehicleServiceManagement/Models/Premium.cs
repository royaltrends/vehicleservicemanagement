﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{

    public class Premium
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Range")]
        public int CCRangeId { get; set; }
        public string Range { get; set; }
        [Required(ErrorMessage = "Please Enter  ClaimLimit")]
        public int ClaimLimitId { get; set; }

        public string CategoryName { get; set; }
        public string ClaimLimit { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  DealerName")]
        public int DealerId { get; set; }
        public string Name { get; set; }
       
        public string SubGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  Duration")]
        public int ExtDurationId { get; set; }
        [Required(ErrorMessage = "Please Enter  Duration")]
        public int MaDurationId { get; set; }
        public string Duration { get; set; }
        [Required(ErrorMessage = "Please Enter  MilageCutoff")]
        public int MaMileageCutoffId { get; set; }
      
        public float ManufacturerCutoff { get; set; }
        [Required(ErrorMessage = "Please Enter  MilageCutoff")]
        public int ExtMilaegeCutoffId { get; set; }
        public float MilageCutoff { get; set; }
        [Required(ErrorMessage = "Please Enter  FromDate")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }
        [Required(ErrorMessage = "Please Enter  ToDate")]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }
        [Required(ErrorMessage = "Please Enter  MinimumAmount")]
      
        public float MinimumAmount { get; set; }
        [Required(ErrorMessage = "Please Enter  PremiumAmount")]
        public float PremiumAmount { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public int create(Premium obj)
        {
             int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddPremium");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@ClaimLimitId", obj.ClaimLimitId);
            cmd.Parameters.AddWithValue("@CCRangeId", obj.CCRangeId);

            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@MaDurationId", obj.MaDurationId);

            cmd.Parameters.AddWithValue("@ExtDurationId", obj.ExtDurationId);
            cmd.Parameters.AddWithValue("@MaMileageCutoffId", obj.MaMileageCutoffId);
            cmd.Parameters.AddWithValue("@ExtMilaegeCutoffId", obj.ExtMilaegeCutoffId);
            cmd.Parameters.AddWithValue("@FromDate", obj.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", obj.ToDate);
            cmd.Parameters.AddWithValue("@MinimumAmount", obj.MinimumAmount);
            cmd.Parameters.AddWithValue("@PremiumAmount", obj.PremiumAmount);
           
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);


            i = DbConnection.Create(cmd);
            return i;
        }
        public List<Premium> GetAllPremium()
        {

            List<Premium> Infromation = new List<Premium>();

            SqlCommand com = new SqlCommand("ReadPremium");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new Premium
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        ClaimLimitId = Convert.ToInt32(dr["ClaimLimitId"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        Name = Convert.ToString(dr["Name"]),
                        ClaimLimit = Convert.ToString(dr["ClaimLimit"]),
                        CCRangeId = Convert.ToInt32(dr["CCRangeId"]),

                        CategoryId = Convert.ToInt32(dr["CategoryId"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        MaDurationId = Convert.ToInt32(dr["MaDurationId"]),
                        ExtDurationId = Convert.ToInt32(dr["ExtDurationId"]),
                        MaMileageCutoffId = Convert.ToInt32(dr["MaMileageCutoffId"]),
                        ExtMilaegeCutoffId = Convert.ToInt32(dr["ExtMilaegeCutoffId"]),

                         FromDate = Convert.ToDateTime(dr["FromDate"]),
                        ToDate = Convert.ToDateTime(dr["ToDate"]),
                        MinimumAmount = Convert.ToSingle(dr["MinimumAmount"]),
                        PremiumAmount = Convert.ToSingle(dr["PremiumAmount"]),
                        Remark = Convert.ToString(dr["Remark"]),
                        Range = Convert.ToString(dr["Range"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        Duration = Convert.ToString(dr["Duration"]),
                        ManufacturerCutoff = Convert.ToSingle(dr["ManufacturerCutoff"]),
                        MilageCutoff = Convert.ToSingle(dr["MilageCutoff"])

                    }
                    );
            }

            return Infromation;

        }
        public Premium details(int id)
        {
            Premium obj = new Premium();
            SqlCommand com = new SqlCommand("ViewPremium");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.ClaimLimitId = Convert.ToInt32(dr["ClaimLimitId"]);
            obj.CCRangeId = Convert.ToInt32(dr["CCRangeId"]);
            obj.DealerId = Convert.ToInt32(dr["DealerId"]);
            obj.CategoryId = Convert.ToInt32(dr["CategoryId"]);
            obj.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
            obj.MaDurationId = Convert.ToInt32(dr["MaDurationId"]);
            obj.ExtDurationId = Convert.ToInt32(dr["ExtDurationId"]);
            obj.MaMileageCutoffId = Convert.ToInt32(dr["MaMileageCutoffId"]);
            obj.ExtMilaegeCutoffId = Convert.ToInt32(dr["ExtMilaegeCutoffId"]);

            obj.FromDate = Convert.ToDateTime(dr["FromDate"]);
            obj.ToDate = Convert.ToDateTime(dr["ToDate"]);
            obj.MinimumAmount = Convert.ToSingle(dr["MinimumAmount"]);
            obj.PremiumAmount = Convert.ToSingle(dr["PremiumAmount"]);
            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);


        }
        public int update(Premium obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdatePremium");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@ClaimLimitId", obj.ClaimLimitId);
            cmd.Parameters.AddWithValue("@CCRangeId", obj.CCRangeId);
           // cmd.Parameters.AddWithValue("@WorkShopId", obj.WorkShopId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@MaDurationId", obj.MaDurationId);
            cmd.Parameters.AddWithValue("@ExtDurationId", obj.ExtDurationId);
            cmd.Parameters.AddWithValue("@MaMileageCutoffId", obj.MaMileageCutoffId);
            cmd.Parameters.AddWithValue("@ExtMilaegeCutoffId", obj.ExtMilaegeCutoffId);
            cmd.Parameters.AddWithValue("@FromDate", obj.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", obj.ToDate);
            cmd.Parameters.AddWithValue("@MinimumAmount", obj.MinimumAmount);
            cmd.Parameters.AddWithValue("@PremiumAmount", obj.PremiumAmount);
           // cmd.Parameters.AddWithValue("@WorkShopBranch", obj.PremiumAmount);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeletePremium");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;
        }

        public int getcount()
        {
            int count = 0;
            SqlCommand com = new SqlCommand("GetPremiumCount");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                count = Convert.ToInt32(dt.Rows[0]["valuecount"]);
            }
            return count;
        }

    }

    public class PremiumDisplay
    {
        public List<Premium> premiumList { get; set; }
        public UserPermission userP { get; set; }
    }
}

