﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class EmployeeBasicInformation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Employee ID")]
        public string EmployeeCode { get; set; }
        [Required(ErrorMessage = "Please Enter Employee Name")]
        public string  Name { get; set; }
      
        public string Gender { get; set; }
        [Required(ErrorMessage = "Please Enter  FromDate")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string AddressInHomeCountry { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public int EmployeeFileNo { get; set; }
      
        public int Designation { get; set; }
        public int Department { get; set; }
        public float Salary { get; set; }
        public int create(EmployeeBasicInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddEmployeeBasicInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmployeeCode", obj.EmployeeCode);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@Gender", obj.Gender);
            cmd.Parameters.AddWithValue("@DateOfBirth", obj.DateOfBirth);
            cmd.Parameters.AddWithValue("@Nationality", obj.Nationality);
            cmd.Parameters.AddWithValue("@AddressInHomeCountry", obj.AddressInHomeCountry);
            cmd.Parameters.AddWithValue("@MobileNo", obj.MobileNo);
            cmd.Parameters.AddWithValue("@Email", obj.Email);
            cmd.Parameters.AddWithValue("@EmployeeFileNo", obj.EmployeeFileNo);
            cmd.Parameters.AddWithValue("@Designation", obj.Designation);
            cmd.Parameters.AddWithValue("@Department", obj.Department);
            cmd.Parameters.AddWithValue("@Salary", obj.Salary);


            i = DbConnection.Create(cmd);
            return i;
        }
               
        public List<EmployeeBasicInformation> GetEmployeeMaster()
        {
            List<EmployeeBasicInformation> master = new List<EmployeeBasicInformation>();

            SqlCommand com = new SqlCommand("spGetAllEmployee");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {
                
                master.Add(

                    new EmployeeBasicInformation
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        MobileNo = Convert.ToString(dr["MobileNo"]),
                     
                    }

                    );
            }

            return master;


        }

        public List<EmployeeBasicInformation> GetEmployeeMasterByDepartment()
        {
            List<EmployeeBasicInformation> master = new List<EmployeeBasicInformation>();

            SqlCommand com = new SqlCommand("GetEmployeeByDepartment");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Department", Department);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new EmployeeBasicInformation
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                    }

                    );
            }

            return master;

        }

        public EmployeeBasicInformation details(int id)
        {

            EmployeeBasicInformation dsg = new EmployeeBasicInformation();
            SqlCommand com = new SqlCommand("GetEmployeeBasicInformation");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            dsg.Id = Convert.ToInt32(dr["Id"]);
            dsg.EmployeeCode = Convert.ToString(dr["EmployeeCode"]);
            dsg.Name = Convert.ToString(dr["Name"]);
            dsg.Gender = Convert.ToString(dr["Gender"]);
            dsg.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"]);
            dsg.Nationality = Convert.ToString(dr["Nationality"]);
            dsg.AddressInHomeCountry = Convert.ToString(dr["AddressInHomeCountry"]);
            dsg.MobileNo = Convert.ToString(dr["MobileNo"]);
            dsg.Email = Convert.ToString(dr["Email"]);
            dsg.EmployeeFileNo = Convert.ToInt32(dr["EmployeeFileNo"]);
            dsg.Designation = Convert.ToInt32(dr["Designation"]);
            dsg.Department = Convert.ToInt32(dr["Department"]);
            dsg.Salary = Convert.ToSingle(dr["Salary"]);
            return (dsg);

        }

        public int update(EmployeeBasicInformation obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateEmployeeBasicInformation");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@mployeeCode", obj.EmployeeCode);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@Gender", obj.Gender);
            cmd.Parameters.AddWithValue("@DateOfBirth", obj.DateOfBirth);
            cmd.Parameters.AddWithValue("@Nationality", obj.Nationality);
            cmd.Parameters.AddWithValue("@AddressInHomeCountry", obj.AddressInHomeCountry);
            cmd.Parameters.AddWithValue("@MobileNo", obj.MobileNo);
            cmd.Parameters.AddWithValue("@Email", obj.Email);
            cmd.Parameters.AddWithValue("@EmployeeFileNo", obj.EmployeeFileNo);
            cmd.Parameters.AddWithValue("@Designation", obj.Designation);
            cmd.Parameters.AddWithValue("@Department", obj.Department);
            cmd.Parameters.AddWithValue("@Salary", obj.Salary);
            i = DbConnection.Update(cmd);
            return i;


        }

        public int delete(int id)
        {
             int i = 0;
            SqlCommand com = new SqlCommand("DeleteEmployeeBasicInformation");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }
        
    }

    public class EmployeeBasicInformationDisplay
    {
        public List<EmployeeBasicInformation> employeeList { get; set; }
        public UserPermission userP { get; set; }
    }
}
