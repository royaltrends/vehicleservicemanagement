﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class UserSettings
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Login ID")]
        public string LoginId { get; set; }
        [Required(ErrorMessage = "Please Enter User Name")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please Enter Password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please Enter Confirm Password")]
        public string ConfirmPassword { get; set; }
        public string UserGroup { get; set; }
        public int Employee { get; set; }
        public bool AllLocationAllowed { get; set; }
        public bool SpecificLocationAllowed { get; set; }
        public int Location { get; set; }
       // public string LocationName { get; set; }
        public string LocationList { get; set; }
       // public bool yes { get; set; }
       // public bool no { get; set; }
       //  public long SelectedItem { get; set; }


        public List<UserSettings> UserSettingslist { get; set; }

        //public List<BasicEmployee> EmpList { get; set; }

        public int create(UserSettings obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddUserSettings");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("LoginId", obj.LoginId);
            cmd.Parameters.AddWithValue("Username", obj.Username);
            cmd.Parameters.AddWithValue("Password", obj.Password);
            cmd.Parameters.AddWithValue("UserGroup", obj.UserGroup);
            cmd.Parameters.AddWithValue("Employee", obj.Employee);
            cmd.Parameters.AddWithValue("AllLocationAllowed", obj.AllLocationAllowed);
            cmd.Parameters.AddWithValue("SpecificLocationAllowed", obj.SpecificLocationAllowed);
            cmd.Parameters.AddWithValue("Location", obj.Location);
            cmd.Parameters.AddWithValue("LocationList", obj.LocationList);

            i = DbConnection.Create(cmd);
            return i;
        }

        public List<UserSettings> GetAllUsers()

        {
            List<UserSettings> lsUser = new List<UserSettings>();
            SqlCommand cmd;
            cmd = new SqlCommand("GetAllUserSettings");
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(cmd);
            lsUser = (from DataRow dr in dt.Rows
                      select new UserSettings()
                      {
                          Id = Convert.ToInt32(dr["Id"]),
                          LoginId = Convert.ToString(dr["LoginId"]),
                          Username = Convert.ToString(dr["Username"]),
                          Password = Convert.ToString(dr["Password"]),
                          UserGroup = Convert.ToString(dr["UserGroup"]),
                          Employee = Convert.ToInt32(dr["Employee"]),
                          AllLocationAllowed = Convert.ToBoolean(dr["AllLocationAllowed"]),
                          SpecificLocationAllowed = Convert.ToBoolean(dr["SpecificLocationAllowed"]),
                          Location = Convert.ToInt32(dr["Location"]),
                          LocationList = Convert.ToString(dr["LocationList"])                  


                      }).ToList();
            return lsUser;
        }





        public UserSettings details(int id)
        {
            UserSettings con = new UserSettings();
            SqlCommand com = new SqlCommand("UserSettingsDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.LoginId = Convert.ToString(dr["LoginId"]);
                con.Username = Convert.ToString(dr["Username"]);
                con.Password = Convert.ToString(dr["Password"]);
                con.UserGroup = Convert.ToString(dr["UserGroup"]);
                

            }


            return con;

        }
        public int update(UserSettings obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UserSettingsUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("LoginId", obj.LoginId);
            cmd.Parameters.AddWithValue("Username", obj.Username);
            cmd.Parameters.AddWithValue("Password", obj.Password);
            cmd.Parameters.AddWithValue("UserGroup", obj.UserGroup);
            cmd.Parameters.AddWithValue("Employee", obj.Employee);
            cmd.Parameters.AddWithValue("AllLocationAllowed", obj.AllLocationAllowed);
            cmd.Parameters.AddWithValue("SpecificLocationAllowed", obj.SpecificLocationAllowed);
            cmd.Parameters.AddWithValue("Location", obj.Location);
            cmd.Parameters.AddWithValue("LocationList", obj.LocationList);

            i = DbConnection.Update(cmd);
            return i;


        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("UserSettingsDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }
                             

        //public List<BasicEmployee> GetAllEmployees()
        //{
        //    List<BasicEmployee> objlist = new List<BasicEmployee>();
        //    SqlCommand cmd;
        //    cmd = new SqlCommand("GetAllEmployeeBasic");
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    DataTable dt = DbConnection.Read(cmd);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        BasicEmployee obj = new BasicEmployee();
        //        obj.Id = Convert.ToInt32(dt.Rows[i]["Id"]);
        //        obj.Name = Convert.ToString(dt.Rows[i]["Name"]);
        //        objlist.Add(obj);
        //    }
        //    return objlist;
        //}

        //public enum enumUserModulePermission
        //{
        //    Company = 1,
        //    Tools = 2,
        //    Purchase = 3,
        //    Inventory = 4,
        //    Sales = 5,
        //    Financial = 6,
        //    Banking = 7,
        //    Payroll = 8,
        //    VAT = 9,
        //    Help = 10,
    }

    public class UserSettingsDisplay
    {
        public List<UserSettings> userList { get; set; }
        public UserPermission userP { get; set; }
    }

    //    public List<UserSettings> GetAllUser()
    //    {

    //        List<UserSettings> userSettings = new List<UserSettings>();

    //        SqlCommand com = new SqlCommand("ReadUser");
    //        com.CommandType = CommandType.StoredProcedure;
    //        DataTable dt = new DataTable();
    //        dt = DbConnection.Read(com);
    //        //Bind EmpModel generic list using dataRow
    //        foreach (DataRow dr in dt.Rows)
    //        {

    //            userSettings.Add(

    //            new UserSettings
    //            {

    //                Id = Convert.ToInt32(dr["Id"]),
    //                LoginId = Convert.ToString(dr["LoginId"]),
    //                Username = Convert.ToString(dr["Username"]),
    //                Password = Convert.ToString(dr["Password"]),
    //                UserGroup = Convert.ToString(dr["UserGroup"]),
    //                // LocationAllowed = Convert.ToInt32(dr["LocationAllowed"]),
    //                //LocationName = Convert.ToString(dr["LocationName"])

    //            }


    //            );
    //        }

    //        return userSettings;

    //    }

    //}

    //public class BasicEmployee
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }

    //}

}