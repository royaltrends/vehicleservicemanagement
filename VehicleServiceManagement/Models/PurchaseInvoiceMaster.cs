﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using VehicleServiceManagement.Models;

namespace BusinessSoftware.Models
{
    public class PurchaseMaster
    {

        public int Id { get; set; }
        public int DealerLedgerId { get; set; }
        public string DealerName { get; set; }
       
        [StringLength(60, MinimumLength = 3)]      
        public string Address { get; set; }
      
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
       
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PolicyNumber { get; set; }
        public int DealerInvoiceNo { get; set; }     
        public bool Status { get; set; }

        [StringLength(60, MinimumLength = 3)]
        public string Reference { get; set; }

        [StringLength(60, MinimumLength = 3)]     
        public string Remarks { get; set; }

        public float PurchaseTotal { get; set; }
        public float Discount { get; set; }
        public float VATAmount { get; set; }        
        public float SubTotal { get; set; }
        public float NetTotal { get; set; }

        public List<PurchaseMaster> GetPurchaseInvoiceMaster()
        {

            List<PurchaseMaster> master = new List<PurchaseMaster>();
            SqlCommand com = new SqlCommand("GetAllPurchase");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {
                master.Add(
                    new PurchaseMaster
                    { 
                        Id = Convert.ToInt32(dr["Id"]),
                        PolicyNumber = Convert.ToString(dr["PolicyNumber"]),
                        DealerName = Convert.ToString(dr["DealerName"]),
                        Date = Convert.ToDateTime(dr["Date"]),
                        EmployeeName = Convert.ToString(dr["Name"]),
                        DealerInvoiceNo = Convert.ToInt32(dr["DealerInvoiceNo"]),                        
                        SubTotal = Convert.ToSingle(dr["SubTotal"]),
                    });
            }
            return master;
        }  
    }

    public class PurchaseMasterDisplay
    {
        public List<PurchaseMaster> purchaseMasterList { get; set; }
        public UserPermission userP { get; set; }
    }
}
