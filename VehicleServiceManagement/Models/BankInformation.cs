﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class BankInformation
    {
         public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Code")]
        public string BankCode { get; set; }
        [Required(ErrorMessage = "Please Enter Bank Name")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Please Enter Branch Name")]
        public string BranchName { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter Swift Code")]
        public string SwiftCode { get; set; }
        
         public string CityName { get; set; }
        [Required(ErrorMessage = "Please Select City")]
        public int City { get; set; }
        [Required(ErrorMessage = "Please Select Country")]
         public int Country{ get; set; }
        public string CountryName { get; set; }
        public string Telephone{ get; set; }
        public string Email { get; set; }
        public string Description { get; set; }

        public string IdBranch { get; set; }

        public int create(BankInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddBankInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("BankCode", obj.BankCode);
            cmd.Parameters.AddWithValue("BankName", obj.BankName);
            cmd.Parameters.AddWithValue("BranchName", obj.BranchName);
            cmd.Parameters.AddWithValue("Address", obj.Address);
            cmd.Parameters.AddWithValue("SwiftCode", obj.SwiftCode);
            cmd.Parameters.AddWithValue("City", obj.City);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("Telephone", obj.Telephone);
            cmd.Parameters.AddWithValue("Email", obj.Email);
            cmd.Parameters.AddWithValue("Description", obj.Description);            
            i = DbConnection.Create(cmd);
            return i;
        }
        public List<BankInformation> GetAllInformation()
        {

            List<BankInformation> bankInformation = new List<BankInformation>();

            SqlCommand com = new SqlCommand("ReadBankInformations");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                bankInformation.Add(

                    new BankInformation
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        BankCode = Convert.ToString(dr["BankCode"]),
                        BankName = Convert.ToString(dr["BankName"]),
                        BranchName = Convert.ToString(dr["BranchName"]),
                        Address = Convert.ToString(dr["Address"]),
                        SwiftCode = Convert.ToString(dr["SwiftCode"]),
                        City = Convert.ToInt32(dr["City"]),
                        CityName = Convert.ToString(dr["CityName"]),
                        Country = Convert.ToInt32(dr["Country"]),
                        CountryName = Convert.ToString(dr["CountryName"]),
                        Telephone = Convert.ToString(dr["Telephone"]),
                        Email = Convert.ToString(dr["Email"]),
                      
                        Description = Convert.ToString(dr["Description"]),
                        IdBranch= Convert.ToString(dr["Id"])+"," + Convert.ToString(dr["BranchName"]),

                    }


                    );
            }

            return bankInformation;

        }
        public BankInformation details(int id)
        {
            BankInformation con = new BankInformation();
            SqlCommand com = new SqlCommand("BankDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id",id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.Id = Convert.ToInt32(dr["Id"]);
                con.BankCode = dr["BankCode"].ToString();
                con.BankName = dr["BankName"].ToString();
                con.BranchName = dr["BranchName"].ToString();
                con.Address = dr["Address"].ToString();
                con.SwiftCode = dr["SwiftCode"].ToString();
                con.City = Convert.ToInt32(dr["City"]);             
                con.Country = Convert.ToInt32(dr["Country"]);
                con.Telephone = dr["Telephone"].ToString();
                con.Email = dr["Email"].ToString();
                con.Description = dr["Description"].ToString();
            }
           

            return con;

        }
        public int update(BankInformation obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("spUpdateBankInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("BankCode", obj.BankCode);
            cmd.Parameters.AddWithValue("BankName", obj.BankName);
            cmd.Parameters.AddWithValue("BranchName", obj.BranchName);
            cmd.Parameters.AddWithValue("Address", obj.Address);
            cmd.Parameters.AddWithValue("SwiftCode", obj.SwiftCode);
            cmd.Parameters.AddWithValue("City", obj.City);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("Telephone", obj.Telephone);
            cmd.Parameters.AddWithValue("Email", obj.Email);
            cmd.Parameters.AddWithValue("Description", obj.Description);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("deletebankinformation");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }






    }
}