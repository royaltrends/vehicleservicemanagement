﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class EntryEligibility
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  AgeLimit")]
        public string AgeLimit { get; set; }
        [Required(ErrorMessage = "Please Enter  EntryMilage")]
        public float EntryMilage { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }

        public string SubGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public int create(EntryEligibility obj)
        {
            int i = 0;

            SqlCommand cmd;
             cmd = new SqlCommand("AddEntryEligibility");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@AgeLimit", obj.AgeLimit);
            cmd.Parameters.AddWithValue("@EntryMilage", obj.EntryMilage);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<EntryEligibility> GetAllEntryEligibility()
        {

            List<EntryEligibility> Infromation = new List<EntryEligibility>();

            SqlCommand com = new SqlCommand("ReadEntryEligibility");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new EntryEligibility
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        AgeLimit = Convert.ToString(dr["AgeLimit"]),
                        EntryMilage = Convert.ToSingle(dr["EntryMilage"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        SubGroup = Convert.ToString(dr["SubGroup"]),
                        
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;

        }
        public EntryEligibility details(int id)
        {
            EntryEligibility obj = new EntryEligibility();
            SqlCommand com = new SqlCommand("ViewEntryEligibility");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.AgeLimit = Convert.ToString(dr["AgeLimit"]);
            obj.EntryMilage = Convert.ToSingle(dr["EntryMilage"]);
            obj.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
           
                        
                      
            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);


        }
        public int update(EntryEligibility obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateEntryEligibility");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@AgeLimit", obj.AgeLimit);
            cmd.Parameters.AddWithValue("@EntryMilage", obj.EntryMilage);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);

            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteEntryEligibility");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class EntryEligibilityDisplay
    {
        public List<EntryEligibility> entryEligibilityList { get; set; }
        public UserPermission userP { get; set; }
    }
}
