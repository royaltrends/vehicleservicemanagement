﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class LoginInformation
    {
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public int EmployeeId { get; set; }

        public LoginInformation GetAllLogin(Login obj)
        {
            LoginInformation objval = new LoginInformation();
          
            if (!string.IsNullOrEmpty(obj.UserName) && !string.IsNullOrEmpty(obj.Password))
            {
                SqlCommand com = new SqlCommand("CheckLogin");
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Username", obj.UserName);
                com.Parameters.AddWithValue("Password", obj.Password);
                DataSet ds = DbConnection.ReadDs(com);
                DataTable dtlogindetail = ds.Tables[0];
                DataTable dtcompanydetails = ds.Tables[1];
                if(dtlogindetail.Rows.Count>0)
                {
                    objval.UserId = Convert.ToInt32(dtlogindetail.Rows[0]["Id"]);
                    objval.UserName = Convert.ToString(dtlogindetail.Rows[0]["Username"]);
                    objval.EmployeeId = Convert.ToInt32(dtlogindetail.Rows[0]["Employee"]);
                    if(dtcompanydetails.Rows.Count>0)
                    {
                        objval.CompanyName = Convert.ToString(dtcompanydetails.Rows[0]["EnglishName"]);
                    }
                }

            }
            return objval;
        }
    }
}
