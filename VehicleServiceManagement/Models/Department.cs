﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Department
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Department Name")]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }
       
        public List<Department> departmentlist { get; set; }
        public int create(Department obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddDepartment");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Name", obj.Name);
            cmd.Parameters.AddWithValue("Code", obj.Code);


            i = DbConnection.Create(cmd);
            return i;
        }

        public List<Department> GetAllDepartment()
        {

            List<Department> Department = new List<Department>();

            SqlCommand com = new SqlCommand("ReadDepartment");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Department.Add(

                    new Department
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        Code = Convert.ToString(dr["Code"]),


                    }


                    );
            }

            return Department;

        }
        public Department details(int id)
        {
            Department obj = new Department();
            SqlCommand com = new SqlCommand("ViewDepartmentDetail");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.Name = dr["Name"].ToString();
          
            obj.Code = dr["Code"].ToString();
           
            return (obj);


        }
        public int update(Department obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateDepartment");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Name", obj.Name);
           
            cmd.Parameters.AddWithValue("Code", obj.Code);
            

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteDepartment");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);
           
            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class DepartmentDisplay
    {
        public List<Department> departmentList { get; set; }
        public UserPermission userP { get; set; }
    }
}

