﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessSoftware.Models
{
    public class PurchaseDetails
    {

        public long Id { get; set; }
        public long PurchaseId { get; set; }
        public string Particular { get; set; }       
        public int Quantity { get; set; }     
        public float Price { get; set; }       
        public float TotalPrice { get; set; }
        public string Vatcategory { get; set; }
        public float Vat { get; set; }
        public float Total { get; set; } 

        public string UID { get; set; }

    }
}
