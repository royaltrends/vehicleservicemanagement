﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class CategoryDetail
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Name")]
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }

        public int create(CategoryDetail obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddCategoryDetail");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryName", obj.CategoryName);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);

            return i;

        }

        public List<CategoryDetail> GetAllCategoryDetail()
        {

            List<CategoryDetail> catagory = new List<CategoryDetail>();

            SqlCommand com = new SqlCommand("ReadCategoryDetail");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                catagory.Add(

                       new CategoryDetail
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           CategoryName = Convert.ToString(dr["CategoryName"]),
                           Remark = Convert.ToString(dr["Remark"])

                       }


                       );
            }

            return catagory;

        }


        public CategoryDetail details(int id)
        {
            CategoryDetail con = new CategoryDetail();
            SqlCommand com = new SqlCommand("CategoryDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.CategoryName = Convert.ToString(dr["CategoryName"]);
                con.Remark = Convert.ToString(dr["Remark"]);


            }


            return con;

        }
        public int update(CategoryDetail obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("CategoryDetailUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("CategoryName", obj.CategoryName);
            cmd.Parameters.AddWithValue("Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("CategoryDetailDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }




    }

    public class CategoryDetailDisplay
    {
        public List<CategoryDetail> categoryList { get; set; }
        public UserPermission userP { get; set; }
    }
}
