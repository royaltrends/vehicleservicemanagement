﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class DbConnection
    {


        public static string ConnStr = "Data Source=SQL5050.site4now.net;Initial Catalog=DB_A50DBF_MotorFahad;User Id=DB_A50DBF_MotorFahad_admin;Password=evisualSOFT2020;";

        public static DataTable Read(SqlCommand cmd)
        {

            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConnStr))
                try
                {
                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        cmd.Connection = con;
                        con.Open();
                        dt.Locale = System.Globalization.CultureInfo.InvariantCulture;
                        adp.Fill(dt);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }

            return dt;
        }

        public static DataSet ReadDs(SqlCommand cmd)
        {

            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnStr))
                try
                {
                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        cmd.Connection = con;
                        con.Open();                        
                        adp.Fill(ds);
                       
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }

            return ds;
        }

        public static int Create(SqlCommand cmd)
        {
            // string ConnStr = "Server=DESKTOP-9PLIM4G\\SQLEXPRESS;Database=AccountsDb;Trusted_Connection=True;MultipleActiveResultSets=true";
            int res = 0;

            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {                    
                    con.Close();
                }
            }

            return res;
        }

        public static long CreatewithId(SqlCommand cmd)
        {
            // string ConnStr = "Server=DESKTOP-9PLIM4G\\SQLEXPRESS;Database=AccountsDb;Trusted_Connection=True;MultipleActiveResultSets=true";
            object res = 0;

            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    res = 0;
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }

            return Convert.ToInt64( res);
        }

        public static int Update(SqlCommand cmd)
        {
            //string ConnStr = "Server=DESKTOP-9PLIM4G\\SQLEXPRESS;Database=AccountsDb;Trusted_Connection=True;MultipleActiveResultSets=true";
            int res = 0;

            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }

            return res;
        }

        public static int Delete(SqlCommand cmd)
        {
            //string ConnStr = "Server=DESKTOP-9PLIM4G\\SQLEXPRESS;Database=AccountsDb;Trusted_Connection=True;MultipleActiveResultSets=true";
            int res = 0;

            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    res = 0;
                }
                finally
                {
                    con.Close();
                }
            }

            return res;
        }

        public static void BulkInsert(DataTable table, string tableName)
        {
            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    con.Open();
                    using (var bulk = new SqlBulkCopy(con))
                    {
                        bulk.DestinationTableName = tableName;
                        bulk.WriteToServer(table);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public static object GetObject(SqlCommand cmd)
        {
            //string ConnStr = "Server=DESKTOP-9PLIM4G\\SQLEXPRESS;Database=AccountsDb;Trusted_Connection=True;MultipleActiveResultSets=true";
            object res = 0;

            using (SqlConnection con = new SqlConnection(ConnStr))
            {
                try
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {


                }
                finally
                {
                    con.Close();
                }
            }

            return res;
        }

    }
}

