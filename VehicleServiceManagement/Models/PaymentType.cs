﻿using VehicleServiceManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AccounTechSoftware.Models
{
    public class PaymentType
    {
        public int Id { get; set; }
        [Required]
        public string PaymentName { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public int DefaultAccount { get; set; }
        [Required]
        public string Note { get; set; }
        [Required]
        public List<PaymentType> paymentTypelist { get; set; }
        public int create(PaymentType obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddPaymentType");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("PaymentName", obj.PaymentName);
            cmd.Parameters.AddWithValue("Type", obj.Type);
            cmd.Parameters.AddWithValue("DefaultAccount", obj.DefaultAccount);
            cmd.Parameters.AddWithValue("Note", obj.Note);
           

            i = DbConnection.Create(cmd);
            return i;
        }
        /**Used***/
        public List<PaymentType> GetAllPaymentType()
        {

            List<PaymentType> PaymentType = new List<PaymentType>();

            SqlCommand com = new SqlCommand("ReadPaymentType");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                PaymentType.Add(

                    new PaymentType
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        PaymentName = Convert.ToString(dr["PaymentName"]),
                        Type = Convert.ToInt32(dr["Type"]),
                        DefaultAccount = Convert.ToInt32(dr["DefaultAccount"]),
                        Note = Convert.ToString(dr["Note"]),
                    }
                    );
            }

            return PaymentType;

        }

        public PaymentType details(int id)
        {
            PaymentType obj = new PaymentType();
            SqlCommand com = new SqlCommand("ViewPaymentTypeDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.PaymentName = dr["PaymentName"].ToString();
            obj.Type = Convert.ToInt32(dr["Type"]);
            obj.DefaultAccount = Convert.ToInt32(dr["DefaultAccount"]);
            obj.Note = dr["Note"].ToString();
           
            return (obj);


        }
        public int update(PaymentType obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdatePaymentType");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("PaymentName", obj.PaymentName);
            cmd.Parameters.AddWithValue("Type", obj.Type);
            cmd.Parameters.AddWithValue("DefaultAccount", obj.DefaultAccount);
            cmd.Parameters.AddWithValue("Note", obj.Note);
           

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeletePaymentType");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }


    }
}
