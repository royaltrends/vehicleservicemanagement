﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ContractInformation
    {
        public int LedgerId { get; set; }
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  ContractNumber")]
        public long ContractNumber { get; set; }
        [Required(ErrorMessage = "Please Enter  IssueDate")]
        [DataType(DataType.Date)]
        [Display(Name = "IssueDate")]
        public DateTime IssueDate { get; set; }
        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter  ClientReferenceNo")]
        public string ClientReferenceNo { get; set; }
        [Required(ErrorMessage = "Please Enter  Name")]
        public int DealerId { get; set; }
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter  BranchName")]
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        //public string Document { get; set; }
        [Required(ErrorMessage = "Please Enter  File")]
        public string FilePath { get; set; }
        public IFormFile File { get; set; }
        [Required(ErrorMessage = "Please Enter  CustomerName")]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Please Enter  Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter  Mobile")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Please Enter  City")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please Enter  Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroup")]
        public int ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int ProductSubGroupId { get; set; }
        public string SubGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  SoldDate")]
        [DataType(DataType.Date)]
        [Display(Name = "SoldDate")]
        public DateTime SoldDate { get; set; }
        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter  Make")]
        public int MakeId { get; set; }
        public string Make { get; set; }
        [Required(ErrorMessage = "Please Enter  ModelName")]
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]    
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please Enter  ManufacturerAssistanceDuration")]
        public int ManufacturerAssistanceDuration { get; set; }
        [Required(ErrorMessage = "Please Enter  ManufacturerAssistanceMileageCutoff")]
        public int ManufacturerAssistanceMileageCutoff { get; set; }
        [Required(ErrorMessage = "Please Enter  ModelYear")]
        public int ModelYear { get; set; }
        [Required(ErrorMessage = "Please Enter  ExtendedAssistanceDuration")]
        public int ExtendedAssistanceDuration { get; set; }
      
        public int? ExtendedAssistanceMileageCutoff { get; set; }
        [Required(ErrorMessage = "Please Enter  Remarks")]
        public string Remarks { get; set; }
        [Required(ErrorMessage = "Please Enter  ExtendedAssistanceStartDate")]
        [DataType(DataType.Date)]
        [Display(Name = "ExtendedAssistanceStartDate")]
        public DateTime ExtendedAssistanceStartDate { get; set; }
        [Required(ErrorMessage = "Please Enter  ExtendedAssistanceExpiryDate")]
        [DataType(DataType.Date)]
        [Display(Name = "ExtendedAssistanceExpiryDate")]
        public DateTime ExtendedAssistanceExpiryDate { get; set; }
        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter  ManufacturerAssistanceExpiryDate")]
        [DataType(DataType.Date)]
        [Display(Name = "ManufacturerAssistanceExpiryDate")]
        public DateTime ManufacturerAssistanceExpiryDate { get; set; }
        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter  ChassisNo")]

        public string ChassisNo { get; set; }
        [Required(ErrorMessage = "Please Enter  LimitPerClaim")]
        public int LimitPerClaimId { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductPrice")]
        public float ProductPrice { get; set; }
        [Required(ErrorMessage = "Please Enter  CurrentMileage")]
        public int CurrentMileageId { get; set; }
        [Required(ErrorMessage = "Please Enter  Range")]
        public int CCRangeId { get; set; }
        public string Range { get; set; }
        [Required(ErrorMessage = "Please Enter  CubicCapacity")]
        public int CubicCapacity { get; set; }
        [Required(ErrorMessage = "Please Enter  RegistrationNo")]
        public string RegistrationNo { get; set; }
        [Required(ErrorMessage = "Please Enter  RegistrationDate")]
        [DataType(DataType.Date)]
        public DateTime RegistrationDate { get; set; }
        //[DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter  PlateType")]
        public string PlateType { get; set; }
        [Required(ErrorMessage = "Please Enter  SoldBy")]
        public string SoldBy { get; set; }
        [Required(ErrorMessage = "Please Enter  MinPremium")]
        public float MinPremium { get; set; }
        [Required(ErrorMessage = "Please Enter  Premium")]
        public float Premium { get; set; }
        [Required(ErrorMessage = "Please Enter  Status")]
        public int Status { get; set; }

        public ContractInformation details(int id)
        {
            ContractInformation obj = new ContractInformation();
            SqlCommand com = new SqlCommand("ViewContractInformation");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.ContractNumber = Convert.ToInt32(dr["ContractNumber"]);
            obj.BranchId = Convert.ToInt32(dr["BranchId"]);
            obj.BranchName = Convert.ToString(dr["BranchName"]);
            obj.RegistrationNo = Convert.ToString(dr["RegistrationNo"]);
            obj.Address = Convert.ToString(dr["Address"]);
            obj.ChassisNo = Convert.ToString(dr["ChassisNo"]);
            obj.DealerId = Convert.ToInt32(dr["DealerId"]);
            obj.Name = Convert.ToString(dr["Name"]);
            obj.CustomerName = Convert.ToString(dr["CustomerName"]);
            obj.LedgerId = Convert.ToInt32(dr["CustomerLedgerId"]);
            return (obj);

        }

        public int getcount()
        {
            int count = 0;
            SqlCommand com = new SqlCommand("GetContractCount");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if(dt.Rows.Count>0)
            {
                count = Convert.ToInt32(dt.Rows[0]["valuecount"]);
            }
            return count;
        }
     
    }

}
