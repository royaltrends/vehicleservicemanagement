﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ClsContractPrint
    {
        public string ServiceType { get; set; }
        public string ServiceReference { get; set; }
        public string OwnerName { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }

        public string Make { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string Cahsis { get; set; }
        public string VehicleNumber { get; set; }
        public string EngineCC { get; set; }
        public string Reading { get; set; }

        public string ContractIssueDate { get; set; }
        public string ContractPurchaseDate { get; set; }
        public string ValidFrom { get; set; }      
        public string ValidFromKM { get; set; }
        public string ValidTo { get; set; }       
        public string LimitMonth { get; set; }
        public string LimitKm { get; set; }
        public string ClaimAmount { get; set; }

        public ClsContractPrint GetPrint(int id)
        {

            ClsContractPrint obj = new ClsContractPrint();

            SqlCommand com = new SqlCommand("GetntractPrint");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            if(dt.Rows.Count>0)
            {
                obj.ServiceReference =Convert.ToString(dt.Rows[0]["ClientReferenceNo"]);
                obj.OwnerName =Convert.ToString(dt.Rows[0]["CustomerName"]);
                obj.MobileNumber =Convert.ToString(dt.Rows[0]["Mobile"]);
                obj.Email =Convert.ToString(dt.Rows[0]["Email"]);
                obj.City =Convert.ToString(dt.Rows[0]["City"]);
                obj.Address =Convert.ToString(dt.Rows[0]["Address"]);

                obj.Make =Convert.ToString(dt.Rows[0]["Make"]);
                obj.Model =Convert.ToString(dt.Rows[0]["ModelName"]);
                obj.Year =Convert.ToString(dt.Rows[0]["ModelYear"]);
                obj.Cahsis =Convert.ToString(dt.Rows[0]["ChassisNo"]);
                obj.VehicleNumber =Convert.ToString(dt.Rows[0]["RegistrationNo"]);
                obj.EngineCC =Convert.ToString(dt.Rows[0]["CubicCapacity"]);
                obj.ContractIssueDate =Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd-MMM-yyyy");
                obj.ContractPurchaseDate = Convert.ToDateTime(dt.Rows[0]["SoldDate"]).ToString("dd-MMM-yyyy");
                obj.ValidFrom =Convert.ToString(dt.Rows[0]["ValueFrom"]);
                obj.ValidFromKM =Convert.ToString(dt.Rows[0]["ValidFromKM"]);
                obj.ValidTo =Convert.ToString(dt.Rows[0]["ValidTo"]);
                obj.LimitMonth =Convert.ToString(dt.Rows[0]["LimitMonth"]);
                obj.LimitKm =Convert.ToString(dt.Rows[0]["LimitKm"]);
                obj.ClaimAmount = Convert.ToString(dt.Rows[0]["Premium"]);                
            }
            
            return obj;

        }

    }
}
