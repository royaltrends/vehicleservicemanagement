﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ManufactureCutOffInformation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  ManufacturerCutoff")]
        public float ManufacturerCutoff { get; set; }
       
        public string Make { get; set; }
        [Required(ErrorMessage = "Please Enter  Make")]
        public int MakeId { get; set; }
        public string ProductGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  Duration")]
        public string Duration { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroup")]
        public int ProductGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public int create(ManufactureCutOffInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddManufactureCutoff");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@ManufacturerCutoff", obj.ManufacturerCutoff);
            cmd.Parameters.AddWithValue("@MakeId", obj.MakeId);
            cmd.Parameters.AddWithValue("@Duration", obj.Duration);
            cmd.Parameters.AddWithValue("@ProductGroupId", obj.ProductGroupId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<ManufactureCutOffInformation> GetAllManufacturCutoff()
        {

            List<ManufactureCutOffInformation> Infromation = new List<ManufactureCutOffInformation>();

            SqlCommand com = new SqlCommand("ReadManufactureCutOffInformation");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new ManufactureCutOffInformation
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        ManufacturerCutoff = Convert.ToSingle(dr["ManufacturerCutoff"]),
                        //MakeId = Convert.ToInt32(dr["MakeId"]),
                        Make = Convert.ToString(dr["Make"]),
                        ProductGroup = Convert.ToString(dr["ProductGroup"]),
                        Duration = Convert.ToString(dr["Duration"]),
                        ProductGroupId = Convert.ToInt32(dr["ProductGroupId"]),
                        Remark = Convert.ToString(dr["Remark"]),
                         
                    }
                    );
            }

            return Infromation;

        }

        public List<ManufactureCutOffInformation> GetbymakeandPrdctgroup(int makeid,int prsubgroup)
        {

            List<ManufactureCutOffInformation> Infromation = new List<ManufactureCutOffInformation>();

            SqlCommand cmd = new SqlCommand("GetManCuttoffByMakeProduct");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MakeId", makeid);
            cmd.Parameters.AddWithValue("@ProductGroupId", prsubgroup);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(cmd);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new ManufactureCutOffInformation
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        ManufacturerCutoff = Convert.ToSingle(dr["ManufacturerCutoff"]),
                        Duration=Convert.ToString(dr["Duration"])
                    }
                    );
            }

            return Infromation;

        }
        public ManufactureCutOffInformation details(int id)
        {
            ManufactureCutOffInformation obj = new ManufactureCutOffInformation();
            SqlCommand com = new SqlCommand("ViewManufactureCutOffInformation");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.ManufacturerCutoff = Convert.ToSingle(dr["ManufacturerCutoff"]);
            obj.MakeId = Convert.ToInt32(dr["MakeId"]);
            obj.Duration = Convert.ToString(dr["Duration"]);
            obj.ProductGroupId = Convert.ToInt32(dr["ProductGroupId"]);
            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);


        }
        public int update(ManufactureCutOffInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateManufactureCutOffInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@ManufacturerCutoff", obj.ManufacturerCutoff);
            cmd.Parameters.AddWithValue("@MakeId", obj.MakeId);
            cmd.Parameters.AddWithValue("@Duration", obj.Duration);
            cmd.Parameters.AddWithValue("@ProductGroupId", obj.ProductGroupId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);


            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteManufactureCutOffInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class ManufactureCutoffInformationDisplay
    {
        public List<ManufactureCutOffInformation> cutoffList { get; set; }
        public UserPermission userP { get; set; }
    }
}
