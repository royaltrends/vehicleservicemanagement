﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class OtherServiceDetails
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  OtherService")]
        public string OtherService { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }


        public int create(OtherServiceDetails obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddOtherServiceDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@OtherService", obj.OtherService);
           

            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<OtherServiceDetails> GetAllOtherService()
        {

            List<OtherServiceDetails> service = new List<OtherServiceDetails>();

            SqlCommand com = new SqlCommand("ReadOtherService");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                service.Add(

                    new OtherServiceDetails
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        OtherService = Convert.ToString(dr["OtherService"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return service;

        }
        public OtherServiceDetails details(int id)
        {
            OtherServiceDetails obj = new OtherServiceDetails();
            SqlCommand com = new SqlCommand("ViewOtherServiceDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.OtherService = Convert.ToString(dr["OtherService"]);
           

            obj.Remark = Convert.ToString(dr["Remark"]);


            return (obj);


        }
        public int update(OtherServiceDetails obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateOtherServiceDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@OtherService", obj.OtherService);


            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteOtherServiceDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class OtherServiceDetailsDisplay
    {
        public List<OtherServiceDetails> otherServiceList { get; set; }
        public UserPermission userP { get; set; }
    }
}
