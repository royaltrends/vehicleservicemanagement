﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ProductGroups
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroup")]
        public string ProductGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }

        public int create(ProductGroups obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddProductGroups");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@ProductGroup", obj.ProductGroup);


            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<ProductGroups> GetAllProductGroups()
        {

            List<ProductGroups> Infromation = new List<ProductGroups>();

            SqlCommand com = new SqlCommand("ReadProductGroups");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new ProductGroups
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        ProductGroup = Convert.ToString(dr["ProductGroup"]),

                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;

        }
        public ProductGroups details(int id)
        {
            ProductGroups obj = new ProductGroups();
            SqlCommand com = new SqlCommand("ViewProductGroups");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);

            obj.ProductGroup = Convert.ToString(dr["ProductGroup"]);

            obj.Remark = Convert.ToString(dr["Remark"]);


            return (obj);


        }
        public int update(ProductGroups obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateDProductGroups");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
           
            cmd.Parameters.AddWithValue("@ProductGroup", obj.ProductGroup);


            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteProductGroups");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }


    }

    public class ProductGroupsDisplay
    {
        public List<ProductGroups> productGroupList { get; set; }
        public UserPermission userP { get; set; }
    }
}
