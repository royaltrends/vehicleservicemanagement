﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class OtherServicePremiumDetails
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Enter  OtherService")]
        public int OtherServiceId { get; set; }
        public string OtherService { get; set; }

        [Required(ErrorMessage = "Please Enter  Range")]
        public int CCRangeId { get; set; }
        public string Range { get; set; }

        public int WorkShopId { get; set; }
        public string WorkShop { get; set; }

        public int WorkShopBranchId { get; set; }
        public string WorkShopBranch { get; set; }

        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }
        public string SubGroup { get; set; }

        [Required(ErrorMessage = "Please Enter  ExtendedDuration")]
        public int ExtendedDurationId { get; set; }

        [Required(ErrorMessage = "Please Enter  ManufacturerDuration")]
        public int MaDurationId { get; set; }
        public string Duration { get; set; }

        [Required(ErrorMessage = "Please Enter  ManufacturerCutoff")]
        public int MaMilegeCuttoffId { get; set; }

        public int ManufacturerCutoff { get; set; }
        [Required(ErrorMessage = "Please Enter  MilageCutoff")]
        public int ExtendedMilageCuttoffId { get; set; }

        public int MilageCutoff { get; set; }

        [Required(ErrorMessage = "Please Enter  FromDate")]
        [DataType(DataType.Date)]
        public DateTime FromDate { get; set; }

        [Required(ErrorMessage = "Please Enter  ToDate")]
        [DataType(DataType.Date)]
        public DateTime ToDate { get; set; }

        [Required(ErrorMessage = "Please Enter  MinPremiumAmount")]
        public float MinPremiumAmount { get; set; }

        [Required(ErrorMessage = "Please Enter  PremiumAmount")]
        public float PremiumAmount { get; set; }

        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }

        public int create(OtherServicePremiumDetails obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddOtherServicePremiumDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@OtherServiceId", obj.OtherServiceId);
            cmd.Parameters.AddWithValue("@CCRangeId", obj.CCRangeId);
            cmd.Parameters.AddWithValue("@WorkShopId", obj.WorkShopId);
           
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@MaDurationId", obj.MaDurationId);
         
            cmd.Parameters.AddWithValue("@ExtendedDurationId", obj.ExtendedDurationId);
            cmd.Parameters.AddWithValue("@MaMilegeCuttoffId", obj.MaMilegeCuttoffId);
            cmd.Parameters.AddWithValue("@ExtendedMilageCuttoffId", obj.ExtendedMilageCuttoffId);
            cmd.Parameters.AddWithValue("@FromDate", obj.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", obj.ToDate);
            cmd.Parameters.AddWithValue("@MinPremiumAmount", obj.MinPremiumAmount);
            cmd.Parameters.AddWithValue("@PremiumAmount", obj.PremiumAmount);
            cmd.Parameters.AddWithValue("@WorkShopBranch", obj.PremiumAmount);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);


            i = DbConnection.Create(cmd);
            return i;
        }
        public List<OtherServicePremiumDetails> GetAllOtherServicePremiumDetails()
        {

            List<OtherServicePremiumDetails> Infromation = new List<OtherServicePremiumDetails>();

            SqlCommand com = new SqlCommand("ReadOtherServicePremiumDetails");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new OtherServicePremiumDetails
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        OtherServiceId = Convert.ToInt32(dr["OtherServiceId"]),
                        OtherService = Convert.ToString(dr["OtherService"]),
                        CCRangeId = Convert.ToInt32(dr["CCRangeId"]),
                        WorkShopId = Convert.ToInt32(dr["WorkShopId"]),
                        CategoryId = Convert.ToInt32(dr["CategoryId"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        MaDurationId = Convert.ToInt32(dr["MaDurationId"]),
                        ExtendedDurationId = Convert.ToInt32(dr["ExtendedDurationId"]),
                        MaMilegeCuttoffId = Convert.ToInt32(dr["MaMilegeCuttoffId"]),
                        ExtendedMilageCuttoffId = Convert.ToInt32(dr["ExtendedMilageCuttoffId"]),
                        WorkShopBranchId = Convert.ToInt32(dr["WorkShopBranch"]),
                        FromDate = Convert.ToDateTime(dr["FromDate"]),
                        ToDate = Convert.ToDateTime(dr["ToDate"]),
                        MinPremiumAmount = Convert.ToSingle(dr["MinPremiumAmount"]),
                        PremiumAmount = Convert.ToSingle(dr["PremiumAmount"]),
                        Remark = Convert.ToString(dr["Remark"]),
                        Range = Convert.ToString(dr["Range"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        Duration = Convert.ToString(dr["Duration"]),
                        ManufacturerCutoff = Convert.ToInt32(dr["ManufacturerCutoff"]),
                        MilageCutoff = Convert.ToInt32(dr["MilageCutoff"])

                    }
                    );
            }

            return Infromation;

        }
        public OtherServicePremiumDetails details(int id)
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            SqlCommand com = new SqlCommand("ViewOtherServicePremiumDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.OtherServiceId = Convert.ToInt32(dr["OtherServiceId"]);
            obj.CCRangeId = Convert.ToInt32(dr["CCRangeId"]);
            obj.WorkShopId = Convert.ToInt32(dr["WorkShopId"]);
            obj.CategoryId = Convert.ToInt32(dr["CategoryId"]);
            obj.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
            obj.MaDurationId = Convert.ToInt32(dr["MaDurationId"]);
            obj.ExtendedDurationId = Convert.ToInt32(dr["ExtendedDurationId"]);
            obj.MaMilegeCuttoffId = Convert.ToInt32(dr["MaMilegeCuttoffId"]);
            obj.ExtendedMilageCuttoffId = Convert.ToInt32(dr["ExtendedMilageCuttoffId"]);
            obj.WorkShopBranchId = Convert.ToInt32(dr["WorkShopBranch"]);
            obj.FromDate = Convert.ToDateTime(dr["FromDate"]);
            obj.ToDate = Convert.ToDateTime(dr["ToDate"]);
            obj.MinPremiumAmount = Convert.ToSingle(dr["MinPremiumAmount"]);
            obj.PremiumAmount = Convert.ToSingle(dr["PremiumAmount"]);
            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);


        }
        public int update(OtherServicePremiumDetails obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateOtherServicePremiumDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@OtherServiceId", obj.OtherServiceId);
            cmd.Parameters.AddWithValue("@CCRangeId", obj.CCRangeId);
            cmd.Parameters.AddWithValue("@WorkShopId", obj.WorkShopId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@MaDurationId", obj.MaDurationId);
            cmd.Parameters.AddWithValue("@ExtendedDurationId", obj.ExtendedDurationId);
            cmd.Parameters.AddWithValue("@MaMilegeCuttoffId", obj.MaMilegeCuttoffId);
            cmd.Parameters.AddWithValue("@ExtendedMilageCuttoffId", obj.ExtendedMilageCuttoffId);
            cmd.Parameters.AddWithValue("@FromDate", obj.FromDate);
            cmd.Parameters.AddWithValue("@ToDate", obj.ToDate);
            cmd.Parameters.AddWithValue("@MinPremiumAmount", obj.MinPremiumAmount);
            cmd.Parameters.AddWithValue("@PremiumAmount", obj.PremiumAmount);
            cmd.Parameters.AddWithValue("@WorkShopBranch", obj.PremiumAmount);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteOtherServicePremiumDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

        public OtherServicePremiumDetails GetMilageCuttoff()
        {
            OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
            SqlCommand com = new SqlCommand("GetOtherServicePremiumByMilageCuttoff");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@CCRangeId", CCRangeId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);
            com.Parameters.AddWithValue("@MaDurationId", MaDurationId);
            com.Parameters.AddWithValue("@MaMilegeCuttoffId", MaMilegeCuttoffId);
            com.Parameters.AddWithValue("@ExtendedDurationId", ExtendedDurationId);
            com.Parameters.AddWithValue("@ExtendedMilageCuttoffId", ExtendedMilageCuttoffId);
    
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if(dt.Rows.Count>0)
            {
                obj.PremiumAmount = float.Parse(dt.Rows[0]["PremiumAmount"].ToString());
                obj.MinPremiumAmount = float.Parse(dt.Rows[0]["MinPremiumAmount"].ToString());
            }
           
            return obj;
        }

        public List< OtherServicePremiumDetails> GetDuration()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyPremiumDuration");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@CCRangeId", CCRangeId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);
            com.Parameters.AddWithValue("@MaDurationId", MaDurationId);
            com.Parameters.AddWithValue("@MaMilegeCuttoffId", MaMilegeCuttoffId);
            com.Parameters.AddWithValue("@ExtendedDurationId", ExtendedDurationId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i=0;i<dt.Rows.Count;i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.ExtendedMilageCuttoffId = Convert.ToInt32(dt.Rows[0]["ExtendedMilageCuttoffId"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetManuCuttOff()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyManuCuttOff");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@CCRangeId", CCRangeId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);
            com.Parameters.AddWithValue("@MaDurationId", MaDurationId);
            com.Parameters.AddWithValue("@MaMilegeCuttoffId", MaMilegeCuttoffId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.ExtendedDurationId = Convert.ToInt32(dt.Rows[0]["ExtendedDurationId"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetManDuration()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyManDuration");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@CCRangeId", CCRangeId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);
            com.Parameters.AddWithValue("@MaDurationId", MaDurationId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.MaMilegeCuttoffId = Convert.ToInt32(dt.Rows[0]["MaMilegeCuttoffId"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetCCRange()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyCCRange");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);         
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);
            com.Parameters.AddWithValue("@CCRangeId", CCRangeId);
           
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.MaDurationId = Convert.ToInt32(dt.Rows[0]["MaDurationId"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetProductSubGroup()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyProductSubGroup");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);
            com.Parameters.AddWithValue("@SubGroupId", SubGroupId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.CCRangeId = Convert.ToInt32(dt.Rows[0]["CCRangeId"].ToString());
                obj.Range = Convert.ToString(dt.Rows[0]["Range"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetCategory()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyCategory");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);
            com.Parameters.AddWithValue("@CategoryId", CategoryId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.SubGroupId = Convert.ToInt32(dt.Rows[0]["SubGroupId"].ToString());
                obj.SubGroup = Convert.ToString(dt.Rows[0]["SubGroup"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetBranch()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyBranch");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);
            com.Parameters.AddWithValue("@WorkShopBranchId", WorkShopBranchId);         

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.CategoryId = Convert.ToInt32(dt.Rows[0]["CategoryId"].ToString());
                obj.CategoryName = Convert.ToString(dt.Rows[0]["CategoryName"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetWorkshop()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyWorkshop");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);
            com.Parameters.AddWithValue("@WorkShopId", WorkShopId);

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.WorkShopBranchId = Convert.ToInt32(dt.Rows[0]["WorkShopId"].ToString());
                obj.WorkShopBranch = Convert.ToString(dt.Rows[0]["BranchName"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public List<OtherServicePremiumDetails> GetOtherService()
        {
            List<OtherServicePremiumDetails> objlist = new List<OtherServicePremiumDetails>();
            SqlCommand com = new SqlCommand("GetOtherServicebyOtherService");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@OtherServiceId", OtherServiceId);         

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                OtherServicePremiumDetails obj = new OtherServicePremiumDetails();
                obj.WorkShopId = Convert.ToInt32(dt.Rows[0]["WorkShopId"].ToString());
                obj.WorkShop = Convert.ToString(dt.Rows[0]["Name"].ToString());
                objlist.Add(obj);
            }

            return objlist;
        }

        public int getcount()
        {
            int count = 0;
            SqlCommand com = new SqlCommand("GetOtherServiceCount");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                count = Convert.ToInt32(dt.Rows[0]["valuecount"]);
            }
            return count;
        }

    }

    public class OtherServicePremiumDetailsDisplay
    {
        public List<OtherServicePremiumDetails> otherServicePremiumList { get; set; }
        public UserPermission userP { get; set; }
    }
}
