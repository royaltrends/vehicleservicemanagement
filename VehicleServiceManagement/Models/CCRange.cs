﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class CCRange
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Range")]
        public string Range { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Please Enter  DealerName")]
        public int DealerId { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }

        public string CategoryName { get; set; }
        public string Name { get; set; }


        public int create(CCRange obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddCCRange");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Range", obj.Range);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);

            return i;

        }

        public List<CCRange> GetAllCCRange()
        {

            List<CCRange> ccrange = new List<CCRange>();

            SqlCommand com = new SqlCommand("ReadCCRange");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                ccrange.Add(

                    new CCRange
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Range = Convert.ToString(dr["Range"]),
                        CategoryId = Convert.ToInt32(dr["CategoryId"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        Remark = Convert.ToString(dr["Remark"]),
                        Name = Convert.ToString(dr["Name"]),
                        CategoryName = Convert.ToString(dr["CategoryName"])
                        


                    }


                    );
            }

            return ccrange;

        }


        public CCRange details(int id)
        {
            CCRange con = new CCRange();
            SqlCommand com = new SqlCommand("CCRangeDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.Range = Convert.ToString(dr["Range"]);
                con.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                con.DealerId = Convert.ToInt32(dr["DealerId"]);
                con.Remark = Convert.ToString(dr["Remark"]);


            }


            return con;

        }
        public int update(CCRange obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("CCRangeUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Range", obj.Range);
            cmd.Parameters.AddWithValue("CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("CCRangeDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }

    }

    public class CCRangeDisplay
    {
        public List<CCRange> ccrangeList { get; set; }
        public UserPermission userP { get; set; }
    }
}
