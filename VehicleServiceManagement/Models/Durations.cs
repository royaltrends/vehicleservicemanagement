﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Durations
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Duration")]
        public string Duration { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  Category")]
        public int CategoryId { get; set; }
        
        public string Remark { get; set; }
        public string CategoryName { get; set; }
          public string SubGroup { get; set; }

        public Durations objval { get; set; }

        public int create(Durations obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddDurations");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@Duration", obj.Duration);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
         

        public List<Durations> GetAllDurations()
        {

            List<Durations> Infromation = new List<Durations>();

            SqlCommand com = new SqlCommand("ReadDurations");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new Durations
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        Duration = Convert.ToString(dr["Duration"]),
                        CategoryId = Convert.ToInt32(dr["CategoryId"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        Remark = Convert.ToString(dr["Remark"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        SubGroup = Convert.ToString(dr["SubGroup"])
                    }
                    );
            }

            return Infromation;
        }

        public List<Durations> Getbylist(int catid,int subid)
        {

            List<Durations> Infromation = new List<Durations>();

            SqlCommand com = new SqlCommand("GetDurationbyList");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CategoryId", catid);
            com.Parameters.AddWithValue("@SubGroupId", subid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new Durations
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Duration = Convert.ToString(dr["Duration"]),                    
                    }
                    );
            }

            return Infromation;
        }

        public Durations details(int id)
        {
            Durations con = new Durations();
            SqlCommand com = new SqlCommand("DurationInformationDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.Id = Convert.ToInt32(dr["Id"]);
                con.Duration = Convert.ToString(dr["Duration"]);
                con.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
                con.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                con.Remark = Convert.ToString(dr["Remark"]);

            }


            return con;

        }
        public int update(Durations obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("DurationInformationUpdate");

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Duration", obj.Duration);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;



        }



        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DurationInformationDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }

    }

    public class DurationsDisplay
    {
        public List<Durations> durationsList { get; set; }
        public UserPermission userP { get; set; }
    }
}