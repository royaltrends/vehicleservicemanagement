﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace VehicleServiceManagement.Models
{
    public class SetPermission
    {
        public Int64 UserId { get; set; }
        public List<UserPermission> permissionList { get; set; }

        public SetPermission()
        {
            permissionList = new List<UserPermission>();
        }

        public void UpdatePermissions(SetPermission updP)
        {
            var table = new DataTable();
            table.Columns.Add("Id", typeof(Int64));
            table.Columns.Add("UserId", typeof(Int64));
            table.Columns.Add("Form", typeof(Int64));
            table.Columns.Add("InsertPermission", typeof(Boolean));
            table.Columns.Add("UpdatePermission", typeof(Boolean));
            table.Columns.Add("DeletePermission", typeof(Boolean));
            table.Columns.Add("DisplayPermission", typeof(Boolean));
            table.Columns.Add("PrintPermission", typeof(Boolean));

            List<UserPermission> pList = updP.permissionList.Where(a => a.Id == 0).ToList();
            for (var i = 0; i < pList.Count; i++)
            {
                table.Rows.Add(pList[i].Id, updP.UserId, pList[i].Form, pList[i].InsertPermission, pList[i].UpdatePermission,
                    pList[i].DeletePermission, pList[i].DisplayPermission, pList[i].PrintPermission);
            }
            DbConnection.BulkInsert(table, "UserPermission");

            List<UserPermission> updList = updP.permissionList.Where(a => a.Id != 0).ToList();
            foreach (var obj in updList)
            {
                int i = 0;
                SqlCommand cmd;
                cmd = new SqlCommand("UpdatePermissions");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("UserId", updP.UserId);
                cmd.Parameters.AddWithValue("Form", obj.Form);
                cmd.Parameters.AddWithValue("InsertPermission", obj.InsertPermission);
                cmd.Parameters.AddWithValue("UpdatePermission", obj.UpdatePermission);
                cmd.Parameters.AddWithValue("DeletePermission", obj.DeletePermission);
                cmd.Parameters.AddWithValue("DisplayPermission", obj.DisplayPermission);
                cmd.Parameters.AddWithValue("PrintPermission", obj.PrintPermission);
                i = DbConnection.Create(cmd);
            }
           
        }
    }
}
