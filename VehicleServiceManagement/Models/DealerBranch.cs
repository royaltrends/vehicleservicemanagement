﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class DealerBranch
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  BranchName")]
        public string BranchName { get; set; }
        [Required(ErrorMessage = "Please Enter  Dealer")]
        public int DealerId { get; set; }

        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public List<DealerBranch> dealerBranches { get; set; }

        public int create(DealerBranch obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddDealerBranch");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@BranchName", obj.BranchName);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
           
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        /**Used***/
        public List<DealerBranch> GetAllDealerBranch()
        {

            List<DealerBranch> Infromation = new List<DealerBranch>();

            SqlCommand com = new SqlCommand("ReadDealerBranch");
            com.CommandType = CommandType.StoredProcedure;
            
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerBranch
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        BranchName = Convert.ToString(dr["BranchName"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        Name = Convert.ToString(dr["Name"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;
        }

        public List<DealerBranch> GetAllBranchByDealer(int dealerid)
        {

            List<DealerBranch> Infromation = new List<DealerBranch>();

            SqlCommand com = new SqlCommand("GetBranchByDealer");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DealerId", dealerid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerBranch
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        BranchName = Convert.ToString(dr["BranchName"]),
                    }
                    );
            }

            return Infromation;
        }


        public DealerBranch details(int id)
        {
            DealerBranch obj = new DealerBranch();
            SqlCommand com = new SqlCommand("ViewDealerBranch");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.BranchName = Convert.ToString(dr["BranchName"]);
            obj.DealerId = Convert.ToInt32(dr["DealerId"]);
       
            obj.Remark = Convert.ToString(dr["Remark"]);


            return (obj);


        }
        public int update(DealerBranch obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateDealerBranch");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@BranchName", obj.BranchName);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);

            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteDealerBranch");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }
        public List<DealerBranch> GetAllDealerbanches()
        {

            List<DealerBranch> Infromation = new List<DealerBranch>();

            SqlCommand com = new SqlCommand("dealerbranchontype");
            com.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerBranch
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        BranchName = Convert.ToString(dr["BranchName"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        Name = Convert.ToString(dr["Name"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;
        }


    }

    public class DealerBranchDisplay
    {
        public List<DealerBranch> branchList { get; set; }
        public UserPermission userP { get; set; }
    }
}
