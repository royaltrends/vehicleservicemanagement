﻿using VehicleServiceManagement.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccounTechSoftware.Models
{
    public class CompanyInformation
    {
      
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Company Name")]
        public string EnglishName { get; set; }
        [Required(ErrorMessage = "Please Enter Arabic Company Name")]
        public string ArabicName { get; set; }
      
        public string Address { get; set; }

        [Required(ErrorMessage = "Please Enter Country Name")]
        public int Country { get; set; }
        
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please Enter Emirates Name")]
        public string Emirates { get; set; }
      
        public int? PostCode { get; set; }
      
        public string PhoneNo { get; set; }
       
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }

        [Required(ErrorMessage = "Please Enter License Number Name")]
        public string LicenseNo { get; set; }
   
        public string Authority { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Issue Date")]
        public DateTime LicenseIssueDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Expiry Date")]
        public DateTime LicenseExpiryDate { get; set; }

        public string logo { get; set; }
        public IFormFile photo { get; set; }

        [Required(ErrorMessage = "Please Select Tax Type")]
        public string TaxType { get; set; }       
        public string TRNNo { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Reg Date")]
        public DateTime RegDate { get; set; }


        [Required(ErrorMessage = "Please Select Company Type")]
        public string CompanyType { get; set; }
        [Required(ErrorMessage = "Please Select Currency")]
        public int Currency { get; set; }
        public Decimal? Capital { get; set; }    
        public string CurrencyName { get; set; }       
        public string FromMonth { get; set; }
        public string ToMonth { get; set; }

        public int? BankId { get; set; }
        public string IdBranch { get; set; }
        public string AccNo { get; set; }
        public string IBAN { get; set; }
        public string branchname { get; set; }

        
        public string Note { get; set; }
        
        public CompanyInformation details()
        {
            DataTable dt = new DataTable();
            CompanyInformation obj = new CompanyInformation();
            try
            {

                SqlCommand com = new SqlCommand("ViewCompanyInfoDetails");
                com.CommandType = CommandType.StoredProcedure;
                dt = DbConnection.Read(com);

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    obj.Id = Convert.ToInt32(dr["Id"]);
                    obj.EnglishName = dr["EnglishName"].ToString();
                    obj.ArabicName = dr["ArabicName"].ToString();
                    obj.Address = dr["Address"].ToString();
                    obj.Country = Convert.ToInt32(dr["Country"]);
                    obj.Emirates = Convert.ToString(dr["Emirates"]);
                    obj.PostCode = Convert.ToInt32(dr["PostCode"]);
                    obj.PhoneNo = dr["PhoneNo"].ToString();
                    obj.Fax = dr["Fax"].ToString();
                    obj.Mobile = dr["Mobile"].ToString();
                    obj.Website = dr["Website"].ToString();
                    obj.Email = dr["Email"].ToString();

                    obj.Currency = Convert.ToInt32(dr["Currency"]);
                    obj.CompanyType = dr["CompanyType"].ToString();
                    obj.Capital =Convert.ToDecimal( dr["Capital"]);
                    obj.FromMonth = Convert.ToString(dr["FromMonth"]);
                    obj.ToMonth = Convert.ToString(dr["ToMonth"]);
                    
                    obj.LicenseNo = dr["LicenseNo"].ToString();
                    obj.Authority = dr["Authority"].ToString();
                    obj.LicenseExpiryDate = Convert.ToDateTime(dr["LicenseExpiryDate"]);
                    obj.LicenseIssueDate = Convert.ToDateTime(dr["LicenseIssueDate"]);
                    obj.TaxType = dr["TaxType"].ToString();
                    obj.TRNNo = dr["TRNNo"].ToString();
                    obj.RegDate = Convert.ToDateTime(dr["RegDate"]);                   
                    obj.BankId = Convert.ToInt32(dr["BankId"]);
                    if (obj.BankId > 0)
                        obj.IdBranch = obj.BankId + "," + Convert.ToString(dr["BranchName"]);
                    obj.branchname = Convert.ToString(dr["BranchName"]);
                    obj.AccNo = Convert.ToString(dr["AccNo"]);
                    obj.IBAN = Convert.ToString(dr["IBAN"]);
                    obj.Note = dr["Note"].ToString();
                    obj.logo = dr["Logo"].ToString();
                }
            }
            catch (Exception ex)
            {

            }
            return (obj);
        }

        public int update(CompanyInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateCompanyInfo");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("EnglishName", obj.EnglishName);
            cmd.Parameters.AddWithValue("ArabicName", obj.ArabicName);
            cmd.Parameters.AddWithValue("Address", obj.Address );
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("Emirates", obj.Emirates);
            cmd.Parameters.AddWithValue("PostCode", obj.PostCode);
            cmd.Parameters.AddWithValue("PhoneNo", obj.PhoneNo);
            cmd.Parameters.AddWithValue("Fax", obj.Fax);
            cmd.Parameters.AddWithValue("Mobile", obj.Mobile);
            cmd.Parameters.AddWithValue("Website", obj.Website);
            cmd.Parameters.AddWithValue("Email", obj.Email);           
            

            cmd.Parameters.AddWithValue("CompanyType", obj.CompanyType);
            cmd.Parameters.AddWithValue("Currency", obj.Currency);
            cmd.Parameters.AddWithValue("Capital", obj.Capital);       
            cmd.Parameters.AddWithValue("FromMonth", obj.FromMonth);
            cmd.Parameters.AddWithValue("ToMonth", obj.ToMonth);

            cmd.Parameters.AddWithValue("LicenseNo", obj.LicenseNo);
            cmd.Parameters.AddWithValue("Authority", obj.Authority);
            cmd.Parameters.AddWithValue("LicenseIssueDate", obj.LicenseIssueDate.Year < 1880 ? Convert.ToDateTime( "01-01-1980") : obj.LicenseIssueDate);
            cmd.Parameters.AddWithValue("LicenseExpiryDate", obj.LicenseExpiryDate.Year < 1880 ? Convert.ToDateTime("01-01-1980") : obj.LicenseExpiryDate);

            cmd.Parameters.AddWithValue("TaxType", obj.TaxType);
            cmd.Parameters.AddWithValue("TRNNo", obj.TRNNo);
            cmd.Parameters.AddWithValue("RegDate", obj.RegDate.Year < 1880 ? Convert.ToDateTime("01-01-1980") : obj.RegDate);         
            cmd.Parameters.AddWithValue("Note", obj.Note);

            cmd.Parameters.AddWithValue("Logo", obj.logo); 
            
            cmd.Parameters.AddWithValue("AccNo", obj.AccNo);
            cmd.Parameters.AddWithValue("IBAN", obj.IBAN);
            cmd.Parameters.AddWithValue("BankId", obj.BankId);
            i = DbConnection.Update(cmd);
            return i;
        }
      

    }
}
