﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ModelDetail
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Model")]
        public string ModelName { get; set; }
        [Required(ErrorMessage = "Please Enter  Make")]
        public int MakeId { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
       
        public string Make { get; set; }


        public int create(ModelDetail obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddModelDetail");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ModelName", obj.ModelName);
            cmd.Parameters.AddWithValue("@MakeId", obj.MakeId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);

            return i;

        }

        public List<ModelDetail> GetAllModelDetail()
        {

            List<ModelDetail> model = new List<ModelDetail>();

            SqlCommand com = new SqlCommand("ReadModelDetail");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                model.Add(

                       new ModelDetail
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           ModelName = Convert.ToString(dr["ModelName"]),
                           MakeId = Convert.ToInt32(dr["MakeId"]),
                           Remark = Convert.ToString(dr["Remark"]),
                           Make = Convert.ToString(dr["Make"])

                       }


                       );
            }

            return model;

        }

        public List<ModelDetail> GetModelByMake(int makeid)
        {

            List<ModelDetail> model = new List<ModelDetail>();

            SqlCommand com = new SqlCommand("GetModelByMake");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@MakeId", makeid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                model.Add(

                       new ModelDetail
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           ModelName = Convert.ToString(dr["ModelName"]),                           
                       });
            }

            return model;

        }


        public ModelDetail details(int id)
        {
            ModelDetail con = new ModelDetail();
            SqlCommand com = new SqlCommand("ModelDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.ModelName = Convert.ToString(dr["ModelName"]);
                con.MakeId = Convert.ToInt32(dr["MakeId"]);
                con.Remark = Convert.ToString(dr["Remark"]);


            }


            return con;

        }
        public int update(ModelDetail obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("ModelDetailUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@ModelName", obj.ModelName);
            cmd.Parameters.AddWithValue("@MakeId", obj.MakeId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("ModelDetailDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }


    }

    public class ModelDetailDisplay
    {
        public List<ModelDetail> modelList { get; set; }
        public UserPermission userP { get; set; }
    }
}
