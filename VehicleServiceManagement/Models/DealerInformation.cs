﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class DealerInformation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter DealerType")]
        public string DealerType { get; set; }
        [Required(ErrorMessage = "Please Enter Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter TRNumber")]
        public string TRNumber { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter ContactNo")]
        public string ContactNo { get; set; }
        [Required(ErrorMessage = "Please Enter ContactPerson")]
        public string ContactPerson { get; set; }       
        public string Remark { get; set; }

        [Required(ErrorMessage = "Please Enter  File")]
        public string VatDocumentPath { get; set; }
        public IFormFile VatDocument { get; set; }

        [Required(ErrorMessage = "Please Enter  File")]
        public string TaxDocumentPath { get; set; }
        public IFormFile TaxDocument { get; set; }

        public int VendorLedgerId { get; set; }
        public int CustomerLedgerId { get; set; }

        public int create(DealerInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddDealerInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@DealerType", obj.DealerType);
            cmd.Parameters.AddWithValue("@Email", obj.Email);
            cmd.Parameters.AddWithValue("@Address", obj.Address);
            cmd.Parameters.AddWithValue("@ContactNo", obj.ContactNo);
            cmd.Parameters.AddWithValue("@ContactPerson", obj.ContactPerson);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);
            cmd.Parameters.AddWithValue("@VendorLedgerId", obj.VendorLedgerId);
            cmd.Parameters.AddWithValue("@CustomerLedgerId", obj.CustomerLedgerId);

            i = DbConnection.Create(cmd);
            return i;
        }

        public List<DealerInformation> GetAllDealesInformation()
        {

            List<DealerInformation> Infromation = new List<DealerInformation>();

            SqlCommand com = new SqlCommand("ReadDealerInformation");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerInformation
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        DealerType = Convert.ToString(dr["DealerType"]),
                        Email = Convert.ToString(dr["Email"]),
                        Address = Convert.ToString(dr["Address"]),
                        ContactNo = Convert.ToString(dr["ContactNo"]),
                        ContactPerson = Convert.ToString(dr["ContactPerson"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;

        }

        public List<DealerInformation> GetDealerVendor()
        {

            List<DealerInformation> Infromation = new List<DealerInformation>();

            SqlCommand com = new SqlCommand("GetDealerVendor");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                Infromation.Add(
                    new DealerInformation
                    {
                        VendorLedgerId = Convert.ToInt32(dr["VendorLedgerId"]),
                        Name = Convert.ToString(dr["Name"]),
                    }
                    );
            }

            return Infromation;

        }

        public List<DealerInformation> GetDealerCustomer()
        {

            List<DealerInformation> Infromation = new List<DealerInformation>();

            SqlCommand com = new SqlCommand("GetDealerCustomer");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerInformation
                    {
                        CustomerLedgerId = Convert.ToInt32(dr["CustomerLedgerId"]),
                        Name = Convert.ToString(dr["Name"]),
                    }
                    );
            }

            return Infromation;

        }

        public DealerInformation details(int id)
        {
            DealerInformation obj = new DealerInformation();
            SqlCommand com = new SqlCommand("ViewDealerInformation");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.Name = Convert.ToString(dr["Name"]);
            obj.Email = Convert.ToString(dr["Email"]);
            obj.DealerType = Convert.ToString(dr["DealerType"]);
            obj.Address = Convert.ToString(dr["Address"]);
            obj.ContactNo = Convert.ToString(dr["ContactNo"]);
            obj.ContactPerson = Convert.ToString(dr["ContactPerson"]);
            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);
        }

        public int update(DealerInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateDealerInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@DealerType", obj.DealerType);
            cmd.Parameters.AddWithValue("@Email", obj.Email);
            cmd.Parameters.AddWithValue("@Address", obj.Address);
            cmd.Parameters.AddWithValue("@ContactNo", obj.ContactNo);
            cmd.Parameters.AddWithValue("@ContactPerson", obj.ContactPerson);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }

        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteDealerInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

        public long GetLedgerId(int id)
        {
            long ledId = 0;
            SqlCommand cmd = new SqlCommand("GetLedgerIdbyCustomer");
            cmd.Parameters.AddWithValue("@CustomerId", id);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(cmd);
            if (dt.Rows.Count > 0)
            {
                ledId = Convert.ToInt64(dt.Rows[0]["LedgerId"]);
            }
            return ledId;
        }

        public List<DealerInformation> GetAllDealerdetails()
        {
            List<DealerInformation> Infromation = new List<DealerInformation>();
            SqlCommand com = new SqlCommand("ReadDealarBasedonbranch");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new DealerInformation
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        DealerType = Convert.ToString(dr["DealerType"]),
                        Email = Convert.ToString(dr["Email"]),
                        Address = Convert.ToString(dr["Address"]),
                        ContactNo = Convert.ToString(dr["ContactNo"]),
                        ContactPerson = Convert.ToString(dr["ContactPerson"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;


        }
               
    }

    public class DealerInformationDisplay
    {
        public List<DealerInformation> dealerList { get; set; }
        public UserPermission userP { get; set; }
    }
}
