﻿using VehicleServiceManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AccounTechSoftware.Models
{
    public class VatCategory
    {

        public int VatId { get; set; }
        [Required(ErrorMessage = "Please Enter  Code")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please Enter  Category")]
        public string Category { get; set; }
        [Required(ErrorMessage = "Please Enter  RatePerscent")]
        public float RatePerscent { get; set; }
        [Required(ErrorMessage = "Please Enter  Description")]
        public string Description { get; set; }
        public List<VatCategory> vatCategories { get; set; }
        public UserPermission userP { get; set; }

        public int create(VatCategory obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddVatCategory");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Code", obj.Code);
            cmd.Parameters.AddWithValue("Category", obj.Category);
            cmd.Parameters.AddWithValue("RatePerscent", obj.RatePerscent);
            cmd.Parameters.AddWithValue("Description", obj.Description);
        
            i = DbConnection.Create(cmd);
            return i;
        }

        public List<VatCategory> GetAllVat()
        {

            List<VatCategory> objlist = new List<VatCategory>();

            SqlCommand com = new SqlCommand("GetAllVat");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                objlist.Add(

                    new VatCategory
                    {

                        VatId = Convert.ToInt32(dr["VatId"]),
                        Code = Convert.ToString(dr["Code"]),
                        Category = Convert.ToString(dr["Category"]),
                        RatePerscent = float.Parse(dr["RatePerscent"].ToString()),
                        Description = Convert.ToString(dr["Description"]),
                    }
                    );
            }

            return objlist;
        }

        public VatCategory GetbyId(int Id)
        {
            VatCategory obj = new VatCategory();
            SqlCommand com = new SqlCommand("GetVatById");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("VatId", Id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                obj.VatId = Convert.ToInt32(dr["VatId"]);
                obj.Code = Convert.ToString(dr["Code"]);
                obj.Category = Convert.ToString(dr["Category"]);
                obj.RatePerscent = float.Parse(dr["RatePerscent"].ToString());
                obj.Description = Convert.ToString(dr["Description"]);
            }
            return (obj);
        }

        public int update(VatCategory obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateVatCategory");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("VatId", obj.VatId);
            cmd.Parameters.AddWithValue("Code", obj.Code);
            cmd.Parameters.AddWithValue("Category", obj.Category);
            cmd.Parameters.AddWithValue("RatePerscent", obj.RatePerscent);
            cmd.Parameters.AddWithValue("Description", obj.Description);
            i = DbConnection.Update(cmd);
            return i;
        }

        public int delete(int Id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("VatCategoryDelete");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VatId", Id);
            i = DbConnection.Delete(cmd);
            return i;
        }

    }

}
