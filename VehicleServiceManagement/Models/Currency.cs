﻿using VehicleServiceManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AccounTechSoftware.Models
{
    public class Currency
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Currency Name")]
        public string CurrencyName { get; set; }
        [Required(ErrorMessage = "Please Enter Currency Symbol")]
        public string Symbol { get; set; }
        [Required(ErrorMessage = "Please select Country")]
        public int Country { get; set; }
        public string CountryName { get; set; }
        [Required(ErrorMessage = "Please Enter Sub Division")]
        public string SubDivision { get; set; }        
        public string Note { get; set; }        
        public float ExchangeRate { get; set; }
        public Boolean setDefault { get; set; }

        public List<Currency> currencylist { get; set; }
        /**Used***/
        public int create(Currency obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddCurrency");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("CurrencyName", obj.CurrencyName);
            cmd.Parameters.AddWithValue("Symbol", obj.Symbol);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("SubDivision", obj.SubDivision);
            cmd.Parameters.AddWithValue("Note", obj.Note);
            cmd.Parameters.AddWithValue("ExchangeRate", obj.ExchangeRate);
            cmd.Parameters.AddWithValue("setDefault", obj.setDefault);
            i = DbConnection.Create(cmd);
            return i;
        }
        /**Used***/
        public List<Currency> GetAllCurrencies()
        {

            List<Currency> Currency = new List<Currency>();

            SqlCommand com = new SqlCommand("ReadCurrency");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Currency.Add(

                    new Currency
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        CurrencyName = Convert.ToString(dr["CurrencyName"]),
                        Symbol = Convert.ToString(dr["Symbol"]),
                        Country = Convert.ToInt32(dr["Country"]),
                        CountryName = Convert.ToString(dr["CountryName"]),
                        SubDivision = Convert.ToString(dr["SubDivision"]),
                        Note = Convert.ToString(dr["Note"]),
                        ExchangeRate = Convert.ToInt32(dr["ExchangeRate"]),
                    }


                    );
            }

             return Currency;

        }
        /**Used***/
        public Currency details(int id)
        {
            Currency obj = new Currency();
            SqlCommand com = new SqlCommand("ViewCurrencyDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.CurrencyName = dr["CurrencyName"].ToString();
            obj.Symbol = dr["Symbol"].ToString();
            obj.Country = Convert.ToInt32(dr["Country"]);
            obj.SubDivision = dr["SubDivision"].ToString();
            obj.Note = dr["Note"].ToString();
            obj.ExchangeRate = Convert.ToInt32(dr["ExchangeRate"]);
            return (obj);
        }
        /**Used***/
        public int update(Currency obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateCurrency");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("CurrencyName", obj.CurrencyName);
            cmd.Parameters.AddWithValue("Symbol", obj.Symbol);
            cmd.Parameters.AddWithValue("Country", obj.Country);
            cmd.Parameters.AddWithValue("SubDivision", obj.SubDivision);
            cmd.Parameters.AddWithValue("Note", obj.Note);
            cmd.Parameters.AddWithValue("ExchangeRate", obj.ExchangeRate);

            i = DbConnection.Update(cmd);
            return i;
        }
        /**Used***/
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteCurrency");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(cmd);
            return i;
        }

        
    }
}
