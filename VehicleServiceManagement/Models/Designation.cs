﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Designation
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Designation Name")]
        public string Name { get; set; }
        
        public string Code { get; set; }
       
        public List<Designation> designationlist { get; set; }
        public int create(Designation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddDesignation");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Name", obj.Name);
            cmd.Parameters.AddWithValue("Code", obj.Code);
           

            i = DbConnection.Create(cmd);
            return i;
        }

        public List<Designation> GetAllDesignation()
        {

            List<Designation> Designation = new List<Designation>();

            SqlCommand com = new SqlCommand("GetDesignation");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Designation.Add(

                    new Designation
                    {

                         Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        Code = Convert.ToString(dr["Code"]),
                        

                    }


                    );
            }

            return Designation;

        }
        public Designation details(int id)
        {

            Designation dsg = new Designation();
            SqlCommand com = new SqlCommand("GetDesignationList");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            dsg.Id = Convert.ToInt32(dr["Id"]);
            dsg.Name = dr["Name"].ToString();
            dsg.Code = dr["Code"].ToString();
           


            return (dsg);

        }
        public int update(Designation o)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("UpdateDesignation");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("Id", o.Id);
            com.Parameters.AddWithValue("Name", o.Name);
            com.Parameters.AddWithValue("Code", o.Code);
            i = DbConnection.Update(com);
            return i;


        }
        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteDesignation");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }

    }
    public class DesignationDisplay
    {
        public List<Designation> designationList { get; set; }
        public UserPermission userP { get; set; }
    }
}
