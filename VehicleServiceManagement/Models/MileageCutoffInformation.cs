﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class MileageCutoffInformation
    {
         public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  MilageCutoff")]
        public float MilageCutoff { get; set; }
  
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter  Name")]
        public int DealerId { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public string SubGroup { get; set; }
      
        public string Duration { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  Duration")]
        public int DurationId { get; set; }
        [Required(ErrorMessage = "Please Enter  CategoryName")]
        public int CategoryId { get; set; }
     
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public int create(MileageCutoffInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddMileageCutoffInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@MilageCutoff", obj.MilageCutoff);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@DurationId", obj.DurationId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<MileageCutoffInformation> GetAllMileageCutoffInformation()
        {

            List<MileageCutoffInformation> Infromation = new List<MileageCutoffInformation>();

            SqlCommand com = new SqlCommand("ReadMileageCutoffInformation");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new MileageCutoffInformation
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        MilageCutoff = Convert.ToSingle(dr["MilageCutoff"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        Name = Convert.ToString(dr["Name"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        SubGroup = Convert.ToString(dr["SubGroup"]),
                        Duration = Convert.ToString(dr["Duration"]),
                        DurationId = Convert.ToInt32(dr["DurationId"]),
                        CategoryId = Convert.ToInt32(dr["CategoryId"]),
                        CategoryName = Convert.ToString(dr["CategoryName"]),
                        Remark = Convert.ToString(dr["Remark"]),

                    }
                    );
            }

            return Infromation;

        }

        public List<MileageCutoffInformation> GetAllMileageCutoffBylist(int durationid, int dealerId)
        {

            List<MileageCutoffInformation> Infromation = new List<MileageCutoffInformation>();

            SqlCommand com = new SqlCommand("GetMilageCuttoffbyList");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DurationId", durationid);
            com.Parameters.AddWithValue("@DealerId", dealerId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new MileageCutoffInformation
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        MilageCutoff = Convert.ToSingle(dr["MilageCutoff"]),                     
                    }
                    );
            }

            return Infromation;

        }



        public MileageCutoffInformation details(int id)
        {
            MileageCutoffInformation obj = new MileageCutoffInformation();
            SqlCommand com = new SqlCommand("ViewMileageCutoffInformation");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.MilageCutoff = Convert.ToSingle(dr["MilageCutoff"]);
            obj.DealerId = Convert.ToInt32(dr["DealerId"]);

            obj.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);

            obj.DurationId = Convert.ToInt32(dr["DurationId"]);
            obj.CategoryId = Convert.ToInt32(dr["CategoryId"]);

            obj.Remark = Convert.ToString(dr["Remark"]);
            return (obj);


        }
        public int update(MileageCutoffInformation obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateMMileageCutoffInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@MilageCutoff", obj.MilageCutoff);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@DurationId", obj.DurationId);
            cmd.Parameters.AddWithValue("@CategoryId", obj.CategoryId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteMileageCutoffInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class MileageCutoffInformationDisplay
    {
        public List<MileageCutoffInformation> mileageCutoffList { get; set; }
        public UserPermission userP { get; set; }
    }
}
