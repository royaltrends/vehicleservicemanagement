﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ProductSubGroup
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public string SubGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  ProductGroup")]
        public int GroupId { get; set; }
        public string ProductGroup { get; set; }
        [Required(ErrorMessage = "Please Enter  Remark")]
        public string Remark { get; set; }
        public int create(ProductSubGroup obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddProductSubGroup ");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@SubGroup", obj.SubGroup);

            cmd.Parameters.AddWithValue("@GroupId", obj.GroupId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<ProductSubGroup> GetAllProductSubGroup()
        {

            List<ProductSubGroup> Infromation = new List<ProductSubGroup>();

            SqlCommand com = new SqlCommand("ReadProductSubGroup");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new ProductSubGroup
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        SubGroup = Convert.ToString(dr["SubGroup"]),
                        GroupId = Convert.ToInt32(dr["GroupId"]),
                        ProductGroup= Convert.ToString(dr["ProductGroup"]),
                        Remark = Convert.ToString(dr["Remark"])
                    }
                    );
            }

            return Infromation;

        }

        public List<ProductSubGroup> GetSubGroupByProductGroup(int productgroup)
        {

            List<ProductSubGroup> Infromation = new List<ProductSubGroup>();

            SqlCommand com = new SqlCommand("GetSubGroupByProductGroup");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ProductGroup", productgroup);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Infromation.Add(

                    new ProductSubGroup
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        SubGroup = Convert.ToString(dr["SubGroup"]),
                        
                    }
                    );
            }

            return Infromation;

        }


        public ProductSubGroup details(int id)
        {
            ProductSubGroup obj = new ProductSubGroup();
            SqlCommand com = new SqlCommand("ViewProductSubGroup");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);

            obj.SubGroup = Convert.ToString(dr["SubGroup"]);
            obj.GroupId = Convert.ToInt32(dr["GroupId"]);
            obj.Remark = Convert.ToString(dr["Remark"]);


            return (obj);


        }
        public int update(ProductSubGroup obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateProductSubGroup");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);

            cmd.Parameters.AddWithValue("@SubGroup", obj.SubGroup);

            cmd.Parameters.AddWithValue("@GroupId", obj.GroupId);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteSubGroup");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);

            i = DbConnection.Delete(cmd);
            return i;


        }

    }

    public class ProductSubGroupDisplay
    {
        public List<ProductSubGroup> productSubGroupList { get; set; }
        public UserPermission userP { get; set; }
    }
}
