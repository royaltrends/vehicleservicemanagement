﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class OtherService
    {
        public string UID { get; set; }

        public int Id { get; set; }
       
        public int? OtherServiceId { get; set; }
        public string OtherServiceName { get; set; }
   
        public int WorkshopId { get; set; }
      
        public string Workshop { get; set; }
       
        public int WorkshopBranchId { get; set; }
        public string WorkshopBranch { get; set; }
     
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    
        public int ProductSubGroupId { get; set; }
        public string SubGroup { get; set; }
    
        public int CCRangeId { get; set; }
        public string Range { get; set; }
 
        public int ManuDuration { get; set; }
     
        public int ManuCuttoff { get; set; }
      
        public int Duration { get; set; }
       
        public int MilageCuttoff { get; set; }
      
        public float? MinPremium { get; set; }
     
        public float? Premium { get; set; }
      
        public int? Quantity { get; set; }

    }
}
