﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class WorkTaskLog
    {
        public long LogId { get; set; }
        public long JobNo { get; set; }   
        public DateTime CreateDate { get; set; }            
        public float CompletePercntage { get; set; }     
        public EnumWorkStatus Status { get; set; }
        public string Remark { get; set; }


        public int create(WorkTaskLog obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddEmployeeWorkTaskLog");
            cmd.CommandType = CommandType.StoredProcedure;            
            cmd.Parameters.AddWithValue("@JobNo", obj.JobNo);
            cmd.Parameters.AddWithValue("@CreateDate", obj.CreateDate);          
            cmd.Parameters.AddWithValue("@CompletePercntage", obj.CompletePercntage);         
            cmd.Parameters.AddWithValue("@Status", obj.Status);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);     
            i = DbConnection.Create(cmd);
            return i;
        }
               
        public List<WorkTask> GetAllWorkTask()
        {
            List<WorkTask> master = new List<WorkTask>();

            SqlCommand com = new SqlCommand("GetAllEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {                
                master.Add(
                    new WorkTask
                    {
                        JobNo = Convert.ToInt32(dr["JobNo"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        TargetDate = Convert.ToDateTime(dr["TargetDate"]),
                        Subject = Convert.ToString(dr["Subject"]),
                        Status = (EnumWorkStatus)(dr["Status"]),
                    }
                    );
            }

            return master;


        }

        public WorkTask details(int jobid)
        {

            WorkTask dsg = new WorkTask();
            SqlCommand com = new SqlCommand("GetEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", jobid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                dsg.JobNo = Convert.ToInt32(dr["JobNo"]);
                dsg.StartDate = Convert.ToDateTime(dr["StartDate"]);
                dsg.TargetDate = Convert.ToDateTime(dr["TargetDate"]);
                dsg.Subject = Convert.ToString(dr["Subject"]);
                dsg.WorkDetails = Convert.ToString(dr["WorkDetails"]);
                dsg.CompletePercentage = float.Parse(dr["CompletePercntage"].ToString());              
                dsg.Status = (EnumWorkStatus)(dr["Status"]);
                dsg.Remark = Convert.ToString(dr["Remark"]);            
            }
            return (dsg);

        }

        public int update(WorkTask obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateEmployeeWorkTask");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobNo", obj.JobNo);
            cmd.Parameters.AddWithValue("@EmployeeId", obj.EmployeeId);
            cmd.Parameters.AddWithValue("@StartDate", obj.StartDate);
            cmd.Parameters.AddWithValue("@TargetDate", obj.TargetDate);
            cmd.Parameters.AddWithValue("@Subject", obj.Subject);
            cmd.Parameters.AddWithValue("@WorkDetails", obj.WorkDetails);
            cmd.Parameters.AddWithValue("@CompletePercntage", obj.CompletePercentage);
            cmd.Parameters.AddWithValue("@priority", obj.Priority);
            cmd.Parameters.AddWithValue("@Status", obj.Status);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);
            i = DbConnection.Update(cmd);
            return i;
        }

        public int delete()
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@JobNo", JobNo);         
            i = DbConnection.Delete(com);
            return i;
        }


    }

   
}
