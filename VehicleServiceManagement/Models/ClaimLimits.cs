﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ClaimLimits
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter  ClaimLimit")]
        public string ClaimLimit { get; set; }
        [Required(ErrorMessage = "Please Enter  Name")]
        public int DealerId { get; set; }
        [Required(ErrorMessage = "Please Enter  SubGroup")]
        public int SubGroupId { get; set; }
        [Required(ErrorMessage = "Please Enter  Remarks")]
        public string Remarks { get; set; }
        public string Name { get; set; }
        public string SubGroup { get; set; }

        public int create(ClaimLimits obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddClaimLimits");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ClaimLimit", obj.ClaimLimit);
            cmd.Parameters.AddWithValue("@DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("@SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);

            i = DbConnection.Create(cmd);

            return i;

        }

        public List<ClaimLimits> GetAllClaimLimits()
        {

            List<ClaimLimits> claimlimit = new List<ClaimLimits>();

            SqlCommand com = new SqlCommand("ReadClaimLimits");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                claimlimit.Add(

                    new ClaimLimits
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        ClaimLimit = Convert.ToString(dr["ClaimLimit"]),
                        DealerId = Convert.ToInt32(dr["DealerId"]),
                        SubGroupId = Convert.ToInt32(dr["SubGroupId"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        SubGroup = Convert.ToString(dr["SubGroup"]),
                        Name = Convert.ToString(dr["Name"])


                    }


                    );
            }

            return claimlimit;

        }


        public ClaimLimits details(int id)
        {
            ClaimLimits con = new ClaimLimits();
            SqlCommand com = new SqlCommand("ClaimLimitsDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                con.ClaimLimit = Convert.ToString(dr["ClaimLimit"]);
                con.DealerId = Convert.ToInt32(dr["DealerId"]);
                con.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
                con.Remarks = Convert.ToString(dr["Remarks"]);

            }


            return con;

        }
        public int update(ClaimLimits obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("ClaimLimitsUpdate");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("ClaimLimit", obj.ClaimLimit);
            cmd.Parameters.AddWithValue("DealerId", obj.DealerId);
            cmd.Parameters.AddWithValue("SubGroupId", obj.SubGroupId);
            cmd.Parameters.AddWithValue("Remarks", obj.Remarks);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("ClaimLimitsDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }

    }

    public class ClaimLimitsDisplay
    {
        public List<ClaimLimits> claimLimitsList { get; set; }
        public UserPermission userP { get; set; }
    }
}
