﻿using VehicleServiceManagement.Models;
using VehicleServiceManagement.Models.Accounts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessSoftware.Models
{
    public class PurchaseInvoice
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public string UID { get; set; }

        public PurchaseMaster PurchaseInvoiceMaster { get; set; }
        public PurchaseDetails PurchaseInvoiceDetails { get; set; }
       
        public ClsAccountTransaction Transaction { get; set; }
        public List<PurchaseDetails> PurchaseInvoiceDetailslist { get; set; }
     
        public List<PurchaseInvoice> PurchaseInvoicelist { get; set; }
   

        public long PurchaseCreate(PurchaseInvoice obj)
        {
                    
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand("AddPurchase");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("DealerLedgerId", obj.PurchaseInvoiceMaster.DealerLedgerId);          
            cmd.Parameters.AddWithValue("Address", obj.PurchaseInvoiceMaster.Address);
            cmd.Parameters.AddWithValue("Date", obj.PurchaseInvoiceMaster.Date);         
            cmd.Parameters.AddWithValue("EmployeeId", obj.PurchaseInvoiceMaster.EmployeeId);
            cmd.Parameters.AddWithValue("PolicyNumber", obj.PurchaseInvoiceMaster.PolicyNumber);
            cmd.Parameters.AddWithValue("DealerInvoiceNo", obj.PurchaseInvoiceMaster.DealerInvoiceNo);
            cmd.Parameters.AddWithValue("Reference", obj.PurchaseInvoiceMaster.Reference);
            cmd.Parameters.AddWithValue("Remark", obj.PurchaseInvoiceMaster.Remarks);
            cmd.Parameters.AddWithValue("PurchaseTotal", obj.PurchaseInvoiceMaster.PurchaseTotal);
            cmd.Parameters.AddWithValue("Discount", obj.PurchaseInvoiceMaster.Discount);
            cmd.Parameters.AddWithValue("VATAmount", obj.PurchaseInvoiceMaster.VATAmount);
            cmd.Parameters.AddWithValue("SubTotal", obj.PurchaseInvoiceMaster.SubTotal);          
            return DbConnection.CreatewithId(cmd);
        }

        public int PurchaseInvoiceDetailsCreate(List<PurchaseDetails> InvList,long purchaseId)
        {
            int i = 0;
            PurchaseInvoice obj = new PurchaseInvoice();

            foreach (var item in InvList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddPurchaseDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("PurchaseId", purchaseId);
                cmd.Parameters.AddWithValue("Particular", item.Particular);   
                cmd.Parameters.AddWithValue("Quantity", item.Quantity);               
                cmd.Parameters.AddWithValue("Price", item.Price);
                cmd.Parameters.AddWithValue("Vatcategory", item.Vatcategory);
                cmd.Parameters.AddWithValue("Vat", item.Vat);
                cmd.Parameters.AddWithValue("Total", item.Total);               
                i = DbConnection.Create(cmd);
            }
            return i;
        }
     
        public List<PurchaseDetails> GetAllDetails(int PurchaseId)
        {

            List<PurchaseDetails> pi = new List<PurchaseDetails>();

            SqlCommand com = new SqlCommand("ReadPurchaseDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("PurchaseId", PurchaseId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                pi.Add(

                    new PurchaseDetails
                    {
                        Id = Convert.ToInt64(dr["Id"]),
                        PurchaseId = Convert.ToInt64(dr["PurchaseId"]),
                        Particular = Convert.ToString(dr["Purticulars"]),                    
                        Quantity = Convert.ToInt32(dr["Quantity"]),                   
                        Price = Convert.ToSingle(dr["Price"]),
                        Vatcategory = Convert.ToString(dr["Vatcategory"]),
                        Vat = Convert.ToSingle(dr["Vat"]),
                        Total = Convert.ToSingle(dr["Total"])
                    }
                    );
            }

            return pi;

        }


        public PurchaseMaster details(int id)
        {
            PurchaseMaster obj = new PurchaseMaster();
            SqlCommand com = new SqlCommand("ViewPurchase");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {

                obj.Id = Convert.ToInt32(dt.Rows[0]["Id"]);             
                obj.DealerLedgerId = Convert.ToInt32(dt.Rows[0]["DealerLedgerId"]);
                obj.Address = dt.Rows[0]["Address"].ToString();             
                obj.Date = Convert.ToDateTime(dt.Rows[0]["Date"]);
                obj.EmployeeId = Convert.ToInt32(dt.Rows[0]["Employee"]);               
                obj.PolicyNumber = Convert.ToString(dt.Rows[0]["PolicyNumber"]);
                obj.DealerInvoiceNo = Convert.ToInt32(dt.Rows[0]["DealerInvoiceNo"]);               
                obj.Reference = dt.Rows[0]["Reference"].ToString();
                obj.Remarks = dt.Rows[0]["Remarks"].ToString();
                obj.PurchaseTotal = Convert.ToSingle(dt.Rows[0]["PurchaseTotal"]);
                obj.Discount = Convert.ToSingle(dt.Rows[0]["Discount"]);
                obj.VATAmount = Convert.ToSingle(dt.Rows[0]["VATAmount"]);
                obj.SubTotal = Convert.ToSingle(dt.Rows[0]["SubTotal"]);
              
            }
            return (obj);
        }

        public List<PurchaseMaster> GetInvoiceByVendor(int id)
        {
            List<PurchaseMaster> master = new List<PurchaseMaster>();

            SqlCommand com = new SqlCommand("ViewPurchasebyDealer");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@DealerId", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new PurchaseMaster
                    {

                        Id = Convert.ToInt32(dr["Id"]),                     
                        Address = dt.Rows[0]["Address"].ToString(),               
                        Date = Convert.ToDateTime(dt.Rows[0]["Date"]),
                        EmployeeId = Convert.ToInt32(dt.Rows[0]["Employee"]),                      
                        PolicyNumber = Convert.ToString(dt.Rows[0]["PolicyNumber"]),                      
                        DealerInvoiceNo = Convert.ToInt32(dt.Rows[0]["DealerInvoiceNo"]),                      
                        Reference = dt.Rows[0]["Reference"].ToString(),
                        Remarks = dt.Rows[0]["Remarks"].ToString(),
                        PurchaseTotal = Convert.ToSingle(dt.Rows[0]["PurchaseTotal"]),
                        Discount = Convert.ToSingle(dt.Rows[0]["Discount"]),
                        VATAmount = Convert.ToSingle(dt.Rows[0]["VATAmount"]),
                        SubTotal = Convert.ToSingle(dt.Rows[0]["SubTotal"]),                       
                    }
                    );
            }

            return master;


        }
               
        public int UpdatePurchaseInvoiceMaster(PurchaseInvoice obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdatePurchase");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);          
            cmd.Parameters.AddWithValue("DealerLedgerId", obj.PurchaseInvoiceMaster.DealerLedgerId);
            cmd.Parameters.AddWithValue("Address", obj.PurchaseInvoiceMaster.Address);         
            cmd.Parameters.AddWithValue("Date", obj.PurchaseInvoiceMaster.Date);
            cmd.Parameters.AddWithValue("Employee", obj.PurchaseInvoiceMaster.EmployeeId);
            cmd.Parameters.AddWithValue("PolicyNumber", obj.PurchaseInvoiceMaster.PolicyNumber);
            cmd.Parameters.AddWithValue("DealerInvoiceNo", obj.PurchaseInvoiceMaster.DealerInvoiceNo);
            cmd.Parameters.AddWithValue("Status", obj.PurchaseInvoiceMaster.Status);
            cmd.Parameters.AddWithValue("Reference", obj.PurchaseInvoiceMaster.Reference);
            cmd.Parameters.AddWithValue("Remarks", obj.PurchaseInvoiceMaster.Remarks);
            cmd.Parameters.AddWithValue("PurchaseTotal", obj.PurchaseInvoiceMaster.PurchaseTotal);
            cmd.Parameters.AddWithValue("Discount", obj.PurchaseInvoiceMaster.Discount);
            cmd.Parameters.AddWithValue("VATAmount", obj.PurchaseInvoiceMaster.VATAmount);
            cmd.Parameters.AddWithValue("SubTotal", obj.PurchaseInvoiceMaster.SubTotal);          
            i = DbConnection.Update(cmd);
            return i;


        }

        public int UpdateDetails(List<PurchaseDetails> InqList)
        {
            int i = 0;
            PurchaseInvoice obj = new PurchaseInvoice();
            SqlCommand cmd1 = new SqlCommand();
            cmd1 = new SqlCommand("DeletePurchaseDetails");
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@PurchaseId", InqList[0].PurchaseId);
            i = DbConnection.Delete(cmd1);
            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddPurchaseDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("PurchaseId", item.PurchaseId);
                cmd.Parameters.AddWithValue("Purticulars", item.Particular);
                cmd.Parameters.AddWithValue("Quantity", item.Quantity);             
                cmd.Parameters.AddWithValue("Price", item.Price);
                cmd.Parameters.AddWithValue("Vatcategory", item.Vatcategory);
                cmd.Parameters.AddWithValue("Vat", item.Vat);
                cmd.Parameters.AddWithValue("Total", item.Total);
                i = DbConnection.Create(cmd);
            }
            return i;
        }
      

        public int PurchaseInvoiceMasterDelete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeletePurchase");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }

        public int PurchaseInvoiceDetailsDelete(List<PurchaseDetails> InqList)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeletePurchaseDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@PurchaseId", InqList[0].PurchaseId);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }


        public PurchaseMaster GetDetailsByVendor(int id)
        {
            PurchaseMaster obj = new PurchaseMaster();
            SqlCommand com = new SqlCommand("ViewPurchaseTotalMaster");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@VendorId", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                obj.SubTotal = Convert.ToSingle(dt.Rows[0]["SubTotal"]);
            }
            return (obj);
        }
       
    }
}
