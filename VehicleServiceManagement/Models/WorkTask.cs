﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class WorkTask
    {
        public long JobNo { get; set; }
        [Required(ErrorMessage = "Please Enter Employee ID")]
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "StartDate")]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "TargetDate")]
        public DateTime TargetDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "FinishDate")]
        public DateTime FinishedDate { get; set; }
        public string Subject { get; set; }
        public string WorkDetails { get; set; }
        public float CompletePercentage { get; set; }
        public EnumWorkPriority Priority { get; set; }
        public EnumWorkStatus Status { get; set; }
        public string Remark { get; set; }


        public int create(WorkTask obj)
        {

            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddEmployeeWorkTask");
            cmd.CommandType = CommandType.StoredProcedure;            
            cmd.Parameters.AddWithValue("@EmployeeId", obj.EmployeeId);
            cmd.Parameters.AddWithValue("@StartDate", obj.StartDate);
            cmd.Parameters.AddWithValue("@TargetDate", obj.TargetDate);
            cmd.Parameters.AddWithValue("@Subject", obj.Subject);
            cmd.Parameters.AddWithValue("@WorkDetails", obj.WorkDetails);
            cmd.Parameters.AddWithValue("@CompletePercentage", obj.CompletePercentage);
            cmd.Parameters.AddWithValue("@priority", obj.Priority);
            cmd.Parameters.AddWithValue("@Status", obj.Status);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);           

            i = DbConnection.Create(cmd);
            return i;
        }
               
        public List<WorkTask> GetAllWorkTask()
        {

            List<WorkTask> master = new List<WorkTask>();
            SqlCommand com = new SqlCommand("GetAllEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {                
                master.Add(
                    new WorkTask
                    {
                        JobNo = Convert.ToInt32(dr["JobNo"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        TargetDate = Convert.ToDateTime(dr["TargetDate"]),
                        Subject = Convert.ToString(dr["Subject"]),
                        Status = (EnumWorkStatus)(dr["Status"]),
                    }
                    );
            }
            return master;

        }

        public List<WorkTask> GetAllWorkTaskByEmployee()
        {

            List<WorkTask> master = new List<WorkTask>();
            SqlCommand com = new SqlCommand("GetAllWorkTaskbyEmployee");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@EmployeeId", EmployeeId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {
                master.Add(
                    new WorkTask
                    {
                        JobNo = Convert.ToInt32(dr["JobNo"]),
                        StartDate = Convert.ToDateTime(dr["StartDate"]),
                        TargetDate = Convert.ToDateTime(dr["TargetDate"]),
                        Subject = Convert.ToString(dr["Subject"]),
                        Status = (EnumWorkStatus)(dr["Status"]),
                    }
                    );
            }
            return master;

        }

        public WorkTask details()
        {

            WorkTask dsg = new WorkTask();
            SqlCommand com = new SqlCommand("GetEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@JobNo", JobNo);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                dsg.JobNo = Convert.ToInt32(dr["JobNo"]);
                dsg.EmployeeName = Convert.ToString(dr["EmployeeName"]);
                dsg.DepartmentName = Convert.ToString(dr["DepartmentName"]);
                dsg.StartDate = Convert.ToDateTime(dr["StartDate"]);
                dsg.TargetDate = Convert.ToDateTime(dr["TargetDate"]);
                dsg.Subject = Convert.ToString(dr["Subject"]);
                dsg.WorkDetails = Convert.ToString(dr["WorkDetails"]);
                dsg.CompletePercentage = float.Parse(dr["CompletePercentage"].ToString());
                dsg.Priority = (EnumWorkPriority)(dr["priority"]);
                dsg.Status = (EnumWorkStatus)(dr["Status"]);
                dsg.Remark = Convert.ToString(dr["Remark"]);            
            }
            return (dsg);

        }

        public int update(WorkTask obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateEmployeeWorkTask");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobNo", obj.JobNo);
            cmd.Parameters.AddWithValue("@EmployeeId", obj.EmployeeId);
            cmd.Parameters.AddWithValue("@StartDate", obj.StartDate);
            cmd.Parameters.AddWithValue("@TargetDate", obj.TargetDate);
            cmd.Parameters.AddWithValue("@Subject", obj.Subject);
            cmd.Parameters.AddWithValue("@WorkDetails", obj.WorkDetails);
            cmd.Parameters.AddWithValue("@CompletePercntage", obj.CompletePercentage);
            cmd.Parameters.AddWithValue("@priority", obj.Priority);
            cmd.Parameters.AddWithValue("@Status", obj.Status);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);
            i = DbConnection.Update(cmd);
            return i;
        }

        public int updatebyEmployee(WorkTask obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateWorkTaskbyEmployee");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobNo", obj.JobNo);          
            cmd.Parameters.AddWithValue("@CompletePercentage", obj.CompletePercentage);           
            cmd.Parameters.AddWithValue("@Status", obj.Status);
            cmd.Parameters.AddWithValue("@Remark", obj.Remark);
            i = DbConnection.Update(cmd);
            return i;
        }

        public int delete()
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteEmployeeWorkTask");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@JobNo", JobNo);         
            i = DbConnection.Delete(com);
            return i;
        }


    }

    public class WorkTaskDisplay
    {
        public List<WorkTask> taskList { get; set; }
        public UserPermission userP { get; set; }
    }

    public enum EnumWorkPriority
    {
        Urgent = 1,
        VeryImportand = 2,
        Importand = 3,
        Normal = 4,       
    }

    public enum EnumWorkStatus
    {
        NotStarted = 1,
        InProgress = 2,
        Completed = 3,
        Cancelled = 4,
       
    }

}
