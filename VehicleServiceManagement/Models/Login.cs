﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Login
    {
        public int Id { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int LoginId { get; set; }

        public bool GetAllLogin(Login obj)
        {
            bool status = false;
            object ob;
            if (!string.IsNullOrEmpty(obj.UserName) && !string.IsNullOrEmpty(obj.Password))
            {
                SqlCommand com = new SqlCommand("CheckLogin");
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("Username", obj.UserName);
                com.Parameters.AddWithValue("Password", obj.Password);
                ob = DbConnection.GetObject(com);
                status = Convert.ToBoolean(ob);
            }
            return (status);
        }

        public Login GetLoginId(string username)
        {
            Login obj = new Login();
            SqlCommand com = new SqlCommand("GetLoginId");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("username", username);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.LoginId = Convert.ToInt32(dr["LoginId"]);
            return (obj);
        }

    }
}