﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class ContractManagement
    {
        public ContractInformation contractInformation { get; set; }
        public OtherService otherService { get; set; }
        public List<ContractInformation> contractInformations { get; set; }
        public List<OtherService> otherServices { get; set; }
        public string savetype { get; set; }


        public int CreateMaster(ContractManagement sale)
        {

            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddContractInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ContractNumber", sale.contractInformation.ContractNumber);
            cmd.Parameters.AddWithValue("@IssueDate", sale.contractInformation.IssueDate);
            cmd.Parameters.AddWithValue("@ClientReferenceNo", sale.contractInformation.ClientReferenceNo);
            cmd.Parameters.AddWithValue("@DealerId", sale.contractInformation.DealerId);
            cmd.Parameters.AddWithValue("@BranchId", sale.contractInformation.BranchId);
            cmd.Parameters.AddWithValue("@FilePath", sale.contractInformation.FilePath);
            cmd.Parameters.AddWithValue("@CustomerName", sale.contractInformation.CustomerName);
            cmd.Parameters.AddWithValue("@Email", sale.contractInformation.Email);
            cmd.Parameters.AddWithValue("@Mobile", sale.contractInformation.Mobile);
            cmd.Parameters.AddWithValue("@City", sale.contractInformation.City);
            cmd.Parameters.AddWithValue("@Address", sale.contractInformation.Address);
            cmd.Parameters.AddWithValue("@ProductGroupId", sale.contractInformation.ProductGroupId);
            cmd.Parameters.AddWithValue("@ProductSubGroupId", sale.contractInformation.ProductSubGroupId);
            cmd.Parameters.AddWithValue("@SoldDate", sale.contractInformation.SoldDate);
            cmd.Parameters.AddWithValue("@MakeId", sale.contractInformation.MakeId);
            cmd.Parameters.AddWithValue("@ModelId", sale.contractInformation.ModelId);
            cmd.Parameters.AddWithValue("@CategoryName", sale.contractInformation.CategoryName);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceDuration", sale.contractInformation.ManufacturerAssistanceDuration);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceMileageCutoff", sale.contractInformation.ManufacturerAssistanceMileageCutoff);
            cmd.Parameters.AddWithValue("@ModelYear", sale.contractInformation.ModelYear);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceDuration", sale.contractInformation.ExtendedAssistanceDuration);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceMileageCutoff", sale.contractInformation.ExtendedAssistanceMileageCutoff);
            cmd.Parameters.AddWithValue("@Remarks", sale.contractInformation.Remarks);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceStartDate", sale.contractInformation.ExtendedAssistanceStartDate);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceExpiryDate", sale.contractInformation.ExtendedAssistanceExpiryDate);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceExpiryDate", sale.contractInformation.ManufacturerAssistanceExpiryDate);
            cmd.Parameters.AddWithValue("@ChassisNo", sale.contractInformation.ChassisNo);
            cmd.Parameters.AddWithValue("@LimitPerClaimId", sale.contractInformation.LimitPerClaimId);
            cmd.Parameters.AddWithValue("@ProductPrice", sale.contractInformation.ProductPrice);
            cmd.Parameters.AddWithValue("@CurrentMileageId", sale.contractInformation.CurrentMileageId);
            cmd.Parameters.AddWithValue("@CCRangeId", sale.contractInformation.CCRangeId);
            cmd.Parameters.AddWithValue("@CubicCapacity", sale.contractInformation.CubicCapacity);
            cmd.Parameters.AddWithValue("@RegistrationNo", sale.contractInformation.RegistrationNo);
            cmd.Parameters.AddWithValue("@RegistrationDate", sale.contractInformation.RegistrationDate);
            cmd.Parameters.AddWithValue("@PlateType", sale.contractInformation.PlateType);
            cmd.Parameters.AddWithValue("@SoldBy", sale.contractInformation.SoldBy);
            cmd.Parameters.AddWithValue("@MinPremium", sale.contractInformation.MinPremium);
            cmd.Parameters.AddWithValue("@Premium", sale.contractInformation.Premium);
            cmd.Parameters.AddWithValue("@Status", sale.contractInformation.Status);

            i = DbConnection.Create(cmd);

            return i;

        }

        public int CreateDetails(List<OtherService> InqList, long contractnumber)
        {
            int i = 0;


            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddOtherServices");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ContractNumber", contractnumber);
                cmd.Parameters.AddWithValue("@OtherServiceId", item.OtherServiceId);
                cmd.Parameters.AddWithValue("@WorkshopId", item.WorkshopId);
                cmd.Parameters.AddWithValue("@WorkshopBranchId", item.WorkshopBranchId);
                cmd.Parameters.AddWithValue("@CategoryId", item.CategoryId);
                cmd.Parameters.AddWithValue("@ProductSubGroupId", item.ProductSubGroupId);
                cmd.Parameters.AddWithValue("@CCRangeId", item.CCRangeId);
                cmd.Parameters.AddWithValue("@ManuDuration", item.ManuDuration);
                cmd.Parameters.AddWithValue("@ManuCuttoff", item.ManuCuttoff);
                cmd.Parameters.AddWithValue("@Durations", item.Duration);
                cmd.Parameters.AddWithValue("@MilageCuttoff", item.MilageCuttoff);
                cmd.Parameters.AddWithValue("@MinPremium", item.MinPremium);
                cmd.Parameters.AddWithValue("@Premium", item.Premium);
                cmd.Parameters.AddWithValue("@Quantity", item.Quantity);
                i = DbConnection.Create(cmd);
            }
            return i;
        }
       
        public List<ContractInformation> GetAllContractInformation()
        {

            List<ContractInformation> contract = new List<ContractInformation>();

            SqlCommand com = new SqlCommand("ReadContractManagement");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                contract.Add(

                       new ContractInformation
                       {
                           Id = Convert.ToInt32(dr["Id"]),
                           ContractNumber = Convert.ToInt64(dr["ContractNumber"]),
                           ClientReferenceNo = Convert.ToString(dr["ClientReferenceNo"]),
                           CustomerName = Convert.ToString(dr["CustomerName"]),
                           ModelId = Convert.ToInt32(dr["ModelId"]),
                           ChassisNo = Convert.ToString(dr["ChassisNo"]),
                           RegistrationNo = Convert.ToString(dr["RegistrationNo"]),
                       }
                       );
            }

            return contract;

        }
        
        public ContractInformation details(int id)
        {
            ContractInformation con = new ContractInformation();
            SqlCommand com = new SqlCommand("ContractInformationDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                con.ContractNumber = Convert.ToInt64(dr["ContractNumber"]);
                con.IssueDate = Convert.ToDateTime(dr["IssueDate"]);
                con.ClientReferenceNo = Convert.ToString(dr["ClientReferenceNo"]);
                con.DealerId = Convert.ToInt32(dr["DealerId"]);
                con.BranchId = Convert.ToInt32(dr["BranchId"]);
                con.CustomerName = Convert.ToString(dr["CustomerName"]);
                con.Email = Convert.ToString(dr["Email"]);
                con.Mobile = Convert.ToString(dr["Mobile"]);
                con.City = Convert.ToString(dr["City"]);
                con.Address = Convert.ToString(dr["Address"]);
                con.ProductGroupId = Convert.ToInt32(dr["ProductGroupId"]);
                con.ProductSubGroupId = Convert.ToInt32(dr["ProductSubGroupId"]);
                con.SoldDate = Convert.ToDateTime(dr["SoldDate"]);
                con.MakeId = Convert.ToInt32(dr["MakeId"]);
                con.ModelId = Convert.ToInt32(dr["ModelId"]);
                con.CategoryName = Convert.ToString(dr["CategoryName"]);
                con.ManufacturerAssistanceDuration = Convert.ToInt32(dr["ManufacturerAssistanceDuration"]);
                con.ManufacturerAssistanceMileageCutoff = Convert.ToInt32(dr["ManufacturerAssistanceMileageCutoff"]);
                con.ModelYear = Convert.ToInt32(dr["ModelYear"]);
                con.ExtendedAssistanceDuration = Convert.ToInt32(dr["ExtendedAssistanceDuration"]);
                con.ExtendedAssistanceMileageCutoff = Convert.ToInt32(dr["ExtendedAssistanceMileageCutoff"]);
                con.ExtendedAssistanceMileageCutoff = Convert.ToInt32(dr["ExtendedAssistanceMileageCutoff"]);
                con.Remarks = Convert.ToString(dr["Remarks"]);
                con.ExtendedAssistanceStartDate = Convert.ToDateTime(dr["ExtendedAssistanceStartDate"]);
                con.ExtendedAssistanceExpiryDate = Convert.ToDateTime(dr["ExtendedAssistanceExpiryDate"]);
                con.ManufacturerAssistanceExpiryDate = Convert.ToDateTime(dr["ManufacturerAssistanceExpiryDate"]);
                con.ChassisNo = Convert.ToString(dr["ChassisNo"]);
                con.LimitPerClaimId = Convert.ToInt32(dr["LimitPerClaimId"]);
                con.ProductPrice = Convert.ToSingle(dr["ProductPrice"]);
                con.CurrentMileageId = Convert.ToInt32(dr["CurrentMileageId"]);
                con.CCRangeId = Convert.ToInt32(dr["CCRangeId"]);
                con.CubicCapacity = Convert.ToInt32(dr["CubicCapacity"]);
                con.RegistrationNo = Convert.ToString(dr["RegistrationNo"]);
                con.RegistrationDate = Convert.ToDateTime(dr["RegistrationDate"]);
                con.PlateType = Convert.ToString(dr["PlateType"]);
                con.SoldBy = Convert.ToString(dr["SoldBy"]);
                con.MinPremium = Convert.ToSingle(dr["MinPremium"]);
                con.Premium = Convert.ToSingle(dr["Premium"]);
                con.Status = Convert.ToInt32(dr["Status"]);

            }
            return con;
        }

        public int update(ContractManagement sale)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateContractInformation");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", sale.contractInformation.Id);
            cmd.Parameters.AddWithValue("@ContractNumber", sale.contractInformation.ContractNumber);
            cmd.Parameters.AddWithValue("@IssueDate", sale.contractInformation.IssueDate);
            cmd.Parameters.AddWithValue("@ClientReferenceNo", sale.contractInformation.ClientReferenceNo);
            cmd.Parameters.AddWithValue("@DealerId", sale.contractInformation.DealerId);
            cmd.Parameters.AddWithValue("@BranchId", sale.contractInformation.BranchId);
            cmd.Parameters.AddWithValue("@FilePath", sale.contractInformation.FilePath);
            cmd.Parameters.AddWithValue("@CustomerName", sale.contractInformation.CustomerName);
            cmd.Parameters.AddWithValue("@Email", sale.contractInformation.Email);
            cmd.Parameters.AddWithValue("@Mobile", sale.contractInformation.Mobile);
            cmd.Parameters.AddWithValue("@City", sale.contractInformation.City);
            cmd.Parameters.AddWithValue("@Address", sale.contractInformation.Address);
            cmd.Parameters.AddWithValue("@ProductGroupId", sale.contractInformation.ProductGroupId);
            cmd.Parameters.AddWithValue("@ProductSubGroupId", sale.contractInformation.ProductSubGroupId);
            cmd.Parameters.AddWithValue("@SoldDate", sale.contractInformation.SoldDate);
            cmd.Parameters.AddWithValue("@MakeId", sale.contractInformation.MakeId);
            cmd.Parameters.AddWithValue("@ModelId", sale.contractInformation.ModelId);
            cmd.Parameters.AddWithValue("@CategoryName", sale.contractInformation.CategoryName);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceDuration", sale.contractInformation.ManufacturerAssistanceDuration);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceMileageCutoff", sale.contractInformation.ManufacturerAssistanceMileageCutoff);
            cmd.Parameters.AddWithValue("@ModelYear", sale.contractInformation.ModelYear);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceDuration", sale.contractInformation.ExtendedAssistanceDuration);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceMileageCutoff", sale.contractInformation.ExtendedAssistanceMileageCutoff);
            cmd.Parameters.AddWithValue("@Remarks", sale.contractInformation.Remarks);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceStartDate", sale.contractInformation.ExtendedAssistanceStartDate);
            cmd.Parameters.AddWithValue("@ExtendedAssistanceExpiryDate", sale.contractInformation.ExtendedAssistanceExpiryDate);
            cmd.Parameters.AddWithValue("@ManufacturerAssistanceExpiryDate", sale.contractInformation.ManufacturerAssistanceExpiryDate);
            cmd.Parameters.AddWithValue("@ChassisNo", sale.contractInformation.ChassisNo);
            cmd.Parameters.AddWithValue("@LimitPerClaimId", sale.contractInformation.LimitPerClaimId);
            cmd.Parameters.AddWithValue("@ProductPrice", sale.contractInformation.ProductPrice);
            cmd.Parameters.AddWithValue("@CurrentMileageId", sale.contractInformation.CurrentMileageId);
            cmd.Parameters.AddWithValue("@CCRangeId", sale.contractInformation.CCRangeId);
            cmd.Parameters.AddWithValue("@CubicCapacity", sale.contractInformation.CubicCapacity);
            cmd.Parameters.AddWithValue("@RegistrationNo", sale.contractInformation.RegistrationNo);
            cmd.Parameters.AddWithValue("@RegistrationDate", sale.contractInformation.RegistrationDate);
            cmd.Parameters.AddWithValue("@PlateType", sale.contractInformation.PlateType);
            cmd.Parameters.AddWithValue("@SoldBy", sale.contractInformation.SoldBy);
            cmd.Parameters.AddWithValue("@MinPremium", sale.contractInformation.MinPremium);
            cmd.Parameters.AddWithValue("@Premium", sale.contractInformation.Premium);
            cmd.Parameters.AddWithValue("@Status", sale.contractInformation.Status);

            i = DbConnection.Update(cmd);
            return i;



        }

        public int UpdateDetails(List<OtherService> InqList)
        {
            int i = 0;


            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("UpdateOtherServices");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", item.Id);
                cmd.Parameters.AddWithValue("@OtherServiceId", item.OtherServiceId);
                cmd.Parameters.AddWithValue("@WorkshopId", item.WorkshopId);
                cmd.Parameters.AddWithValue("@WorkshopBranchId", item.WorkshopBranchId);
                cmd.Parameters.AddWithValue("@CategoryId", item.CategoryId);
                cmd.Parameters.AddWithValue("@ProductSubGroupId", item.ProductSubGroupId);
                cmd.Parameters.AddWithValue("@CCRangeId", item.CCRangeId);
                cmd.Parameters.AddWithValue("@ManuDuration", item.ManuDuration);
                cmd.Parameters.AddWithValue("@ManuCuttoff", item.ManuCuttoff);
                cmd.Parameters.AddWithValue("@Durations", item.Duration);
                cmd.Parameters.AddWithValue("@MilageCuttoff", item.MilageCuttoff);
                cmd.Parameters.AddWithValue("@MinPremium", item.MinPremium);
                cmd.Parameters.AddWithValue("@Premium", item.Premium);
                cmd.Parameters.AddWithValue("@Quantity", item.Quantity);
                i = DbConnection.Update(cmd);
            }
            return i;
        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("ContractInformationDelete");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Delete(com);
            return i;
        }

        public long GetContarctNumber()
        {
            long contarctnumber = 0;
         
            SqlCommand com = new SqlCommand("GetContarctNumber");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                contarctnumber = Convert.ToInt64(dt.Rows[0]["ContactNumber"]);
            }
            return contarctnumber+1;
        }

    }

    public class ContractManagementDisplay
    {
        public List<ContractInformation> contractList { get; set; }
        public UserPermission userP { get; set; }
    }

}
