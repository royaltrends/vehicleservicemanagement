﻿using VehicleServiceManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class Country
    {

        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Country Name")]
        public string CountryName { get; set; }
        public string Description { get; set; }      
        public List<Country> countrylist { get; set; }

        public int create(Country obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddCountry");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("CountryName", obj.CountryName);           
            cmd.Parameters.AddWithValue("Description", obj.Description);

            i = DbConnection.Create(cmd);
            return i;
        }

        public List<Country> GetAllCountries()
        {

            List<Country> Country = new List<Country>();

            SqlCommand com = new SqlCommand("ReadCountry");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                Country.Add(
                    new Country
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        CountryName = Convert.ToString(dr["CountryName"]),                      
                        Description = Convert.ToString(dr["Description"])
                    }
                    );
            }
            return Country;
        }

        public Country details(int id)
        {

            Country con = new Country();
            SqlCommand com = new SqlCommand("GetCountryDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            con.Id = Convert.ToInt32(dr["Id"]);
            con.CountryName = dr["CountryName"].ToString();           
            con.Description = dr["Description"].ToString();
            return con;
        }

        public int  update(Country o)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("UpdateCountry");
            com.CommandType = CommandType.StoredProcedure;           
            com.Parameters.AddWithValue("Id", o.Id);
            com.Parameters.AddWithValue("CountryName", o.CountryName);
            com.Parameters.AddWithValue("Description", o.Description);
             i = DbConnection.Update(com);
            return i;
        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteCountry");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }
    }
}