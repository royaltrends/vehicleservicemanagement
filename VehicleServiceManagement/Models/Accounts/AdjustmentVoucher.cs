﻿using VehicleServiceManagement.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class AdjustmentVoucher
    {
        public AdjustmentVoucherMaster AdjustmentVoucherMaster { get; set; }
        public AdjustmentVoucherDetails AdjustmentVoucherDetails { get; set; }

        public int AdjustmentVoucherMasterCreate(AdjustmentVoucher obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddAdjustmentMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("VoucherNo", obj.AdjustmentVoucherMaster.VoucherNo);
            cmd.Parameters.AddWithValue("Date", obj.AdjustmentVoucherMaster.Date);
            cmd.Parameters.AddWithValue("Location", obj.AdjustmentVoucherMaster.Location);
            cmd.Parameters.AddWithValue("PaymentType", obj.AdjustmentVoucherMaster.PaymentType);
            cmd.Parameters.AddWithValue("Currency", obj.AdjustmentVoucherMaster.Currency);
            cmd.Parameters.AddWithValue("Reference", obj.AdjustmentVoucherMaster.Reference);
            cmd.Parameters.AddWithValue("Status", obj.AdjustmentVoucherMaster.Status);
            cmd.Parameters.AddWithValue("Remarks", obj.AdjustmentVoucherMaster.Remarks);
            cmd.Parameters.AddWithValue("TotalDebit", obj.AdjustmentVoucherMaster.TotalDebit);
            cmd.Parameters.AddWithValue("TotalCredit", obj.AdjustmentVoucherMaster.TotalCredit);
            i = DbConnection.Create(cmd);
            return i;
        }

        public int AdjustmentVoucherDetailsCreate(List<AdjustmentVoucherDetails> InqList)
        {
            int i = 0;
            AdjustmentVoucher obj = new AdjustmentVoucher();

            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddAdjustmentDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("VoucherNo", item.VoucherNo);
                cmd.Parameters.AddWithValue("Account", item.Account);
                cmd.Parameters.AddWithValue("Narration", item.Narration);
                cmd.Parameters.AddWithValue("InvoiceNo", item.InvoiceNo);
                cmd.Parameters.AddWithValue("ReceiptNo", item.ReceiptNo);
                cmd.Parameters.AddWithValue("PaymentNo", item.PaymentNo);
                cmd.Parameters.AddWithValue("DrAmount", item.DrAmount);
                cmd.Parameters.AddWithValue("CrAmount", item.CrAmount);
                cmd.Parameters.AddWithValue("Balance", item.Balance);
                i = DbConnection.Create(cmd);
            }
            return i;


        }

        public List<AdjustmentVoucherDetails> GetAllDetails(int InquiryNo)
        {

            List<AdjustmentVoucherDetails> pi = new List<AdjustmentVoucherDetails>();

            SqlCommand com = new SqlCommand("ReadAdjustmentDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("VoucherNo", InquiryNo);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                pi.Add(

                    new AdjustmentVoucherDetails
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        VoucherNo = Convert.ToInt32(dr["VoucherNo"]),
                        Account = Convert.ToInt32(dr["Account"]),
                        Narration = Convert.ToString(dr["Narration"]),
                        InvoiceNo = Convert.ToInt32(dr["InvoiceNo"]),
                        ReceiptNo = Convert.ToInt32(dr["ReceiptNo"]),
                        PaymentNo = Convert.ToInt32(dr["PaymentNo"]),
                        DrAmount = Convert.ToSingle(dr["DrAmount"]),
                        CrAmount = Convert.ToSingle(dr["CrAmount"]),
                        Balance = Convert.ToSingle(dr["Balance"]),


                    }


                    );
            }

            return pi;

        }


    }
}

