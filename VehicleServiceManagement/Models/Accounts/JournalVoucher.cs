﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class JournalVoucher
    {
        public int Id { get; set; }
        public JournalVoucherMaster JournalVoucherMaster { get; set; }
        public JournalVoucherDetails JournalVoucherDetails { get; set; }

        /**Used***/
        public int JournalVoucherMasterCreate(JournalVoucher obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddJournalMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("@Date", obj.JournalVoucherMaster.Date);
            cmd.Parameters.AddWithValue("@LocationId", obj.JournalVoucherMaster.LocationId);
            cmd.Parameters.AddWithValue("@EmployeeId", obj.JournalVoucherMaster.EmployeeId);
            cmd.Parameters.AddWithValue("@Reference", obj.JournalVoucherMaster.Reference);
            cmd.Parameters.AddWithValue("@Status", obj.JournalVoucherMaster.Status);
            cmd.Parameters.AddWithValue("@Remarks", obj.JournalVoucherMaster.Remarks);
            cmd.Parameters.AddWithValue("@TotalDebit", obj.JournalVoucherMaster.TotalDebit);
            cmd.Parameters.AddWithValue("@TotalCredit", obj.JournalVoucherMaster.TotalCredit);
            i = DbConnection.Create(cmd);
            return i;
        }

        /**Used***/
        public int JournalVoucherDetailsCreate(List<JournalVoucherDetails> InqList,long jvId)
        {
            int i = 0;
            JournalVoucher obj = new JournalVoucher();

            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddJournalDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JVId", jvId);
                cmd.Parameters.AddWithValue("@LedgerId", item.LedgerId);
                cmd.Parameters.AddWithValue("@Narration", item.Narration);              
                cmd.Parameters.AddWithValue("@DrAmount", item.DrAmount);
                cmd.Parameters.AddWithValue("@CrAmount", item.CrAmount);
                cmd.Parameters.AddWithValue("@Balance", item.Balance);
                i = DbConnection.Create(cmd);
                Decimal amount = 0;
                Boolean credit = true;
                if (item.DrAmount > 0)
                {
                    credit = false;
                    amount = Convert.ToDecimal(item.DrAmount);
                }
                else
                {                    
                    amount = Convert.ToDecimal(item.CrAmount);
                }

                ClsAccountTransaction objacc = new ClsAccountTransaction();
                objacc.create(ClsAccountTransaction.EnumTransactionType.JournelVoucher, i.ToString(), item.LedgerId, amount, credit, "JV Entry");
            }
            return i;
        }
        /**Used***/
        public List<JournalVoucherDetails> GetAllDetails(int jvid)
        {

            List<JournalVoucherDetails> pi = new List<JournalVoucherDetails>();

            SqlCommand com = new SqlCommand("ReadJournalDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@JVId", jvid);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                pi.Add(

                    new JournalVoucherDetails
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        JVId = Convert.ToInt32(dr["JVId"]),
                        LedgerId = Convert.ToInt32(dr["LedgerId"]),
                        LedgerName = Convert.ToString(dr["LedgerName"]),
                        Narration = Convert.ToString(dr["Narration"]),                       
                        DrAmount = Convert.ToSingle(dr["DrAmount"]),
                        CrAmount = Convert.ToSingle(dr["CrAmount"]),
                        Balance = Convert.ToSingle(dr["Balance"]),
                    }

                    );
            }

            return pi;

        }
        /**Used***/
        public JournalVoucherMaster details(int id)
        {
            JournalVoucherMaster obj = new JournalVoucherMaster();
            SqlCommand com = new SqlCommand("ViewJournalVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);           
            obj.Date = Convert.ToDateTime(dr["Date"]);
            obj.LocationId = Convert.ToInt32(dr["Location"]);
            obj.LocationName = Convert.ToString(dr["BranchName"]);
            obj.Reference  = dr["Reference"].ToString();
            obj.Status = Convert.ToBoolean(dr["Status"]);
            obj.Remarks = dr["Remarks"].ToString();
            obj.TotalDebit = Convert.ToSingle(dr["TotalDebit"]);
            obj.TotalCredit = Convert.ToSingle(dr["TotalCredit"]);
           
            return (obj);


        }
        /**Used***/
        public int UpdateJournalVoucherMaster(JournalVoucher obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdateJournalVoucherMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Date", obj.JournalVoucherMaster.Date);
            cmd.Parameters.AddWithValue("LocationId", obj.JournalVoucherMaster.LocationId);
            cmd.Parameters.AddWithValue("Reference", obj.JournalVoucherMaster.Reference);
            cmd.Parameters.AddWithValue("Status", obj.JournalVoucherMaster.Status);
            cmd.Parameters.AddWithValue("Remarks", obj.JournalVoucherMaster.Remarks);
            cmd.Parameters.AddWithValue("TotalDebit", obj.JournalVoucherMaster.TotalDebit);
            cmd.Parameters.AddWithValue("TotalCredit", obj.JournalVoucherMaster.TotalCredit);
            i = DbConnection.Update(cmd);
            return i;
        }
        
        /**Used***/
        public int UpdateDetails(List<JournalVoucherDetails> InqList,long jvid)
        {
            int i = 0;
            JournalVoucher obj = new JournalVoucher();
            SqlCommand cmd1 = new SqlCommand();
            cmd1 = new SqlCommand("DeleteJournalVoucherDetails");
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@JVId", jvid);
            i = DbConnection.Delete(cmd1);
            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddJournalDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@JVId", jvid);
                cmd.Parameters.AddWithValue("@LedgerId", item.LedgerId);
                cmd.Parameters.AddWithValue("@Narration", item.Narration);      
                cmd.Parameters.AddWithValue("@DrAmount", item.DrAmount);
                cmd.Parameters.AddWithValue("@CrAmount", item.CrAmount);
                cmd.Parameters.AddWithValue("@Balance", item.Balance);
                i = DbConnection.Create(cmd);
            }
            return i;
        }
        /**Used***/
        public int JournalVoucherMasterDelete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteJournalVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }
        /**Used***/
        public int JournalVoucherDetailsDelete(long jvid)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteJournalVoucherDetails");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@JVId", jvid);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }

    }
}
