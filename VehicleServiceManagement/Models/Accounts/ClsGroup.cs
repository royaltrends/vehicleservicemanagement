﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsGroup
    {
        public int Id { get; set; }
        public int RootId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<ClsSubGroup> SubGroupList { get; set; }
               
        public List<ClsSubGroup> GetAllGroup()
        {

            List<ClsSubGroup> sub = new List<ClsSubGroup>();

            SqlCommand com = new SqlCommand("GetAllGroupAccount");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                sub.Add(
                    new ClsSubGroup
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                    }
                    );
            }
            return sub;
        }

        public enum EnumRootAccount
        {
            Asset = 1,
            Liablities = 2,
        }
    }
}

