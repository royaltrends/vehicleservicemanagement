﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsSubGroup
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Select Chart Of Account")]
        public int GroupId { get; set; }
        public string Code { get; set; }
        public string groupname { get; set; }
        [Required(ErrorMessage = "Please Enter Accounts Group Name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public Boolean IsDefault { get; set; }

        public Clsledger ledger{ get; set; }
        public List<ClsSubGroup> clsList { get; set; }
        public UserPermission userP { get; set; }
        /*****used**************/
        public List<ClsSubGroup> GetAllSubGroup()
        {

            List<ClsSubGroup> sub = new List<ClsSubGroup>();
            SqlCommand com = new SqlCommand("GetAllAccountsSubGroup");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                sub.Add(
                    new ClsSubGroup
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        GroupId = Convert.ToInt32(dr["GroupId"]),
                        groupname = Convert.ToString(dr["GroupName"]),
                        Code = Convert.ToString(dr["Code"]),
                        Description = Convert.ToString(dr["Description"]),
                        IsDefault = Convert.ToBoolean(dr["IsDefault"]),
                    }
                    );
            }
            return sub;
        }
         
        public List<ClsSubGroup> GetAllSubGroupIncome()
        {

            List<ClsSubGroup> sub = new List<ClsSubGroup>();
            SqlCommand com = new SqlCommand("GetAllAccountsSubGroupIncome");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {
                sub.Add(
                    new ClsSubGroup
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["SubGroupName"]),
                    }
                    );
            }
            return sub;
        }

        public List<ClsSubGroup> GetAllSubGroupAsset()
        {

            List<ClsSubGroup> sub = new List<ClsSubGroup>();
            SqlCommand com = new SqlCommand("GetAllAccountsSubGroupAsset");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);          
            foreach (DataRow dr in dt.Rows)
            {

                sub.Add(
                    new ClsSubGroup
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["SubGroupName"]),
                    }
                    );
            }

            return sub;

        }
        /*****used**************/
        public int create(ClsSubGroup obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddSubGroup");
            cmd.CommandType = CommandType.StoredProcedure;          
            cmd.Parameters.AddWithValue("GroupId", obj.GroupId);
            cmd.Parameters.AddWithValue("Name", obj.Name);
            cmd.Parameters.AddWithValue("Code", obj.Code);
            cmd.Parameters.AddWithValue("Description", obj.Description);
            cmd.Parameters.AddWithValue("IsDefault", obj.IsDefault);
            i = DbConnection.Create(cmd);
            return i;
        }


        public ClsSubGroup details(int id)
        {

            ClsSubGroup con = new ClsSubGroup();
            SqlCommand com = new SqlCommand("GetSubGroupDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            con.Id = Convert.ToInt32(dr["Id"]);
            con.GroupId = Convert.ToInt32(dr["GroupId"]);
            con.Name = Convert.ToString(dr["Name"]);            
            con.Code = Convert.ToString(dr["Code"]);
            con.Description = Convert.ToString(dr["Description"]);
            return con;
        }

        public int update(ClsSubGroup obj)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("UpdateSubGroupaccount");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", obj.Id);
            com.Parameters.AddWithValue("GroupId", obj.GroupId);
            com.Parameters.AddWithValue("Name", obj.Name);
            com.Parameters.AddWithValue("Code", obj.Code);
            com.Parameters.AddWithValue("Description", obj.Description);
            i = DbConnection.Update(com);
            return i;
        }

        public int delete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeleteSubGroupaccount");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@Id", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }

        public enum EnumGroupAccount
        {
            CurrentAsset = 1,
            NonCurrentAsset = 2,
            DirectIncome = 3,
            IndirectIncome = 4,
            CurrentLiabilities = 5,
            NonCurrentLiabilities = 6,
            OwnerEquity = 7,
            DirectExpense = 8,
            IndirectExpense = 9,

        }
    }
}
