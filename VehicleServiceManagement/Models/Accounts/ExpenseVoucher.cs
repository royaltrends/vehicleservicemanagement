﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ExpenseVoucher
    {
        public ExpenseVoucherMaster ExpenseMaster { get; set; }
        public ExpenseVoucherDetails ExpenseDetails { get; set; }
        /**Used***/
        public int CreateMaster(ExpenseVoucher rec)
        {

            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddExpenseVoucherMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Location", rec.ExpenseMaster.Location);
            cmd.Parameters.AddWithValue("@FromAccount", rec.ExpenseMaster.FromAccount);
            cmd.Parameters.AddWithValue("@Date", rec.ExpenseMaster.Date);
            cmd.Parameters.AddWithValue("@Reference", rec.ExpenseMaster.Reference);
            
            cmd.Parameters.AddWithValue("@PaymentMethod", rec.ExpenseMaster.PaymentMethod);
            cmd.Parameters.AddWithValue("@Status", rec.ExpenseMaster.Status);
            cmd.Parameters.AddWithValue("@BankName", rec.ExpenseMaster.BankName);
            cmd.Parameters.AddWithValue("@ChequeNo", rec.ExpenseMaster.ChequeNo);
            cmd.Parameters.AddWithValue("@ChequeDate", rec.ExpenseMaster.ChequeDate);
            cmd.Parameters.AddWithValue("@PDC", rec.ExpenseMaster.PDC);
            cmd.Parameters.AddWithValue("@Remarks", rec.ExpenseMaster.Remarks);
            cmd.Parameters.AddWithValue("@TotalAmount", rec.ExpenseMaster.TotalAmount);
          
            i = DbConnection.Create(cmd);
            return i;
        }


        /**Used***/
        public int CreateDetails(List<ExpenseVoucherDetails> InqList)
        {
            int i = 0;
            
            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddExpenseVoucherDetails");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ExpenseId", item.ExpenseId);
                cmd.Parameters.AddWithValue("@Account", item.Account);
                cmd.Parameters.AddWithValue("@Location", item.Location);
                cmd.Parameters.AddWithValue("@Payee", item.Payee);
                cmd.Parameters.AddWithValue("@RefDate", item.RefDate);
                cmd.Parameters.AddWithValue("@Amount", item.Amount);
                cmd.Parameters.AddWithValue("@VATAmount", item.VATAmount);
                cmd.Parameters.AddWithValue("@DrAmount", item.DrAmount);
                cmd.Parameters.AddWithValue("@PaidAmount", item.PaidAmount);
                cmd.Parameters.AddWithValue("@Narration", item.Narration);
                cmd.Parameters.AddWithValue("@Balance", item.Balance);
                i = DbConnection.Create(cmd);
            }
            return i;              
        }

    }
}
