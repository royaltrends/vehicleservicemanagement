﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{

    public class PaymentVoucherMaster
    {

        public int Id { get; set; }
        public int FromAccount { get; set; }
        public string FromAccountName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        [Required(ErrorMessage = "Please Enter  Date")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Please Enter  Location")]
        public int Location { get; set; }
        public string LocationName { get; set; }

        public int EmployeeId { get; set; }
        public string Employee { get; set; }
        public int DiscountLedger { get; set; }

        public int PaymentMethod { get; set; }

        public int? ChequeNumber { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime ChequeDueDate { get; set; }

        public string BankName { get; set; }

        public string OnlineTransactionNumber { get; set; }

        public string Reference { get; set; }
        public string Remarks { get; set; }
        public bool Status { get; set; }     
      
        public Decimal PaidAmount { get; set; }

   
        /**Used***/
        public List<PaymentVoucherMaster> GetPaymentVoucherMaster()
        {
            List<PaymentVoucherMaster> master = new List<PaymentVoucherMaster>();

            SqlCommand com = new SqlCommand("GetAllPaymentVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new PaymentVoucherMaster
                    {
                        Id = Convert.ToInt32(dr["Id"]),                      
                        FromAccountName = Convert.ToString(dr["Name"]),
                        FromAccount  = Convert.ToInt32(dr["FromAccount"]),
                        PaymentMethod  = Convert.ToInt32(dr["PaymentMethod"]),
                        Location = Convert.ToInt32(dr["Location"]),
                        LocationName = Convert.ToString(dr["BranchName"]),
                        Date = Convert.ToDateTime(dr["Date"]),
                        Reference = Convert.ToString(dr["Reference"]),                                    
                        PaidAmount = Convert.ToDecimal(dr["PaidAmount"])    
                    }
                    );
            }

            return master;
        }
    }

    public class PaymentVoucherMasterDisplay
    {
        public List<PaymentVoucherMaster> paymentVoucherList { get; set; }
        public UserPermission userP { get; set; }
    }
}
