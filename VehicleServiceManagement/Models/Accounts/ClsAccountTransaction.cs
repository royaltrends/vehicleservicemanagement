﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsAccountTransaction
    {

        public long TransactionId { get; set; }
        public int TransactionType { get; set; }
        public long LedgerId { get; set; }
        public Boolean Credit { get; set; }
        public Decimal Amount { get; set; }
        public string Description { get; set; }
        public string InvoiceNo { get; set; }

        /**Used***/
        public int create(EnumTransactionType type,string Invoice, long LedgerId, Decimal Amount,Boolean crbool, string Description)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AccountTransactionInsert");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", Convert.ToInt16(type));
            cmd.Parameters.AddWithValue("@Invoice", Invoice);
            cmd.Parameters.AddWithValue("@LedgerId", LedgerId);           
            cmd.Parameters.AddWithValue("@Amount", Amount);
            cmd.Parameters.AddWithValue("@Credit", crbool);
            cmd.Parameters.AddWithValue("@Description", Description);
            i = DbConnection.Create(cmd);

            return i;
        }

        public int CreateByLedgerCode(EnumTransactionType type, string Invoice, string LedgerCode, Decimal Amount,Boolean crcred, string Description)
        {

            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AccountTransactionInsertByLedgerCode");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", Convert.ToInt16(type));
            cmd.Parameters.AddWithValue("@Invoice", Invoice);
            cmd.Parameters.AddWithValue("@LedgerCode", LedgerCode);
            cmd.Parameters.AddWithValue("@Credit", crcred);
            cmd.Parameters.AddWithValue("@Amount", Amount);
            cmd.Parameters.AddWithValue("@Description", Description);
            i = DbConnection.Create(cmd);
            return i;

        }
        
        public enum EnumTransactionType
        {
            Sales = 1,
            purchase = 2,
            payment = 3,
            Reciept = 4,
            JournelVoucher = 5,
            Expense = 5,
            other = 6,
        }

        public List<ClsAccountTransaction> GetAllAmount(int id)
        {

            List<ClsAccountTransaction> objledgerlist = new List<ClsAccountTransaction>();
            SqlCommand cmd = new SqlCommand("GetAllAmounts");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DebitLedger", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(cmd);

            objledgerlist = (from DataRow dr in dt.Rows
                             select new ClsAccountTransaction()
                             {
                                 TransactionId = Convert.ToInt32(dr["TransactionId"]),
                                 TransactionType = Convert.ToInt16(dr["TransactionType"]),
                                 InvoiceNo = dr["Invoice"].ToString(),
                                 LedgerId = Convert.ToInt64(dr["LedgerId"]),
                                 Credit = Convert.ToBoolean(dr["Credit"]),
                                 Amount = Convert.ToDecimal(dr["Amount"]),
                                 Description = Convert.ToString(dr["Description"]),
                              
                             }).ToList();
            return objledgerlist;
        }

        


    }
}
     
    

