﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsRoot
    {
        public int  Id { get; set; }
        public string Name { get; set; }
        public List<ClsGroup> GroupList { get; set; }
    }
   
}
