﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class CheckPayment
    {
      

        public int Id { get; set; }
        public string Checkno { get; set; }
        public float Amount { get; set; }
       

        public int VoucherNo{ get; set; }
        public List<CheckPayment> checkdetails(string date)
        {


            List<CheckPayment> purchaseReports = new List<CheckPayment>();
            SqlCommand com = new SqlCommand("spGetPurchaseReportDate");
            com.CommandType = CommandType.StoredProcedure;
          
            com.Parameters.AddWithValue("@date", Convert.ToDateTime(date));
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {
                purchaseReports.Add(

                new CheckPayment
                {
                    //Id = Convert.ToInt32(dr["Id"]),
                   
                    Checkno = Convert.ToString(dr["Checkno"]),
                    VoucherNo = Convert.ToInt32(dr["VoucherNo"]),
                    Amount = Convert.ToInt32(dr["Amount"])

                }

                  );
            }
            return purchaseReports;
        }

    }
}
