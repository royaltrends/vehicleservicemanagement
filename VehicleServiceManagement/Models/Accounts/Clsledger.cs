﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class Clsledger
    {
        public long Id { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        [Required(ErrorMessage = "Please Select Accounts Group")]
        public long SubGroupId { get; set; }
        public string Code { get; set; }
        [Required(ErrorMessage = "Please Enter Ledger Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter  OpeningBalance")]
        public Decimal? OpeningBalance { get; set; }
        public string Dr { get; set; }
        [Required(ErrorMessage = "Please Enter Currency")]
        public int? Currency { get; set; }
        [Required(ErrorMessage = "Please Enter AsOfDate")]
        [DataType(DataType.Date)]
        [Display(Name = "AsOfDate")]
        public DateTime AsOfDate { get; set; }
        [Required(ErrorMessage = "Please Enter Remark")]
        public string Remark { get; set; }
        public Boolean IsDefault { get; set; }

        public string CurrencyName { get; set; }
        public string SubGroup { get; set; }

        public Decimal Amount { get; set; }

        public List<Clsledger> objledgerlist { get; set; }

        /*****used**************/
        public int create(Clsledger obj)
        {
           
            SqlCommand cmd;
            cmd = new SqlCommand("AddAccountsList");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@Code", obj.Code);
            cmd.Parameters.AddWithValue("@UnderAccount", obj.SubGroupId);
            if (obj.Dr == "Debit" || string.IsNullOrEmpty(obj.Dr))
                cmd.Parameters.AddWithValue("@OpeningBalance", obj.OpeningBalance);
            else
                cmd.Parameters.AddWithValue("@OpeningBalance", (-1 * obj.OpeningBalance));
            cmd.Parameters.AddWithValue("@Dr", obj.Dr);
            cmd.Parameters.AddWithValue("@Currency", obj.Currency);
            cmd.Parameters.AddWithValue("@AsOfDate", obj.AsOfDate);
            cmd.Parameters.AddWithValue("@Remarks", obj.Remark);
            return Convert.ToInt32( DbConnection.CreatewithId(cmd));
        }

        /*****used**************/
        public Clsledger details(int Id)
        {
            Clsledger obj = new Clsledger();
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("GetAccountsListDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            dt = DbConnection.Read(cmd);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.Name = dr["Name"].ToString();
            obj.Code = Convert.ToString(dr["Code"]);
            obj.SubGroupId = Convert.ToInt32(dr["UnderAccount"]);
            obj.OpeningBalance = Convert.ToDecimal(dr["OpeningBalance"]);
            obj.Dr = Convert.ToString(dr["Dr"]);
            obj.Currency = Convert.ToInt32(dr["Currency"]);
            obj.AsOfDate = Convert.ToDateTime(dr["AsofDate"]);
            obj.Remark = Convert.ToString(dr["Remarks"]);
            return obj;
        }

        public Clsledger Details(int Id)
        {
            Clsledger obj = new Clsledger();
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("GetLegerDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", Id);
            dt = DbConnection.Read(cmd);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.Name = dr["Name"].ToString();
            obj.Code = Convert.ToString(dr["Code"]);
            obj.AsOfDate = Convert.ToDateTime(dr["AsofDate"]);
            obj.SubGroupId = Convert.ToInt32(dr["SubGroupId"]);
            obj.OpeningBalance = Convert.ToDecimal(dr["OpeningBalance"]);
            return obj;
        }

        /**Used***/
        public int update(Clsledger obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("UpdateAccountsList");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", obj.Id);
            cmd.Parameters.AddWithValue("@Name", obj.Name);
            cmd.Parameters.AddWithValue("@Code", obj.Code);
            cmd.Parameters.AddWithValue("@UnderAccount", obj.SubGroupId);
            cmd.Parameters.AddWithValue("@OpeningBalance", obj.OpeningBalance);
            cmd.Parameters.AddWithValue("@Dr", obj.Dr);
            cmd.Parameters.AddWithValue("@Currency", obj.Currency);
            cmd.Parameters.AddWithValue("@AsOfDate", obj.AsOfDate);
            cmd.Parameters.AddWithValue("@Remarks", obj.Remark);
            i = DbConnection.Create(cmd);
            return i;
        }

        /**Used***/
        public int delete(int id)
        {
            int i = 0;
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("DeleteAccountsList");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);
            i = DbConnection.Create(cmd);
            return i;
        }

        /**Used***/
        public List<Clsledger> GetAllLedger()
        {
            List<Clsledger> objledgerlist = new List<Clsledger>();
            SqlCommand cmd = new SqlCommand("GetAllAccounts");
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(cmd);
            if (dt.Rows.Count > 0)
            {
                objledgerlist = (from DataRow dr in dt.Rows
                                 select new Clsledger()
                                 {
                                     Id = Convert.ToInt32(dr["Id"]),
                                     Code = dr["Code"].ToString(),
                                     Name = dr["Name"].ToString(),
                                     SubGroup = Convert.ToString(dr["SubGroup"]),
                                     GroupName=Convert.ToString(dr["GroupName"]),
                                     OpeningBalance = Convert.ToDecimal(dr["OpeningBalance"]),
                                     CurrencyName = Convert.ToString(dr["CurrencyName"]),
                                     AsOfDate = Convert.ToDateTime(dr["AsofDate"]),
                                     Remark = dr["Remarks"].ToString(),
                                     IsDefault = Convert.ToBoolean(dr["IsDefault"]),
                                 }).ToList();
            }
            return objledgerlist;
        }

        public List<Clsledger> GetAccountBySubGroupCode(int code)
        {
            List<Clsledger> Objlist = new List<Clsledger>();
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("GetAccountBySubGroupCode");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Code", code); 
            dt = DbConnection.Read(cmd);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Clsledger obj = new Clsledger();
                obj.Id = Convert.ToInt64(dt.Rows[i]["Id"]);
                obj.Name = Convert.ToString(dt.Rows[i]["Name"]);
                Objlist.Add(obj);
            }
            return Objlist;
        }

        public List<Clsledger> GetAccountBySubGroup(int SubGroupId)
        {
            List<Clsledger> Objlist = new List<Clsledger>();
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("GetAccountBySubGroup");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UnderAccount", SubGroupId);
            dt = DbConnection.Read(cmd);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Clsledger obj = new Clsledger();
                obj.Id = Convert.ToInt64(dt.Rows[i]["Id"]);
                obj.Name = Convert.ToString(dt.Rows[i]["Name"]);
                obj.OpeningBalance = Convert.ToDecimal(dt.Rows[i]["OpeningBalance"]);
                obj.AsOfDate = Convert.ToDateTime(dt.Rows[i]["AsOfDate"]);
                Objlist.Add(obj);
            }
            return Objlist;
        }

        public List<Clsledger> GetByRecieveAccount()
        {
            List<Clsledger> Objlist = new List<Clsledger>();
            SqlCommand cmd; DataTable dt = new DataTable();
            cmd = new SqlCommand("GetLegerByRecieveAccount");
            cmd.CommandType = CommandType.StoredProcedure;         
            dt = DbConnection.Read(cmd);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Clsledger obj = new Clsledger();
                obj.Id = Convert.ToInt64(dt.Rows[i]["Id"]);
                obj.Name = Convert.ToString(dt.Rows[i]["Name"]);
                Objlist.Add(obj);
            }
            return Objlist;
        }

        public List<Clsledger> GetLedgerForItemMaster()
        {

            List<Clsledger> objlist = new List<Clsledger>();
            SqlCommand com = new SqlCommand("GetLedgerForItemMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow
            foreach (DataRow dr in dt.Rows)
            {
                objlist.Add(
                    new Clsledger
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = Convert.ToString(dr["Name"]),
                        SubGroupId = Convert.ToInt32(dr["UnderAccount"]),
                        GroupId = Convert.ToInt32(dr["GroupId"]),
                    }
                    );
            }
            return objlist;

        }

    }

    public enum EnumGeneralLedger
    {
        Cash = 1,
        Bank = 2,
        GeneralCustomer = 3,
        GeneralSupplier = 4,
        Sales = 5,
        purchase = 6,
        salesreturn = 7,
        purchasereturn = 8,
        Salary = 9,
        GeneralIncome = 10,
        GeneralExpense = 11,
    }
}