﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ExpenseVoucherMaster
    {
        public int Id { get; set; }     
        public int Location { get; set; }
        public string LocationName { get; set; }
        public int FromAccount { get; set; }

        [DataType(DataType.Date)]
       
        public DateTime Date { get; set; }
     
        public string Reference { get; set; }

        public int PaymentMethod { get; set; }

        public bool Status { get; set; }
      
        public string ChequeNo { get; set; }

        [DataType(DataType.Date)]
     
        public DateTime ChequeDate { get; set; }
      
        public string PDC { get; set; }
   
        public string Remarks { get; set; }
        public float TotalAmount { get; set; }
       

        public int EmployeeId { get; set; }
        public string Employee { get; set; }

        public string BankName { get; set; }

        public string OnlineTransactionNumber { get; set; }
        public float PaidAmount { get; set; }

        public int Discount { get; set; }

        /**Used***/
        public List<ExpenseVoucherMaster> GetExpenseMaster()
        {
            List<ExpenseVoucherMaster> master = new List<ExpenseVoucherMaster>();

            SqlCommand com = new SqlCommand("GetExpenseMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new ExpenseVoucherMaster
                    {
                        Id = Convert.ToInt32(dr["Id"]),                  
                        Date = Convert.ToDateTime(dr["Date"]),
                        PaymentMethod = Convert.ToInt32(dr["PaymentMethod"]),
                        BankName = Convert.ToString(dr["BankName"]),
                        ChequeNo = Convert.ToString(dr["ChequeNo"]),
                        ChequeDate = Convert.ToDateTime(dr["ChequeDate"]),
                        LocationName = Convert.ToString(dr["LocationName"]),
                        Reference = Convert.ToString(dr["Reference"]),
                        TotalAmount = Convert.ToSingle(dr["TotalAmount"]),
                    }
                    );
            }
            return master;
        }

    }

    public class ExpenseVoucherMasterDisplay
    {
        public List<ExpenseVoucherMaster> expenseVoucherList { get; set; }
        public UserPermission userP { get; set; }
    }
}
