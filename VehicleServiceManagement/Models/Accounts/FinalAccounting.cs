﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class FinalAccounting
    {

        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public Decimal Opening { get; set; }
        public Decimal DrAmount { get; set; }
        public Decimal CrAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Closing { get; set; }       

        public Boolean profit { get; set; }

        public List<TRoot> RootList { get; set; }

        public FinalAccounting GetTrialBalance()
        {

            FinalAccounting objval = new FinalAccounting();
            SqlCommand com = new SqlCommand("GetTrialBalance");
            com.CommandType = CommandType.StoredProcedure;          
            DataSet Ds = DbConnection.ReadDs(com);
            DataTable dt = Ds.Tables[0];
            DataTable dtcompany = Ds.Tables[1];
            string RootName = null;
            string GroupName = null;
            string SubGroupName = null;
            List<TRoot> objrootlist = new List<TRoot>();
            List<TGroup> objGrouplist = new List<TGroup>();
            List<TSubGroup> objSubGrouplist = new List<TSubGroup>();
            List<TLedger> objledgerlist = new List<TLedger>();

            #region valueset

            for (int i=0;i<dt.Rows.Count;i++)
            {
                if(RootName!= Convert.ToString(dt.Rows[i]["RootName"]))
                {
                    TRoot obj = new TRoot();
                    obj.Id = Convert.ToInt32(dt.Rows[i]["RootId"]);
                    obj.Name = Convert.ToString(dt.Rows[i]["RootName"]);                   
                    objrootlist.Add(obj);
                }
                if(GroupName!= Convert.ToString(dt.Rows[i]["GroupName"]))
                {
                    TGroup obj = new TGroup();
                    obj.Id= Convert.ToInt32(dt.Rows[i]["GroupId"]);
                    obj.RootId= Convert.ToInt32(dt.Rows[i]["RootId"]);
                    obj.Name= Convert.ToString(dt.Rows[i]["GroupName"]);
                    objGrouplist.Add(obj);
                }
                if (SubGroupName != Convert.ToString(dt.Rows[i]["SubGroupName"]))
                {
                    TSubGroup obj = new TSubGroup();
                    obj.Id = Convert.ToInt32(dt.Rows[i]["SubGroupId"]);
                    obj.GroupId = Convert.ToInt32(dt.Rows[i]["GroupId"]);
                    obj.Name = Convert.ToString(dt.Rows[i]["SubGroupName"]);
                    objSubGrouplist.Add(obj);
                }

                TLedger objld = new TLedger();
                objld.SubGroupId = Convert.ToInt32(dt.Rows[i]["SubGroupId"]);
                objld.Name= Convert.ToString(dt.Rows[i]["LedgerName"]);
                objld.Opening= Convert.ToDecimal(dt.Rows[i]["OpeningBalance"]);
                objld.DrAmount= Convert.ToDecimal(dt.Rows[i]["DebitAmt"]);
                objld.CrAmount= Convert.ToDecimal(dt.Rows[i]["CreditAmt"]);
                objld.NetAmount = objld.DrAmount - objld.CrAmount;
                objld.Closing = objld.Opening + objld.NetAmount;
                objledgerlist.Add(objld);

                RootName = Convert.ToString(dt.Rows[i]["RootName"]);
                GroupName = Convert.ToString(dt.Rows[i]["GroupName"]);
                SubGroupName = Convert.ToString(dt.Rows[i]["SubGroupName"]);
               
            }
            #endregion
            
            foreach(var item in objrootlist)
            {
                List<TGroup>objgrplist= objGrouplist.Where(a => a.RootId == item.Id).ToList();
                
                foreach (var item1 in objgrplist)
                {
                    List<TSubGroup>objsublist= objSubGrouplist.Where(a => a.GroupId == item1.Id).ToList();
                   
                    foreach (var item2 in objsublist)
                    {
                        List<TLedger> ldlist = objledgerlist.Where(a => a.SubGroupId == item2.Id).ToList();
                        item2.Opening = ldlist.Sum(a => a.Opening);
                        item2.DrAmount = ldlist.Sum(a => a.DrAmount);
                        item2.CrAmount = ldlist.Sum(a => a.CrAmount);
                        item2.NetAmount = ldlist.Sum(a => a.NetAmount);
                        item2.Closing = ldlist.Sum(a => a.Closing);
                        item2.ledgerlist = ldlist;                       
                    }
                    item1.Opening = objsublist.Sum(a => a.Opening);
                    item1.DrAmount = objsublist.Sum(a => a.DrAmount);
                    item1.CrAmount = objsublist.Sum(a => a.CrAmount);
                    item1.NetAmount = objsublist.Sum(a => a.NetAmount);
                    item1.Closing = objsublist.Sum(a => a.Closing);
                    item1.subgrouplist = objsublist;
                }

                item.Opening = objgrplist.Sum(a => a.Opening);
                item.DrAmount = objgrplist.Sum(a => a.DrAmount);
                item.CrAmount = objgrplist.Sum(a => a.CrAmount);
                item.NetAmount = objgrplist.Sum(a => a.NetAmount);
                item.Closing = objgrplist.Sum(a => a.Closing);
                item.GroupList = objgrplist;
            }
            if(dtcompany.Rows.Count>0)
            {
                objval.CompanyName = Convert.ToString(dtcompany.Rows[0]["EnglishName"]);
                objval.Address = Convert.ToString(dtcompany.Rows[0]["Address"]);
                objval.Fax = Convert.ToString(dtcompany.Rows[0]["Fax"]);
                objval.Phone = Convert.ToString(dtcompany.Rows[0]["PhoneNo"]);
                objval.Email = Convert.ToString(dtcompany.Rows[0]["Email"]);
            }
            objval.Opening = objrootlist.Sum(a => a.Opening);
            objval.DrAmount = objrootlist.Sum(a => a.DrAmount);
            objval.CrAmount = objrootlist.Sum(a => a.CrAmount);
            objval.NetAmount = objrootlist.Sum(a => a.NetAmount);
            objval.Closing = objrootlist.Sum(a => a.Closing);
            objval.RootList = objrootlist;
            return objval;
        }        
              
    }

    public class ProfitandLoss
    {

        public List<PLedger> IncomeFromSales { get; set; }
        public Decimal TotalSales { get; set; }

        public List<PLedger> PurchaseExpense { get; set; }
        public Decimal TotalPurchase { get; set; }

        public Decimal GrossProfit { get; set; }

        public List<PLedger> DirectIncome { get; set; }
        public Decimal TotalDirectIncome { get; set; }

        public List<PLedger> DirectExpense { get; set; }
        public Decimal TotalDirectExpense { get; set; }

        public List<PLedger> InDirectIncome { get; set; }
        public Decimal TotalInDirectIncome { get; set; }

        public List<PLedger> InDirectExpense { get; set; }
        public Decimal TotalInDirectExpense { get; set; }

        public Decimal NetProfit { get; set; }

        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public ProfitandLoss GetProfitandLoss()
        {

            ProfitandLoss objval = new ProfitandLoss();
            SqlCommand cmd = new SqlCommand("GetProfitandLoss");
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet Ds = DbConnection.ReadDs(cmd);
            DataTable dtTrading = Ds.Tables[0];
            DataTable dtProfit = Ds.Tables[1];
            DataTable dtcompany = Ds.Tables[2];
            objval.IncomeFromSales = new List<PLedger>();
            objval.PurchaseExpense = new List<PLedger>();
            #region Trading

            for (int i = 0; i < dtTrading.Rows.Count; i++)
            {
                PLedger obj = new PLedger();
                obj.Name = Convert.ToString(dtTrading.Rows[i]["Name"]);
                obj.Amount = Convert.ToDecimal(dtTrading.Rows[i]["Amount"]);
                if (Convert.ToInt32(dtTrading.Rows[i]["UnderAccount"]) == 29)
                {
                    objval.IncomeFromSales.Add(obj);
                    objval.TotalSales = objval.TotalSales + obj.Amount;
                }
                else
                {
                    objval.PurchaseExpense.Add(obj);
                    objval.TotalPurchase = objval.TotalPurchase + obj.Amount;
                }
            }

            #endregion

            #region P&L
            objval.DirectIncome = new List<PLedger>();
            objval.InDirectIncome = new List<PLedger>();
            objval.DirectExpense = new List<PLedger>();
            objval.InDirectExpense = new List<PLedger>();
            for (int i = 0; i < dtProfit.Rows.Count; i++)
            {
                PLedger obj = new PLedger();
                obj.Name = Convert.ToString(dtProfit.Rows[i]["Name"]);
                obj.Amount = Convert.ToDecimal(dtProfit.Rows[i]["Amount"]);
                if (Convert.ToInt32(dtProfit.Rows[i]["GroupId"]) == 7)
                {
                    objval.DirectIncome.Add(obj);
                    objval.TotalDirectIncome = objval.TotalDirectIncome + obj.Amount;
                }
                else if (Convert.ToInt32(dtProfit.Rows[i]["GroupId"]) == 8)
                {
                    objval.InDirectIncome.Add(obj);
                    objval.TotalInDirectIncome = objval.TotalInDirectIncome + obj.Amount;
                }
                else if (Convert.ToInt32(dtProfit.Rows[i]["GroupId"]) == 9)
                {
                    objval.DirectExpense.Add(obj);
                    objval.TotalDirectExpense = objval.TotalDirectExpense + obj.Amount;
                }
                else
                {
                    objval.InDirectExpense.Add(obj);
                    objval.TotalInDirectExpense = objval.TotalInDirectExpense + obj.Amount;
                }                
                #endregion
            }
            objval.GrossProfit = objval.TotalSales + objval.TotalDirectIncome - objval.TotalPurchase - objval.TotalDirectExpense;
            objval.NetProfit = objval.GrossProfit + objval.TotalInDirectIncome - objval.TotalInDirectExpense;

            if (dtcompany.Rows.Count > 0)
            {
                objval.CompanyName = Convert.ToString(dtcompany.Rows[0]["EnglishName"]);
                objval.Address = Convert.ToString(dtcompany.Rows[0]["Address"]);
                objval.Fax = Convert.ToString(dtcompany.Rows[0]["Fax"]);
                objval.Phone = Convert.ToString(dtcompany.Rows[0]["PhoneNo"]);
                objval.Email = Convert.ToString(dtcompany.Rows[0]["Email"]);
            }
            return objval;
        }
    }

    public class BalanceSheet
    {

        public List<BGroup> AssetList { get; set; }
        public Decimal TotalAsset { get; set; }

        public List<BGroup> LiablityList { get; set; }
        public Decimal TotalLiablity { get; set; }

        public List<BGroup> EquityList { get; set; }
     
        public List<PLedger> CapitalAcount { get; set; }
        public Decimal TotalCapital { get; set; }


        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public BalanceSheet GetBalanceSheet()
        {
            
            BalanceSheet objval = new BalanceSheet();

            #region GetProfitandLossSummary

            SqlCommand cmd = new SqlCommand("GetProfitLossSummary");
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dtprofit = DbConnection.Read(cmd);
            Decimal Profit = 0;
            if(dtprofit.Rows.Count>0)
            {
                Profit = Convert.ToDecimal(dtprofit.Rows[0]["Amount"]);
            }

            #endregion

            cmd = new SqlCommand("GetBalancesheet");
            cmd.CommandType = CommandType.StoredProcedure;
            DataSet Ds = DbConnection.ReadDs(cmd);
            DataTable dt = Ds.Tables[0];
            DataTable dtcompany = Ds.Tables[1];

            List<BGroup> objGrouplist = new List<BGroup>();
            List<BSubGroup> objSubGrouplist = new List<BSubGroup>();
            List<PLedger> objledgerlist = new List<PLedger>();

            string GroupName = null;
            string SubGroupName = null;

            #region valueset

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                
                if (GroupName != Convert.ToString(dt.Rows[i]["GroupName"]))
                {
                    BGroup obj = new BGroup();
                    obj.Id = Convert.ToInt32(dt.Rows[i]["GroupId"]);
                    obj.RootId = Convert.ToInt32(dt.Rows[i]["RootId"]);                    
                    obj.Name = Convert.ToString(dt.Rows[i]["GroupName"]);
                    objGrouplist.Add(obj);
                }
                if (SubGroupName != Convert.ToString(dt.Rows[i]["SubGroupName"]))
                {
                    BSubGroup obj = new BSubGroup();                  
                    obj.Id = Convert.ToInt32(dt.Rows[i]["SubGroupId"]);
                    obj.GroupId = Convert.ToInt32(dt.Rows[i]["GroupId"]);
                    obj.Name = Convert.ToString(dt.Rows[i]["SubGroupName"]);
                    objSubGrouplist.Add(obj);
                }

                PLedger objld = new PLedger();
                objld.SubGroupId = Convert.ToInt32(dt.Rows[i]["SubGroupId"]);
                objld.Name = Convert.ToString(dt.Rows[i]["LedgerName"]);
                objld.Amount = Convert.ToDecimal(dt.Rows[i]["TotalAmount"]);              
                objledgerlist.Add(objld);
                GroupName = Convert.ToString(dt.Rows[i]["GroupName"]);
                SubGroupName = Convert.ToString(dt.Rows[i]["SubGroupName"]);

            }

            #endregion

            #region AssetFill

            objval.AssetList = objGrouplist.Where(a => a.RootId == 1).ToList();
            foreach(var item in objval.AssetList)
            {
                item.SubGroupList = objSubGrouplist.Where(a => a.GroupId == item.Id).ToList();
                foreach(var item1 in item.SubGroupList)
                {
                    item1.LedgerList = objledgerlist.Where(a => a.SubGroupId == item1.Id).ToList();
                    item.Total=item.Total+ item1.LedgerList.Sum(a => a.Amount);                   
                }
                objval.TotalAsset = objval.TotalAsset + item.Total;
            }

            #endregion

            #region LiablityFill
            Decimal TotLiablity = 0;
            objval.LiablityList = objGrouplist.Where(a => a.RootId == 2).ToList();
            foreach (var item in objval.LiablityList)
            {
                item.SubGroupList = objSubGrouplist.Where(a => a.GroupId == item.Id).ToList();
                foreach (var item1 in item.SubGroupList)
                {
                    item1.LedgerList = objledgerlist.Where(a => a.SubGroupId == item1.Id).ToList();
                    item.Total = item.Total + item1.LedgerList.Sum(a => a.Amount);                   
                }
                TotLiablity = TotLiablity + item.Total;
            }

            #endregion

            #region EquityFill
            Decimal TotEquity = 0;
            objval.EquityList = objGrouplist.Where(a => a.RootId == 3 && a.Id!=5).ToList();
            foreach (var item in objval.EquityList)
            {
                item.SubGroupList = objSubGrouplist.Where(a => a.GroupId == item.Id).ToList();
                foreach (var item1 in item.SubGroupList)
                {
                    item1.LedgerList = objledgerlist.Where(a => a.SubGroupId == item1.Id).ToList();
                    item.Total = item.Total + item1.LedgerList.Sum(a => a.Amount);                    
                }
                TotEquity = TotEquity + item.Total;
            }
            #endregion

            #region Capital

            Decimal TotCapital = 0;
            objval.CapitalAcount = new List<PLedger>();
            List<BGroup> capitallist = objGrouplist.Where(a => a.Id == 5).ToList();
            foreach(var item in capitallist)
            {
                item.SubGroupList = objSubGrouplist.Where(a => a.GroupId == item.Id).ToList();
                foreach (var item1 in item.SubGroupList)
                {
                    item1.LedgerList = objledgerlist.Where(a => a.SubGroupId == item1.Id).ToList();
                    objval.CapitalAcount.AddRange(item1.LedgerList);
                    item.Total = item.Total + item1.LedgerList.Sum(a => a.Amount);
                }
                TotCapital = TotCapital + item.Total;
            }


            PLedger objba = new PLedger();
            objba.Name = "Retained Gain / Loss";
            objba.Amount = objval.TotalAsset - TotLiablity - TotEquity- TotCapital - Profit;
            objval.CapitalAcount.Add(objba);
            PLedger objpf = new PLedger();
            objpf.Name = "Current Year Profit or Loss";
            objpf.Amount = Profit;
            objval.CapitalAcount.Add(objpf);

            objval.TotalCapital = TotCapital + objba.Amount + Profit;
            objval.TotalLiablity = TotLiablity + objval.TotalCapital + TotEquity;

            #endregion


            if (dtcompany.Rows.Count > 0)
            {
                objval.CompanyName = Convert.ToString(dtcompany.Rows[0]["EnglishName"]);
                objval.Address = Convert.ToString(dtcompany.Rows[0]["Address"]);
                objval.Fax = Convert.ToString(dtcompany.Rows[0]["Fax"]);
                objval.Phone = Convert.ToString(dtcompany.Rows[0]["PhoneNo"]);
                objval.Email = Convert.ToString(dtcompany.Rows[0]["Email"]);
            }
          
            return objval;
        }

    }
    
    public class BGroup
    {
        public int RootId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<BSubGroup> SubGroupList { get; set; }
        public Decimal Total { get; set; }
    }

    public class BSubGroup
    {
        public int GroupId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<PLedger> LedgerList {get;set;}
    }

    public class PLedger
    {
        public int SubGroupId { get; set; }
        public string Name { get; set; }
        public Decimal Amount { get; set; }
    }
    
    public class TRoot
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public Decimal Opening { get; set; }
        public Decimal DrAmount { get; set; }
        public Decimal CrAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Closing { get; set; }

        public List<TGroup> GroupList { get; set; }
    }
    public class TGroup
    {
        public string Name { get; set; }
        public int RootId { get; set; }
        public int Id { get; set; }

        public Decimal Opening { get; set; }
        public Decimal DrAmount { get; set; }
        public Decimal CrAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Closing { get; set; }

        public List<TSubGroup> subgrouplist { get; set; }
    }
    public class TSubGroup
    {
        public string Name { get; set; }
        public int GroupId { get; set; }
        public int Id { get; set; }

        public Decimal Opening { get; set; }
        public Decimal DrAmount { get; set; }
        public Decimal CrAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Closing { get; set; }

        public List<TLedger> ledgerlist { get; set; }
    }
    public class TLedger
    {
        public int SubGroupId { get; set; }
        public string Name { get; set; }
        public Decimal Opening { get; set; }
        public Decimal DrAmount { get; set; }
        public Decimal CrAmount { get; set; }
        public Decimal NetAmount { get; set; }
        public Decimal Closing { get; set; }
    }
}
