﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class JournalVoucherReport
    {

        public int Id { get; set; }
        
        public int VoucherNo{ get; set; }
        public DateTime Date { get; set; }
        public int Account { get; set; }
        public string AccountName { get; set; }
        public float DrAmount { get; set; }
        public float CrAmount { get; set; }
        public float Balance { get; set; }
        public float TotalDebit { get; set; }
        public float TotalCredit { get; set; }
        public List<JournalVoucherReport> journalList { get; set; }
        /**Used***/
        public List<JournalVoucherReport> GetJournalVoucherReport()
        {
            List<JournalVoucherReport> journalVoucherReport = new List<JournalVoucherReport>();
            SqlCommand com = new SqlCommand("GetJournalVoucherReport");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            foreach (DataRow dr in dt.Rows)
            {
                journalVoucherReport.Add(

                new JournalVoucherReport
                {
                    VoucherNo = Convert.ToInt32(dr["VoucherNo"]),
                    Date = Convert.ToDateTime(dr["Date"]),
                    Account = Convert.ToInt32(dr["Account"]),
                    AccountName = Convert.ToString(dr["Name"]),
                    TotalDebit = Convert.ToSingle(dr["TotalDebit"]),
                    TotalCredit = Convert.ToSingle(dr["TotalCredit"]),
                }
                  );
            }
            return journalVoucherReport;
        }

        /**Used***/

        public List<JournalVoucherReport> JournalVoucherReportFetch(string todate, string fromdate)
        {


            List<JournalVoucherReport> JournalReports = new List<JournalVoucherReport>();
            SqlCommand com = new SqlCommand("GetJournalVoucherReportDate");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(fromdate));
            com.Parameters.AddWithValue("@ToDate", Convert.ToDateTime(todate));
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {
                JournalReports.Add(

                new JournalVoucherReport
                {
                    VoucherNo = Convert.ToInt32(dr["VoucherNo"]),
                    Date = Convert.ToDateTime(dr["Date"]),
                    Account = Convert.ToInt32(dr["Account"]),
                    AccountName = Convert.ToString(dr["Name"]),
                    TotalDebit = Convert.ToSingle(dr["TotalDebit"]),
                    TotalCredit = Convert.ToSingle(dr["TotalCredit"]),

                }

                  );
            }
            return JournalReports;
        }


    }
}
