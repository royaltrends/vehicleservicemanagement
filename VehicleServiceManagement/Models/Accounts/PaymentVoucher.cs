﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace VehicleServiceManagement.Models.Accounts
{
    public class PaymentVoucher
    {
        public int Id { get; set; }
        public PaymentVoucherMaster PaymentVoucherMaster { get; set; }
        public PaymentVoucherDetails PaymentVoucherDetails { get; set; }
        /**Used***/
        public int PaymentVoucherMasterCreate(PaymentVoucherMaster obj)
        {
            int i = 0;
            SqlCommand cmd;
            cmd = new SqlCommand("AddPaymentVoucherMaster");
            cmd.CommandType = CommandType.StoredProcedure; 
            cmd.Parameters.AddWithValue("@Date", obj.Date);
            cmd.Parameters.AddWithValue("@Location", obj.Location);
            cmd.Parameters.AddWithValue("@Reference", obj.Reference);
            cmd.Parameters.AddWithValue("@FromAccount", obj.FromAccount);
            cmd.Parameters.AddWithValue("@PaymentMethod", obj.PaymentMethod);
            cmd.Parameters.AddWithValue("@OnlineTransactionNumber", obj.OnlineTransactionNumber);
            cmd.Parameters.AddWithValue("@Cheque", obj.ChequeNumber);
            cmd.Parameters.AddWithValue("@ChequeDate", obj.ChequeDueDate);
            cmd.Parameters.AddWithValue("@Status", obj.Status);                
            cmd.Parameters.AddWithValue("@PaidAmount", obj.PaidAmount);  
            cmd.Parameters.AddWithValue("@Remarks", obj.Remarks);
            i = DbConnection.Create(cmd);
            return i;
        }
        /**Used***/
        public int PaymentVoucherDetailsCreate(PaymentVoucherDetails item,long PaymentMasterId)
        {
            int i = 0;
            PaymentVoucher obj = new PaymentVoucher();
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand("AddPaymentVoucherDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("PaymentMasterId", PaymentMasterId);
            cmd.Parameters.AddWithValue("VoucherNo", item.VoucherNo);
            cmd.Parameters.AddWithValue("Discount", item.Discount);
            cmd.Parameters.AddWithValue("PaidAmount", item.PaidAmount);
            cmd.Parameters.AddWithValue("FullyPaid", item.FullyPaid);
            i = DbConnection.Create(cmd);           
            return i;
        }

      
        /**Used***/
        public PaymentVoucherMaster details(int id)
        {
            PaymentVoucherMaster obj = new PaymentVoucherMaster();
            SqlCommand com = new SqlCommand("ViewPaymentVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("VoucherNo", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);          
            obj.Date = Convert.ToDateTime(dr["Date"]);
            obj.Location = Convert.ToInt32(dr["Location"]);
            obj.LocationName = Convert.ToString(dr["LocationName"]);
            obj.Reference = dr["Reference"].ToString();
            obj.Status = Convert.ToBoolean(dr["Status"]);          
            obj.FromAccount = Convert.ToInt32(dr["FromAccount"]);
            obj.FromAccountName = Convert.ToString(dr["Name"]);
            obj.PaidAmount = Convert.ToDecimal(dr["PaidAmount"]);
            return (obj);
        }

        /**Used***/
        public int UpdatePaymentVoucherMaster(PaymentVoucher obj)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand("UpdatePaymentVoucherMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("Date", obj.PaymentVoucherMaster.Date);
            cmd.Parameters.AddWithValue("Location", obj.PaymentVoucherMaster.Location);
            cmd.Parameters.AddWithValue("Reference", obj.PaymentVoucherMaster.Reference);
            cmd.Parameters.AddWithValue("Status", obj.PaymentVoucherMaster.Status);
            cmd.Parameters.AddWithValue("FromAccount", obj.PaymentVoucherMaster.FromAccount);
            cmd.Parameters.AddWithValue("PaidAmount", obj.PaymentVoucherMaster.PaidAmount);
            i = DbConnection.Update(cmd);
            return i;
        }
        /**Used***/
        public int UpdateDetails(List<PaymentVoucherDetails> InqList)
        {
            int i = 0;
            PaymentVoucher obj = new PaymentVoucher();
            SqlCommand cmd1 = new SqlCommand();
            cmd1 = new SqlCommand("DeletePaymentVoucherDetails");
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@VoucherNo", InqList[0].VoucherNo);
            i = DbConnection.Delete(cmd1);
            foreach (var item in InqList)
            {
                SqlCommand cmd = new SqlCommand();
                cmd = new SqlCommand("AddPaymentVoucherDetails");
                cmd.CommandType = CommandType.StoredProcedure;             
                cmd.Parameters.AddWithValue("VoucherNo", item.VoucherNo);
                cmd.Parameters.AddWithValue("Discount", item.Discount);
                cmd.Parameters.AddWithValue("PaidAmount", item.PaidAmount);
                cmd.Parameters.AddWithValue("FullyPaid", item.FullyPaid);
                i = DbConnection.Create(cmd);
            }
            return i;
        }
        /**Used***/
        public int PaymentVoucherMasterDelete(int id)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeletePaymentVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@VoucherNo", id);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }
        /**Used***/
        public int PaymentVoucherDetailsDelete(List<PaymentVoucherDetails> InqList)
        {
            int i = 0;
            SqlCommand com = new SqlCommand("DeletePaymentVoucherDetails");
            com.CommandType = CommandType.StoredProcedure;

            com.Parameters.AddWithValue("@VoucherNo", InqList[0].VoucherNo);
            //DataTable dt = new DataTable();
            i = DbConnection.Delete(com);
            return i;
        }
                
    }
}
