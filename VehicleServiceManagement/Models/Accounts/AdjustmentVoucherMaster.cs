﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class AdjustmentVoucherMaster
    {
        public int Id { get; set; }
        public int VoucherNo { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        public int Location { get; set; }
        public string LocationName { get; set; }
        public string Reference { get; set; }
        public int Currency { get; set; }
        public string CurrencyName { get; set; }
        public int PaymentType { get; set; }
        public string PaymentTypeName { get; set; }
        public bool Status { get; set; }
        public string Remarks { get; set; }
        public float TotalDebit { get; set; }
        public float TotalCredit { get; set; }


        public List<AdjustmentVoucherMaster> GetAdjustmentVoucherMaster()
        {
            List<AdjustmentVoucherMaster> master = new List<AdjustmentVoucherMaster>();

            SqlCommand com = new SqlCommand("GetAllAdjustmentVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new AdjustmentVoucherMaster
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        VoucherNo = Convert.ToInt32(dr["VoucherNo"]),
                        Location = Convert.ToInt32(dr["Location"]),
                        LocationName = Convert.ToString(dr["LocationName"]),
                        Date = Convert.ToDateTime(dr["Date"]),
                        Reference = Convert.ToString(dr["Reference"]),
                        Status = Convert.ToBoolean(dr["Status"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        TotalDebit = Convert.ToSingle(dr["TotalDebit"]),
                        TotalCredit = Convert.ToSingle(dr["TotalCredit"])



                    }


                    );
            }

            return master;


        }

        public int GetAdjustmentNo()
        {
            object ob; int InqNo = 0; string Inq;
            SqlCommand com = new SqlCommand("GetAdjustmentVoucherNo");
            com.CommandType = CommandType.StoredProcedure;
            ob = DbConnection.GetObject(com);
            Inq = Convert.ToString(ob);
            if (string.IsNullOrEmpty(Inq))
            {
                InqNo = 1;
            }
            else
            {

                InqNo = Convert.ToInt32(ob);
                InqNo += 1;

            }

            return InqNo;


        }


    }
}
