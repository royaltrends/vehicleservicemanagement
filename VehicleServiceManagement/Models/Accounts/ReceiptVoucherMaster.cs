﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ReceiptVoucherMaster
    {


        public int Id { get; set; }

        public int ReceivingAccount { get; set; }
        public int ReceivingAccountName { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        [Required(ErrorMessage = "Please Enter  Date")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Please Enter  Location")]
        public int Location { get; set; }
        public string LocationName { get; set; }

        public int EmployeeId { get; set; }
        public string Employee { get; set; }
        public int DiscountLedger { get; set; }

        public int PaymentMethod { get; set; }

        public int? ChequeNumber { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime ChequeDueDate { get; set; }

        public string BankName { get; set; }

        public string OnlineTransactionNumber { get; set; }

        public string Reference { get; set; }
        public bool Status { get; set; }

        public float RecievingAmount { get; set; }

        /**Used***/
        public List<ReceiptVoucherMaster> GetReceiptMaster()
        {
            List<ReceiptVoucherMaster> master = new List<ReceiptVoucherMaster>();

            SqlCommand com = new SqlCommand("GetReceiptMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);

            foreach (DataRow dr in dt.Rows)
            {

                master.Add(

                    new ReceiptVoucherMaster
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                      
                        Date = Convert.ToDateTime(dr["Date"]),
                        PaymentMethod = Convert.ToInt32(dr["PaymentMethod"]),
                        ChequeNumber = Convert.ToInt32(dr["ChequeNo"]),
                        ChequeDueDate = Convert.ToDateTime(dr["ChequeDate"]),
                        LocationName = Convert.ToString(dr["LocationName"]),
                        Reference = Convert.ToString(dr["Reference"]),                    
                        RecievingAmount=float.Parse(dr["RecievingAmount"].ToString())
                    }
                    );
            }

            return master;
        }
      
    }

    public class ReceiptVoucherMasterDisplay
    {
        public List<ReceiptVoucherMaster> receiptVoucherList { get; set; }
        public UserPermission userP { get; set; }
    }
}
