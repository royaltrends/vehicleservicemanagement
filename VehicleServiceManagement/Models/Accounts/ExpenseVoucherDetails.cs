﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ExpenseVoucherDetails
    {
        public int Id { get; set; }
        public int ExpenseId { get; set; }
        public int Account { get; set; }
        public string AccountName { get; set; }
        public string Narration { get; set; }
        public int Location { get; set; }
        public string LocationName { get; set; }
        public String UID { get; set; }
        public String Payee { get; set; }
        [DataType(DataType.Date)]
        public DateTime RefDate { get; set; }       
        public float Amount { get; set; }
        public float VATAmount { get; set; }
        public float DrAmount { get; set; }
        public float PaidAmount { get; set; }
        public float Balance { get; set; }
        
    }
}
