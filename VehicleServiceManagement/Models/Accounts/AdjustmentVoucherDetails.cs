﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class AdjustmentVoucherDetails
    {
        public int Id { get; set; }
        public int VoucherNo { get; set; }
        public int Account { get; set; }
        public string AccountName { get; set; }
        public string Narration { get; set; }
        public int InvoiceNo { get; set; }
        public int ReceiptNo{ get; set; }
        public int PaymentNo { get; set; }
        public float DrAmount { get; set; }
        public float CrAmount { get; set; }
        public float Balance { get; set; }
        public string UID { get; set; }
    }
}
