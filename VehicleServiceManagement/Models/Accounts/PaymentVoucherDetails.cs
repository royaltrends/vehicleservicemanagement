﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class PaymentVoucherDetails
    {
        public int Id { get; set; }
        public long PaymentMasterId { get; set; }
        public long VoucherNo { get; set; }
        public DateTime Date { get; set; }
        public string VendorInvoice { get; set; }
        public float InvoiceAmount { get; set; }
        public float PrevPaid { get; set; }
        public float Balance { get; set; }
        public float Discount { get; set; }
        public float PaidAmount { get; set; }
        public Boolean FullyPaid { get; set; }
        
        public List<PaymentVoucherDetails> GetDetailsByVendor(long LedgerId)
        {

            List<PaymentVoucherDetails> objlist = new List<PaymentVoucherDetails>();
            SqlCommand com = new SqlCommand("GetPaymentByVendor");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@LedgerId", LedgerId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PaymentVoucherDetails obj = new PaymentVoucherDetails();              
                obj.VoucherNo = Convert.ToInt64(dt.Rows[i]["VoucherNo"]);
                obj.Date = Convert.ToDateTime(dt.Rows[i]["Date"]);
                obj.VendorInvoice = Convert.ToString(dt.Rows[i]["VendorInvoice"]);
                obj.InvoiceAmount = float.Parse(dt.Rows[i]["InvoiceAmount"].ToString());
                obj.PrevPaid = float.Parse(dt.Rows[i]["PrevPaid"].ToString());
                obj.Balance = obj.InvoiceAmount - obj.PrevPaid;
                objlist.Add(obj);
            }           
            return objlist;
        }

    }
}
