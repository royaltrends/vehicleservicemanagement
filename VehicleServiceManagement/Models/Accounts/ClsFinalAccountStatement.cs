﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsFinalAccountStatement
    {
        public List<ledgerdetails> incomelist { get; set; }
        public List<ledgerdetails> expenselist { get; set; }
        public Decimal adjustmentAmount { get; set; }
        public Boolean Isprofit { get; set; }

        public Decimal Prev_adjustmentAmount { get; set; }
        public Boolean Prev_Isprofit { get; set; }

    }

    public class ledgerdetails
    {
        public string ledgername { get; set; }
        public Decimal Amount { get; set; }     

    }
    
}
