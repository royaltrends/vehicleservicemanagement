﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ReceiptVoucher
    {
        public ReceiptVoucherMaster ReceiptMaster { get; set; }
        public ReceiptVoucherDetails ReceiptDetails { get; set; }

        /**Used***/

        public int CreateMaster(ReceiptVoucherMaster rec)
        {

            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("AddReceiptVoucherMaster");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Location", rec.Location);
            cmd.Parameters.AddWithValue("@ReceivingAccount", rec.ReceivingAccount);
            cmd.Parameters.AddWithValue("@Date", rec.Date);
            cmd.Parameters.AddWithValue("@Reference", rec.Reference);
            cmd.Parameters.AddWithValue("@BankName", rec.BankName);
            cmd.Parameters.AddWithValue("@PaymentMethod", rec.PaymentMethod);
            cmd.Parameters.AddWithValue("@OnlineTransactionNumber", rec.OnlineTransactionNumber);
            cmd.Parameters.AddWithValue("@Cheque", rec.ChequeNumber);
            cmd.Parameters.AddWithValue("@ChequeDate", rec.ChequeDueDate);
            cmd.Parameters.AddWithValue("@Status", rec.Status);
            cmd.Parameters.AddWithValue("@RecievingAmount", rec.RecievingAmount);
            i = DbConnection.Create(cmd);

            return i;


        }

        /**Used***/
        public int CreateDetails(ReceiptVoucherDetails item,long RecieptMasterId)
        {
            int i = 0;
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand("AddReceiptVoucherDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RecieptMasterId", item.RecieptMasterId);
            cmd.Parameters.AddWithValue("@ReceiptVoucherNo", item.ReceiptVoucherNo);            
            cmd.Parameters.AddWithValue("@Date", item.Date);
            cmd.Parameters.AddWithValue("@Discount", item.Discount);
            cmd.Parameters.AddWithValue("@RecieptAmount", item.RecieptAmount);
            cmd.Parameters.AddWithValue("@FullyRecieved", item.FullyRecieved);       
            i = DbConnection.Create(cmd);
            return i;
        }
    }


}
