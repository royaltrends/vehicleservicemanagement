﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ChartofAccounts
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string SubGroupName { get; set; }
        public int RootId { get; set; }
        public string GroupName { get; set; }
        public int MainId { get; set; }
        public string RootName { get; set; }
        public List<ChartofAccounts> chart { get; set; }
        public List<ChartofAccounts> GetAccounts()
        {

            List<ChartofAccounts> acc = new List<ChartofAccounts>();

            SqlCommand com = new SqlCommand("getDetails");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                acc.Add(

                    new ChartofAccounts
                    {

                      
                        GroupId = Convert.ToInt32(dr["GroupId"]),
                        SubGroupName = Convert.ToString(dr["Name"]),
                        RootId = Convert.ToInt32(dr["RootId"]),
                        GroupName = Convert.ToString(dr["GroupName"]),
                        MainId = Convert.ToInt32(dr["Id"]),
                        RootName = Convert.ToString(dr["RootName"])

                    }


                    );
            }

            return acc;

        }

    }
}
