﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class JournalVoucherMaster
    {
        public int Id { get; set; }
     
        [Required(ErrorMessage = "Please Enter  Date")]
        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Please Select  Location")]
        public int LocationId { get; set; }
        public string LocationName { get; set; }

        public string Reference { get; set; }
      
        public bool Status { get; set; }
        [Required(ErrorMessage = "Please Enter  Remarks")]
        public string Remarks { get; set; }
        public float TotalDebit { get; set; }
        public float TotalCredit { get; set; }

        public int EmployeeId { get; set; }
        public string Employee { get; set; }

        /**Used***/
        public List<JournalVoucherMaster> GetJournalVoucherMaster()
        {
            List<JournalVoucherMaster> master = new List<JournalVoucherMaster>();

            SqlCommand com = new SqlCommand("GetAllJournalVoucherMaster");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    master.Add(

                        new JournalVoucherMaster
                        {

                            Id = Convert.ToInt32(dr["Id"]),
                            LocationId = Convert.ToInt32(dr["LocationId"]),
                            LocationName = Convert.ToString(dr["LocationName"]),
                            Date = Convert.ToDateTime(dr["Date"]),
                            Reference = Convert.ToString(dr["Reference"]),
                            Status = Convert.ToBoolean(dr["Status"]),
                            Remarks = Convert.ToString(dr["Remarks"]),
                            TotalDebit = float.Parse(dr["TotalDebit"].ToString()),
                            TotalCredit = float.Parse(dr["TotalCredit"].ToString())
                        }
                        );
                }
            }
            return master;
        }

    }

    public class JournalVoucherMasterDisplay
    {
        public List<JournalVoucherMaster> journalVoucherList { get; set; }
        public UserPermission userP { get; set; }
    }
}
