﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ReceiptVoucherDetails
    {

        public int Id { get; set; }
        public long RecieptMasterId { get; set; }
        public long ReceiptVoucherNo { get; set; }
        public DateTime Date { get; set; }      
        public float InvoiceAmount { get; set; }
        public float PrevPaid { get; set; }
        public float Balance { get; set; }
        public float Discount { get; set; }
        public float RecieptAmount { get; set; }
        public Boolean FullyRecieved { get; set; }


        public List<ReceiptVoucherDetails> GetDetailsByCustomer(long LedgerId)
        {

            List<ReceiptVoucherDetails> objlist = new List<ReceiptVoucherDetails>();
            SqlCommand com = new SqlCommand("GetRecieptByCustomer");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@LedgerId", LedgerId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ReceiptVoucherDetails obj = new ReceiptVoucherDetails();
                obj.ReceiptVoucherNo = Convert.ToInt64(dt.Rows[i]["ReceiptVoucherNo"]);
                obj.Date = Convert.ToDateTime(dt.Rows[i]["Date"]);          
                obj.InvoiceAmount = float.Parse(dt.Rows[i]["InvoiceAmount"].ToString());
                obj.PrevPaid = float.Parse(dt.Rows[i]["PrevPaid"].ToString());
                obj.Balance = obj.InvoiceAmount - obj.PrevPaid;
                objlist.Add(obj);
            }
            return objlist;
        }
    }
}
