﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class ClsChartofAccount
    {
        public List<ClsRoot> RootList { get; set; }

        public ClsChartofAccount GetChartOfAccount()
        {
            ClsChartofAccount objval = new ClsChartofAccount();
            SqlCommand cmd = new SqlCommand("GetChartOfAccount");
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = DbConnection.Read(cmd);

            string RootName = null;
            string GroupName = null;
   
            List<ClsRoot> objrootlist = new List<ClsRoot>();
            List<ClsGroup> objGrouplist = new List<ClsGroup>();
            List<ClsSubGroup> objSubGrouplist = new List<ClsSubGroup>();

            #region valueset

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (RootName != Convert.ToString(dt.Rows[i]["RootName"]))
                {
                    ClsRoot obj = new ClsRoot();
                    obj.Id = Convert.ToInt32(dt.Rows[i]["RootId"]);
                    obj.Name = Convert.ToString(dt.Rows[i]["RootName"]);
                    objrootlist.Add(obj);
                }
                if (GroupName != Convert.ToString(dt.Rows[i]["GroupName"]))
                {
                    ClsGroup obj = new ClsGroup();
                    obj.Id = Convert.ToInt32(dt.Rows[i]["GroupId"]);
                    obj.RootId = Convert.ToInt32(dt.Rows[i]["RootId"]);
                    obj.Name = Convert.ToString(dt.Rows[i]["GroupName"]);
                    objGrouplist.Add(obj);
                }
                ClsSubGroup objsub = new ClsSubGroup();
                objsub.Id = Convert.ToInt32(dt.Rows[i]["SubGroupId"]);
                objsub.GroupId = Convert.ToInt32(dt.Rows[i]["GroupId"]);
                objsub.Name = Convert.ToString(dt.Rows[i]["SubGroupName"]);
                objSubGrouplist.Add(objsub);
                RootName = Convert.ToString(dt.Rows[i]["RootName"]);
                GroupName = Convert.ToString(dt.Rows[i]["GroupName"]);
            }
            #endregion

            foreach(var item in objrootlist)
            {
                item.GroupList = objGrouplist.Where(a => a.RootId == item.Id).ToList();
                foreach(var item1 in item.GroupList)
                {
                    item1.SubGroupList = objSubGrouplist.Where(a => a.GroupId == item1.Id).ToList();
                }
            }
            objval.RootList = objrootlist;

            return objval;
        }

    }

     

}
