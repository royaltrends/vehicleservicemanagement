﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models.Accounts
{
    public class JournalVoucherDetails
    {
        public long Id { get; set; }
        public long JVId { get; set; }       
        [Required(ErrorMessage = "Field required Account")]
        public int LedgerId { get; set; }
        public string LedgerName { get; set; }
        public string Narration { get; set; }
        public float DrAmount { get; set; }
        public float CrAmount { get; set; }
        public float Balance { get; set; }
        public string UID { get; set; }

    }
}
