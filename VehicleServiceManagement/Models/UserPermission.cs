﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class UserPermission
    {
        public Int64 Id { get; set; }
        public Int64 UserId { get; set; }
        public Int64 Form { get; set; }
        public string FormName { get; set; }
        public bool InsertPermission { get; set; }
        public bool UpdatePermission { get; set; }
        public bool DeletePermission { get; set; }
        public bool DisplayPermission { get; set; }
        public bool PrintPermission { get; set; }

        public static List<UserPermission> permissionList { get; set; }
        public List<UserPermission> GetUserPermissions(Int64 UserId = 0)
        {
            List<UserPermission> _permissions = new List<UserPermission>();

            SqlCommand com = new SqlCommand("GetUserPermission");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@UserId", UserId);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            foreach (DataRow dr in dt.Rows)
            {
                UserPermission pr = new UserPermission();

                pr.Id = Convert.ToInt64(string.IsNullOrEmpty(Convert.ToString(dr["Id"])) ? "0" : Convert.ToString(dr["Id"]));
                pr.UserId = Convert.ToInt64(string.IsNullOrEmpty(Convert.ToString(dr["UserId"])) ? "0" : Convert.ToString(dr["UserId"]));
                pr.Form = Convert.ToInt64(string.IsNullOrEmpty(Convert.ToString(dr["Form"])) ? "0" : Convert.ToString(dr["Form"]));
                pr.FormName = Convert.ToString(dr["FormName"]);
                pr.InsertPermission = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(dr["InsertPermission"])) ? 0 : Convert.ToInt16(dr["InsertPermission"]));
                pr.UpdatePermission = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(dr["UpdatePermission"])) ? 0 : Convert.ToInt16(dr["UpdatePermission"]));
                pr.DeletePermission = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(dr["DeletePermission"])) ? 0 : Convert.ToInt16(dr["DeletePermission"]));
                pr.DisplayPermission = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(dr["DisplayPermission"])) ? 0 : Convert.ToInt16(dr["DisplayPermission"]));
                pr.PrintPermission = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(dr["PrintPermission"])) ? 0 : Convert.ToInt16(dr["PrintPermission"]));
                _permissions.Add(pr);
            }
            return _permissions;
        }
    }
}
