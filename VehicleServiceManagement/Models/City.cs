﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace VehicleServiceManagement.Models
{
    public class City
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter City Name")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Please Select Country")]
        public int? CountryId { get; set; }        
        public string CountryName { get;set; }        
        public string Description { get; set; }        
         public List<City> citylist { get; set; }

        public int create(City obj)
        {
            int i = 0;

             SqlCommand cmd;
            cmd = new SqlCommand("AddCity");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("CityName", obj.CityName);
            cmd.Parameters.AddWithValue("CountryId", obj.CountryId == null ? 0 : obj.CountryId);
            cmd.Parameters.AddWithValue("Description", obj.Description);

            i = DbConnection.Create(cmd);
            return i;
        }
        public List<City> GetAllCities()
        {

            List<City> City = new List<City>();

            SqlCommand com = new SqlCommand("ReadCity1");
            com.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            //Bind EmpModel generic list using dataRow 
            foreach (DataRow dr in dt.Rows)
            {

                City.Add(

                    new City
                    {

                        Id = Convert.ToInt32(dr["Id"]),
                        CityName = Convert.ToString(dr["CityName"]),
                        //CountryId = Convert.ToInt32(dr["CountryId"]),
                        CountryName = Convert.ToString(dr["CountryName"]),
                        Description = Convert.ToString(dr["Description"])
                    }
                    );
            }

            return City;

        }
        public City details(int id)
        {
            City obj = new City();
            SqlCommand com = new SqlCommand("ViewCityDetails");
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("Id", id);
            DataTable dt = new DataTable();
            dt = DbConnection.Read(com);
            DataRow dr = dt.Rows[0];
            obj.Id = Convert.ToInt32(dr["Id"]);
            obj.CityName = dr["CityName"].ToString();
            obj.CountryId = Convert.ToInt32(dr["CountryId"]);           
            obj.Description = dr["Description"].ToString();
            return (obj);


        }
        public int update(City obj)
        {
            int i = 0;

            SqlCommand cmd;
            cmd = new SqlCommand("UpdateCity");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", obj.Id);
            cmd.Parameters.AddWithValue("CityName", obj.CityName);
            cmd.Parameters.AddWithValue("CountryId", obj.CountryId);            
            cmd.Parameters.AddWithValue("Description", obj.Description);

            i = DbConnection.Update(cmd);
            return i;
        }
        public int delete(int id)
        {
            int i = 0;
            //City obj = new City();
            SqlCommand cmd;
            cmd = new SqlCommand("DeleteCity");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Id", id);
           
            i = DbConnection.Delete(cmd);
            return i;


        }

    }
}

