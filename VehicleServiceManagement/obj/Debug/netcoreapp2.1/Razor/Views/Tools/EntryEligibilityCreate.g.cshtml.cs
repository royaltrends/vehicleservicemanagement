#pragma checksum "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f463df5d20ccad9d2f3633842dc99c51715a6186"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tools_EntryEligibilityCreate), @"mvc.1.0.view", @"/Views/Tools/EntryEligibilityCreate.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tools/EntryEligibilityCreate.cshtml", typeof(AspNetCore.Views_Tools_EntryEligibilityCreate))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement;

#line default
#line hidden
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f463df5d20ccad9d2f3633842dc99c51715a6186", @"/Views/Tools/EntryEligibilityCreate.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7224e969f80340f2a5e0ad75f7f561d5b344964f", @"/Views/_ViewImports.cshtml")]
    public class Views_Tools_EntryEligibilityCreate : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<VehicleServiceManagement.Models.EntryEligibility>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control form-control-spl"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("bankcode"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("height:auto !important; max-width:100%; width:100%;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rows", new global::Microsoft.AspNetCore.Html.HtmlString("3"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EntryEligibilityCreate", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("enctype", new global::Microsoft.AspNetCore.Html.HtmlString("multipart/form-data"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.TextAreaTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
  
    ViewData["Title"] = "EntryEligibilityCreate";
    Layout = "~/Views/Shared/_ToolLayout.cshtml";

#line default
#line hidden
            BeginContext(166, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(168, 7201, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "82d48f08a4d34ef3bf863f9fb6fa43fe", async() => {
                BeginContext(240, 1527, true);
                WriteLiteral(@"
    <div class=""content"">
        <div class=""row"">
            <div class=""col-sm-12"">
                <div class=""box box-primary"">
                    <div class=""box-header with-border"">
                        <h2 class=""box-title"">Create Entry Eligibility </h2>
                        <div class=""empl-regi"">

                        </div>
                    </div>


                    <div class=""box-body with-border"">
                        <div class=""row"">
                            <div class=""col-sm-12"">
                                <div class=""empl-regi"">
                                    <div class=""tab-content"">
                                        <div id=""bi"" class=""tab-pane fade in active"">
                                            <div class=""col-sm-6"">

                                                <div class=""row mb-5"">
                                                    <div class=""col-sm-12"">
                                                       ");
                WriteLiteral(@" <div class=""form-regi"">
                                                            <ul>
                                                                <li>
                                                                    <label>Age Limit: <lable style=""color:red"">*</lable></label>
                                                                </li>

                                                                <li>
                                                                    ");
                EndContext();
                BeginContext(1767, 89, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4cd5fc927f914c47952fb4800ef73b80", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 37 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.AgeLimit);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1856, 70, true);
                WriteLiteral("\r\n                                                                    ");
                EndContext();
                BeginContext(1927, 86, false);
#line 38 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
                                                               Write(Html.ValidationMessageFor(model => model.AgeLimit, "", new { @class = "text-danger" }));

#line default
#line hidden
                EndContext();
                BeginContext(2013, 1032, true);
                WriteLiteral(@"
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=""row mb-5"">
                                                    <div class=""col-sm-12"">
                                                        <div class=""form-regi"">
                                                            <ul>
                                                                <li>
                                                                    <label>Entry Milage: <lable style=""color:red"">*</lable></label>
                                                                </li>

                                                                <li>
                                                            ");
                WriteLiteral("        ");
                EndContext();
                BeginContext(3045, 92, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "b233275223494e208e2d2b1d7d77ee8b", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 54 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.EntryMilage);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3137, 70, true);
                WriteLiteral("\r\n                                                                    ");
                EndContext();
                BeginContext(3208, 89, false);
#line 55 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
                                                               Write(Html.ValidationMessageFor(model => model.EntryMilage, "", new { @class = "text-danger" }));

#line default
#line hidden
                EndContext();
                BeginContext(3297, 1029, true);
                WriteLiteral(@"
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=""row mb-5"">
                                                    <div class=""col-sm-12"">
                                                        <div class=""form-regi"">
                                                            <ul>
                                                                <li>
                                                                    <label>SubGroup <lable style=""color:red"">*</lable></label>
                                                                </li>

                                                                <li>

                                                               ");
                WriteLiteral("     ");
                EndContext();
                BeginContext(4327, 245, false);
#line 72 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
                                                               Write(Html.DropDownListFor(m => m.SubGroupId, (IEnumerable<SelectListItem>)ViewBag.Product, "-- Select ProductGroup--", new { required = "required", @id = "ProductGroupId", @class = "form-control form-control-spl ", @placeholder = "ProductGroupId*" }));

#line default
#line hidden
                EndContext();
                BeginContext(4572, 70, true);
                WriteLiteral("\r\n                                                                    ");
                EndContext();
                BeginContext(4643, 88, false);
#line 73 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
                                                               Write(Html.ValidationMessageFor(model => model.SubGroupId, "", new { @class = "text-danger" }));

#line default
#line hidden
                EndContext();
                BeginContext(4731, 1092, true);
                WriteLiteral(@"
                                                                </li>
                                                              

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class=""row mb-5"">
                                                    <div class=""col-sm-12"">
                                                        <div class=""form-regi"">
                                                            <ul>
                                                                <li>
                                                                    <label>Remark: <lable style=""color:red"">*</lable></label>
                                                                </li>

                                                                <li>
");
                WriteLiteral("                                                                    ");
                EndContext();
                BeginContext(5823, 145, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("textarea", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "67ee83c40ff04e56b3fd46d4a3a1bddc", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.TextAreaTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper);
#line 91 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Remark);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_TextAreaTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(5968, 70, true);
                WriteLiteral("\r\n                                                                    ");
                EndContext();
                BeginContext(6039, 84, false);
#line 92 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\EntryEligibilityCreate.cshtml"
                                                               Write(Html.ValidationMessageFor(model => model.Remark, "", new { @class = "text-danger" }));

#line default
#line hidden
                EndContext();
                BeginContext(6123, 1239, true);
                WriteLiteral(@"
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>






                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""box-footer"">
                    <div class=""form-group"">
                        <div class=""pull-right"">
                            <button type=""submit"" id=""submit"" class=""btn btn-primary"">Save & Done</button>
                            <button type=""button"" class=""btn btn-success"">Save & New</button>
                            <input type=""butt");
                WriteLiteral("on\" class=\"btn btn-primary btn-danger\" value=\"Cancel\" onclick=\"history.back()\">\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7369, 10, true);
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<VehicleServiceManagement.Models.EntryEligibility> Html { get; private set; }
    }
}
#pragma warning restore 1591
