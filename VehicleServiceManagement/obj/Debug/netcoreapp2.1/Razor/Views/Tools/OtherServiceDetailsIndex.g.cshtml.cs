#pragma checksum "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "63e89f2d4614620cd156b774e1b311bd08ca3eec"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tools_OtherServiceDetailsIndex), @"mvc.1.0.view", @"/Views/Tools/OtherServiceDetailsIndex.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tools/OtherServiceDetailsIndex.cshtml", typeof(AspNetCore.Views_Tools_OtherServiceDetailsIndex))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement;

#line default
#line hidden
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"63e89f2d4614620cd156b774e1b311bd08ca3eec", @"/Views/Tools/OtherServiceDetailsIndex.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7224e969f80340f2a5e0ad75f7f561d5b344964f", @"/Views/_ViewImports.cshtml")]
    public class Views_Tools_OtherServiceDetailsIndex : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<VehicleServiceManagement.Models.OtherServiceDetailsDisplay>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "OtherServiceDetailsCreate", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Tools", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
  
    ViewData["Title"] = "OtherServiceDetailsIndex";
     Layout = "~/Views/Shared/_ToolLayout.cshtml";

#line default
#line hidden
            BeginContext(179, 884, true);
            WriteLiteral(@"<script>



    var gl_Id = 0;

    function DealerBranchDelete(Id) {
        gl_Id = Id;
        $(""#DeleteConfirmation"").modal(""show"");
    }
    function ConfirmDelete() {
        var id = $(""#Id"").val();

        $.ajax({
            type: ""POST"",
            url: ""/Tools/DeleteOtherServiceDetails?Id="" + gl_Id,
            success: function (data) {
                $(""#DeleteConfirmation"").modal(""hide"");
                $(""table #"" + id).remove();
                location.reload(true);
            }
        })

    }

    /**/</script>
<div class=""box box-primary"">
    <div class=""box-header with-border"">
        <h2 class=""box-title"">Other Service Information</h2>
    </div>
    <div class=""box-body"">
        <div class=""row mb-7"">
            <div class=""col-sm-12"">
                <div class=""pur-in-btn"">
                    <ul>
");
            EndContext();
#line 41 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                         if (Model.userP.InsertPermission)
                        {

#line default
#line hidden
            BeginContext(1150, 28, true);
            WriteLiteral("                        <li>");
            EndContext();
            BeginContext(1178, 99, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "161da66c65224cedbb069a96fa854296", async() => {
                BeginContext(1243, 30, true);
                WriteLiteral("New <i class=\"fa fa-plus\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1277, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 44 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                        }
                        

#line default
#line hidden
            BeginContext(1480, 28, true);
            WriteLiteral("                        <li>");
            EndContext();
            BeginContext(1508, 81, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "543d22e0861243a9b2405f0461912a45", async() => {
                BeginContext(1553, 32, true);
                WriteLiteral("Cancel <i class=\"fa fa-ban\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1589, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 48 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                         if (Model.userP.PrintPermission)
                        {

#line default
#line hidden
            BeginContext(1682, 83, true);
            WriteLiteral("                        <li><a href=\"\">Print <i class=\"fa fa-print\"></i></a></li>\r\n");
            EndContext();
#line 51 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                        }

#line default
#line hidden
            BeginContext(1792, 87, true);
            WriteLiteral("                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
            EndContext();
            BeginContext(1978, 90, true);
            WriteLiteral("\r\n        <table class=\"table table-striped\">\r\n            <thead>\r\n                <tr>\r\n");
            EndContext();
#line 61 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                      
                        int sno = 0;
                    

#line default
#line hidden
            BeginContext(2153, 350, true);
            WriteLiteral(@"                    <th>SL No.</th>
                    <th>Other Service</th>
                  
                    <th>Remark</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody id=""SetDepartmentList"">
                <tr id=""LoadingStatus"" style=""color:red""></tr>
");
            EndContext();
#line 74 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                 foreach (var item in Model.otherServiceList)
                {

#line default
#line hidden
            BeginContext(2585, 23, true);
            WriteLiteral("                    <tr");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 2608, "\"", 2621, 1);
#line 76 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
WriteAttributeValue("", 2613, item.Id, 2613, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2622, 35, true);
            WriteLiteral(">\r\n\r\n                        <td>\r\n");
            EndContext();
#line 79 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                               sno++; 

#line default
#line hidden
            BeginContext(2698, 28, true);
            WriteLiteral("                            ");
            EndContext();
            BeginContext(2727, 3, false);
#line 80 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                       Write(sno);

#line default
#line hidden
            EndContext();
            BeginContext(2730, 61, true);
            WriteLiteral("\r\n                        </td>\r\n                        <td>");
            EndContext();
            BeginContext(2792, 17, false);
#line 82 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                       Write(item.OtherService);

#line default
#line hidden
            EndContext();
            BeginContext(2809, 59, true);
            WriteLiteral("</td>\r\n                      \r\n                        <td>");
            EndContext();
            BeginContext(2869, 11, false);
#line 84 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                       Write(item.Remark);

#line default
#line hidden
            EndContext();
            BeginContext(2880, 39, true);
            WriteLiteral("</td>\r\n\r\n                        <td>\r\n");
            EndContext();
#line 87 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                             if (Model.userP.UpdatePermission)
                            {

#line default
#line hidden
            BeginContext(3014, 107, true);
            WriteLiteral("                            <a href=\'#\' class=\"btn btn-primary btn-xs btn-marign-8x fa fa-edit\"title=\"Edit\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3121, "\"", 3217, 3);
            WriteAttributeValue("", 3131, "location.href=\'", 3131, 15, true);
#line 89 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
WriteAttributeValue("", 3146, Url.Action("EditOtherServiceDetails", "Tools", new { id = @item.Id }), 3146, 70, false);

#line default
#line hidden
            WriteAttributeValue("", 3216, "\'", 3216, 1, true);
            EndWriteAttribute();
            BeginContext(3218, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 90 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                            }

#line default
#line hidden
            BeginContext(3256, 28, true);
            WriteLiteral("                            ");
            EndContext();
#line 91 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                             if (Model.userP.DisplayPermission)
                            {

#line default
#line hidden
            BeginContext(3352, 107, true);
            WriteLiteral("                            <a href=\'#\' class=\"btn btn-success btn-xs btn-marign-8x fa fa-eye\" title=\"View\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3459, "\"", 3552, 3);
            WriteAttributeValue("", 3469, "location.href=\'", 3469, 15, true);
#line 93 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
WriteAttributeValue("", 3484, Url.Action("OtherServiceDetailss", "Tools", new { id = @item.Id }), 3484, 67, false);

#line default
#line hidden
            WriteAttributeValue("", 3551, "\'", 3551, 1, true);
            EndWriteAttribute();
            BeginContext(3553, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 94 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                            }

#line default
#line hidden
            BeginContext(3591, 28, true);
            WriteLiteral("                            ");
            EndContext();
#line 95 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                             if (Model.userP.DeletePermission)
                            {

#line default
#line hidden
            BeginContext(3686, 109, true);
            WriteLiteral("                            <a href=\'#\' class=\"btn btn-danger btn-xs btn-marign-8x fa fa-trash\"title=\"Delete\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3795, "\"", 3833, 3);
            WriteAttributeValue("", 3805, "DealerBranchDelete(", 3805, 19, true);
#line 97 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
WriteAttributeValue("", 3824, item.Id, 3824, 8, false);

#line default
#line hidden
            WriteAttributeValue("", 3832, ")", 3832, 1, true);
            EndWriteAttribute();
            BeginContext(3834, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 98 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                            }

#line default
#line hidden
            BeginContext(3872, 58, true);
            WriteLiteral("                        </td>\r\n                    </tr>\r\n");
            EndContext();
#line 101 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Tools\OtherServiceDetailsIndex.cshtml"
                }

#line default
#line hidden
            BeginContext(3949, 817, true);
            WriteLiteral(@"            </tbody>
        </table>
    </div>
</div>
<div class=""modal fade"" id=""MyModal1"">
</div>


<div class=""modal fade"" id=""DeleteConfirmation"">
    <div class=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <a href=""#"" class=""close"" data-dismiss=""modal"">&times;</a>
                <h4 id=""ModalTitle""></h4>
            </div>
            <div class=""modal-body"">
                <h4>Are you sure? You want to delete this record.</h4>
            </div>
            <div class=""modal-footer"">
                <a href=""#"" class=""btn btn-primary"" data-dismiss=""modal"">Cancel</a>
                <a href=""#"" class=""btn btn-danger"" onclick=""ConfirmDelete()"">Confirm</a>
            </div>
        </div>
    </div>
</div>



");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<VehicleServiceManagement.Models.OtherServiceDetailsDisplay> Html { get; private set; }
    }
}
#pragma warning restore 1591
