#pragma checksum "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a22b33a20eb5925d6c91f64f055fc363c88bc4e2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Master_MakeDetailIndex), @"mvc.1.0.view", @"/Views/Master/MakeDetailIndex.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Master/MakeDetailIndex.cshtml", typeof(AspNetCore.Views_Master_MakeDetailIndex))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement;

#line default
#line hidden
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\_ViewImports.cshtml"
using VehicleServiceManagement.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a22b33a20eb5925d6c91f64f055fc363c88bc4e2", @"/Views/Master/MakeDetailIndex.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7224e969f80340f2a5e0ad75f7f561d5b344964f", @"/Views/_ViewImports.cshtml")]
    public class Views_Master_MakeDetailIndex : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<VehicleServiceManagement.Models.MakeDetailDisplay>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "MakeDetailCreate", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Master", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
  
    ViewData["Title"] = "MakeDetailIndex";
    Layout = "~/Views/Shared/_MasterLayout.cshtml";

#line default
#line hidden
            BeginContext(162, 937, true);
            WriteLiteral(@"    <script>
        var gl_Id = 0;

        function DealerBranchDelete(Id) {
            gl_Id = Id;
            $(""#DeleteConfirmation"").modal(""show"");
        }
        function ConfirmDelete() {
            var id = $(""#Id"").val();

            $.ajax({
                type: ""POST"",
                url: ""/Master/DeleteMakeDetail?Id="" + gl_Id,
                success: function (data) {
                    $(""#DeleteConfirmation"").modal(""hide"");
                    $(""table #"" + id).remove();
                    location.reload(true);
                }
            })

        }

        /**/</script>
<div class=""box box-primary"">
    <div class=""box-header with-border"">
        <h2 class=""box-title"">Make Information</h2>
    </div>
    <div class=""box-body"">
        <div class=""row mb-7"">
            <div class=""col-sm-12"">
                <div class=""pur-in-btn"">
                    <ul>
");
            EndContext();
#line 38 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                         if (Model.userP.InsertPermission)
                        {

#line default
#line hidden
            BeginContext(1186, 28, true);
            WriteLiteral("                        <li>");
            EndContext();
            BeginContext(1214, 91, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f554173a35684d8a91a2649b4664d582", async() => {
                BeginContext(1271, 30, true);
                WriteLiteral("New <i class=\"fa fa-plus\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1305, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 41 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                        }
                        

#line default
#line hidden
            BeginContext(1508, 28, true);
            WriteLiteral("                        <li>");
            EndContext();
            BeginContext(1536, 82, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2cdf3d15cf244bf1957ca0d130ba395e", async() => {
                BeginContext(1582, 32, true);
                WriteLiteral("Cancel <i class=\"fa fa-ban\"></i>");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1618, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 45 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                         if (Model.userP.PrintPermission)
                        {

#line default
#line hidden
            BeginContext(1711, 83, true);
            WriteLiteral("                        <li><a href=\"\">Print <i class=\"fa fa-print\"></i></a></li>\r\n");
            EndContext();
#line 48 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                        }

#line default
#line hidden
            BeginContext(1821, 87, true);
            WriteLiteral("                    </ul>\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
            EndContext();
            BeginContext(2007, 90, true);
            WriteLiteral("\r\n        <table class=\"table table-striped\">\r\n            <thead>\r\n                <tr>\r\n");
            EndContext();
#line 58 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                      
                        int sno = 0;
                    

#line default
#line hidden
            BeginContext(2182, 155, true);
            WriteLiteral("                    <th>SL No.</th>\r\n                    <th>Make</th>\r\n                    <th>Product Group</th>\r\n                    <th>Category</th>\r\n");
            EndContext();
            BeginContext(2379, 212, true);
            WriteLiteral("                    <th>Action</th>\r\n                    \r\n                </tr>\r\n            </thead>\r\n            <tbody id=\"SetDepartmentList\">\r\n                <tr id=\"LoadingStatus\" style=\"color:red\"></tr>\r\n");
            EndContext();
#line 72 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                 foreach (var item in Model.makeList)
                {

#line default
#line hidden
            BeginContext(2665, 15, true);
            WriteLiteral("            <tr");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 2680, "\"", 2693, 1);
#line 74 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
WriteAttributeValue("", 2685, item.Id, 2685, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2694, 27, true);
            WriteLiteral(">\r\n\r\n                <td>\r\n");
            EndContext();
#line 77 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                       sno++; 

#line default
#line hidden
            BeginContext(2754, 20, true);
            WriteLiteral("                    ");
            EndContext();
            BeginContext(2775, 3, false);
#line 78 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
               Write(sno);

#line default
#line hidden
            EndContext();
            BeginContext(2778, 45, true);
            WriteLiteral("\r\n                </td>\r\n                <td>");
            EndContext();
            BeginContext(2824, 9, false);
#line 80 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
               Write(item.Make);

#line default
#line hidden
            EndContext();
            BeginContext(2833, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(2861, 17, false);
#line 81 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
               Write(item.ProductGroup);

#line default
#line hidden
            EndContext();
            BeginContext(2878, 27, true);
            WriteLiteral("</td>\r\n                <td>");
            EndContext();
            BeginContext(2906, 17, false);
#line 82 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
               Write(item.CategoryName);

#line default
#line hidden
            EndContext();
            BeginContext(2923, 7, true);
            WriteLiteral("</td>\r\n");
            EndContext();
            BeginContext(2975, 22, true);
            WriteLiteral("                <td>\r\n");
            EndContext();
#line 85 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                     if (Model.userP.UpdatePermission)
                    {


#line default
#line hidden
            BeginContext(3078, 104, true);
            WriteLiteral("                        <a href=\'#\' class=\"btn btn-primary btn-xs btn-marign-8x fa fa-edit\" title=\"Edit\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3182, "\"", 3266, 3);
            WriteAttributeValue("", 3192, "location.href=\'", 3192, 15, true);
#line 88 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
WriteAttributeValue("", 3207, Url.Action("MakeDetailEdit", "Master",new {id=@item.Id }), 3207, 58, false);

#line default
#line hidden
            WriteAttributeValue("", 3265, "\'", 3265, 1, true);
            EndWriteAttribute();
            BeginContext(3267, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 89 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                    }

#line default
#line hidden
            BeginContext(3297, 20, true);
            WriteLiteral("                    ");
            EndContext();
#line 90 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                     if (Model.userP.DisplayPermission)
                    {

#line default
#line hidden
            BeginContext(3377, 103, true);
            WriteLiteral("                        <a href=\'#\' class=\"btn btn-success btn-xs btn-marign-8x fa fa-eye\" title=\"View\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3480, "\"", 3566, 3);
            WriteAttributeValue("", 3490, "location.href=\'", 3490, 15, true);
#line 92 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
WriteAttributeValue("", 3505, Url.Action("MakeDetailss", "Master", new { id = @item.Id }), 3505, 60, false);

#line default
#line hidden
            WriteAttributeValue("", 3565, "\'", 3565, 1, true);
            EndWriteAttribute();
            BeginContext(3567, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 93 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                    }

#line default
#line hidden
            BeginContext(3597, 20, true);
            WriteLiteral("                    ");
            EndContext();
#line 94 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                     if (Model.userP.DeletePermission)
                    {

#line default
#line hidden
            BeginContext(3676, 106, true);
            WriteLiteral("                        <a href=\'#\' class=\"btn btn-danger btn-xs btn-marign-8x fa fa-trash\" title=\"Delete\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 3782, "\"", 3820, 3);
            WriteAttributeValue("", 3792, "DealerBranchDelete(", 3792, 19, true);
#line 96 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
WriteAttributeValue("", 3811, item.Id, 3811, 8, false);

#line default
#line hidden
            WriteAttributeValue("", 3819, ")", 3819, 1, true);
            EndWriteAttribute();
            BeginContext(3821, 7, true);
            WriteLiteral("></a>\r\n");
            EndContext();
#line 97 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                    }

#line default
#line hidden
            BeginContext(3851, 42, true);
            WriteLiteral("                </td>\r\n            </tr>\r\n");
            EndContext();
#line 100 "C:\project\vehicleservicemanagement\VehicleServiceManagement\Views\Master\MakeDetailIndex.cshtml"
                }

#line default
#line hidden
            BeginContext(3912, 813, true);
            WriteLiteral(@"            </tbody>
        </table>
    </div>
</div>
<div class=""modal fade"" id=""MyModal1"">
</div>


<div class=""modal fade"" id=""DeleteConfirmation"">
    <div class=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <a href=""#"" class=""close"" data-dismiss=""modal"">&times;</a>
                <h4 id=""ModalTitle""></h4>
            </div>
            <div class=""modal-body"">
                <h4>Are you sure? You want to delete this record.</h4>
            </div>
            <div class=""modal-footer"">
                <a href=""#"" class=""btn btn-primary"" data-dismiss=""modal"">Cancel</a>
                <a href=""#"" class=""btn btn-danger"" onclick=""ConfirmDelete()"">Confirm</a>
            </div>
        </div>
    </div>
</div>

");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<VehicleServiceManagement.Models.MakeDetailDisplay> Html { get; private set; }
    }
}
#pragma warning restore 1591
